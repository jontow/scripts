#!/bin/sh

if [ -z "$1" ]; then
	echo "Syntax: startup.sh <camhostname.example.com>"
	exit 1
fi

CAM_HOST="$1"
SESSION_NAME="$(echo ${CAM_HOST} | sed 's/\./_/g')"

SCRIPT_DIR="$HOME/GIT-je/scripts/axis-webcam"
CAM_DIR="$HOME/cam/${CAM_HOST}"

if ! camfetchpid=$(pgrep -f "^ruby.*/camfetch.rb ${CAM_HOST}"); then
	tmux new-session -d -s "${SESSION_NAME}" "cd ${CAM_DIR} && ${SCRIPT_DIR}/camfetch.rb ${CAM_HOST} ${CAM_HOST}.jpg 60"
else
	echo "Detected a running camfetch for this host: ${camfetchpid}"
	exit 1
fi
