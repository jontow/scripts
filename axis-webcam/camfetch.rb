#!/usr/bin/env ruby
#
# 2010-07-09 -- jontow@zenbsd.net -- initial rev
# 2013-09-24 -- jontow@zenbsd.net -- parameterized
# 2013-10-10 -- jontow@zenbsd.net -- made timing more precise
#
# Fetch an image every @interval seconds from a given camera, stored 
# locally with timestamp.
#

if ARGV[2].nil?
	puts "Syntax: camfetch.rb <host|ipaddr> <filename> <interval-in-seconds>"
	puts "    ie: camfetch.rb 192.168.0.1 parkinglot1.jpg 60"
	exit
end

@hostname = ARGV[0]
@basename = ARGV[1]
@interval = ARGV[2]

counter = 0

# Wait until the top of a minute (:00 sec) for cleanliness
print "Pausing until top of minute..."
STDOUT.flush
until (Time.now.to_i % 60) == 0
	sleep 0.2
end
puts "done: " + Time.now.strftime("%Y-%m-%d %H:%M:%S")

loop do
	# Is image directory available?
	imgdir = Time.now.strftime("%Y-%m-%d")
	if !File.directory?(imgdir)
		Dir.mkdir(imgdir)
	end

	until (Time.now.to_i % @interval.to_i) == 0
		sleep 0.2
	end
	now = Time.now
	fnow = now.strftime("%Y-%m-%d_%H:%M:%S")
	puts fnow + " -- " + Time.now.to_f.to_s

	cmd = "wget -q -O #{imgdir}/#{fnow}-#{@basename} http://#{@hostname}/jpg/image.jpg"
	#cmd = "wget -q -O /dev/null http://#{@hostname}/jpg/image.jpg"
	#p cmd
	`#{cmd}`

	if @interval.to_i > 5
		sleep @interval.to_i / 2
	else
		sleep 5
	end
end
