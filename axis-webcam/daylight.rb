#!/usr/bin/env ruby
################################################################################
#
# 2013-11-05 -- jtowne@clarkson.edu
#
# Attempt to 'track' daylight hours across a given series of images, using a
# directory tree like the following example:
#
# 2013-11-04/
#   +--2013-11-04_10:00:00-cameraname.jpg
#   +--2013-11-04_10:01:00-cameraname.jpg
#   +--2013-11-04_10:02:00-cameraname.jpg
# 2013-11-05/
#   +etc
#
################################################################################

require 'time'

DEBUG = true

# Stored timetable, aa.usno.navy.mil .txt format
TIMETABLE="/home/jtowne/SVN-jontow/jontow/scripts/axis-webcam/timetable.txt"

# Target frames per second for video
FPS="20"

# Target length of video, in seconds
VIDLEN="120"

@imglist = []
@dirlist = []
@timetable = {}

################################################################################
#
# Change input of something like "0415" to "04:15"
#
def fourdigtime_add_colons(fourdig)
	if fourdig.nil? or (fourdig.length != 4)
		return nil
	end

	hh = fourdig[0..1]
	mm = fourdig[2..3]

	return hh + ":" + mm
	#return hh + ":" + mm + ":00"
end

################################################################################
#
# Read in / store timetable in @timetable hash-o-hashes
#
def read_timetable
	if !File.exists?(TIMETABLE)
		STDERR.puts "Timetable file (#{TIMETABLE}) not found!"
		return nil
	end

	File.open(TIMETABLE, 'r').each do |line|
		next if line =~ /^#/
		p line if DEBUG

		(day, janrise, janset, febrise, febset, marrise, marset, aprrise, aprset, mayrise, mayset, junrise, junset, julrise, julset, augrise, augset, seprise, sepset, octrise, octset, novrise, novset, decrise, decset) = line.chomp.split(' ')

		@timetable['01'] = {} if @timetable['01'].nil?
		@timetable['02'] = {} if @timetable['02'].nil?
		@timetable['03'] = {} if @timetable['03'].nil?
		@timetable['04'] = {} if @timetable['04'].nil?
		@timetable['05'] = {} if @timetable['05'].nil?
		@timetable['06'] = {} if @timetable['06'].nil?
		@timetable['07'] = {} if @timetable['07'].nil?
		@timetable['08'] = {} if @timetable['08'].nil?
		@timetable['09'] = {} if @timetable['09'].nil?
		@timetable['10'] = {} if @timetable['10'].nil?
		@timetable['11'] = {} if @timetable['11'].nil?
		@timetable['12'] = {} if @timetable['12'].nil?

		@timetable['01'][day] = {'rise' => fourdigtime_add_colons(janrise), 'set' => fourdigtime_add_colons(janset)}
		@timetable['02'][day] = {'rise' => fourdigtime_add_colons(febrise), 'set' => fourdigtime_add_colons(febset)}
		@timetable['03'][day] = {'rise' => fourdigtime_add_colons(marrise), 'set' => fourdigtime_add_colons(marset)}
		@timetable['04'][day] = {'rise' => fourdigtime_add_colons(aprrise), 'set' => fourdigtime_add_colons(aprset)}
		@timetable['05'][day] = {'rise' => fourdigtime_add_colons(mayrise), 'set' => fourdigtime_add_colons(mayset)}
		@timetable['06'][day] = {'rise' => fourdigtime_add_colons(junrise), 'set' => fourdigtime_add_colons(junset)}
		@timetable['07'][day] = {'rise' => fourdigtime_add_colons(julrise), 'set' => fourdigtime_add_colons(julset)}
		@timetable['08'][day] = {'rise' => fourdigtime_add_colons(augrise), 'set' => fourdigtime_add_colons(augset)}
		@timetable['09'][day] = {'rise' => fourdigtime_add_colons(seprise), 'set' => fourdigtime_add_colons(sepset)}
		@timetable['10'][day] = {'rise' => fourdigtime_add_colons(octrise), 'set' => fourdigtime_add_colons(octset)}
		@timetable['11'][day] = {'rise' => fourdigtime_add_colons(novrise), 'set' => fourdigtime_add_colons(novset)}
		@timetable['12'][day] = {'rise' => fourdigtime_add_colons(decrise), 'set' => fourdigtime_add_colons(decset)}
	end

	# Control.
	print "DEBUG: Summer soltice (06-21): " if DEBUG
	p @timetable['06']['21'] if DEBUG
	print "DEBUG: Winter soltice (12-21): " if DEBUG
	p @timetable['12']['21'] if DEBUG
end

################################################################################
#
# Turn integer it into a 2dig padded string:
# ie. 1 to '01'
#
def int2padstr(someint)
	if !someint.nil?
		if someint < 10
			return "0#{someint.to_s}"
		else
			return "#{someint.to_s}"
		end
	else
		return nil
	end
end

################################################################################
#
# Turn 2dig padded string into integer:
# ie. '01' to 1
#
def padstr2int(somestr)
	if !somestr.nil?
		return somestr.to_i
	else
		return nil
	end
end

################################################################################
#
# Return iterative list from 'start' to 'end', string format
# ie. gen_num_list('06', 12)
#
def gen_num_list(start, times)
	if start.nil? or times.nil?
		return nil
	end

	retlist = []
	startint = padstr2int(start)
	times.times do |t|
		retlist << int2padstr(startint + t)
	end

	return retlist
end

################################################################################
#
################################################################################
#
# Begin main code flow
#
################################################################################

#p gen_num_list('06', 12)

# First, read in our timetable so we have data ready
read_timetable

# What time of day does 'morning' light start? (default, overwritten later)
@morning = Time.parse("06:30")
# What time of day does 'evening' light end? (default, overwritten later)
@evening = Time.parse("18:00")

@currentyear = Time.now.strftime("%Y")

@totalframes = VIDLEN.to_i * FPS.to_i
puts "DEBUG: @totalframes = #{@totalframes}" if DEBUG

@dirlist = Dir.glob('*').select {|f| File.directory? f}.sort!
puts "DEBUG: @dirlist: #{@dirlist.join(', ')}" if DEBUG

@totaldays = @dirlist.count
puts "DEBUG: @totaldays = #{@totaldays}" if DEBUG

# We need 'this many frames' from each day to hit VIDLEN at FPS
@framesperday = @totalframes.to_i / @totaldays.to_i
puts "DEBUG: @framesperday = #{@framesperday}" if DEBUG

@day_list = []
@day_lens = []
@dirlist.each do |day|
	(yyyy, mm, dd) = day.split('-', 3)
	tmrise = @timetable[mm][dd]['rise']
	tmset = @timetable[mm][dd]['set']

	dayrise = Time.parse("#{yyyy}-#{mm}-#{dd} #{tmrise}:00")
	dayset = Time.parse("#{yyyy}-#{mm}-#{dd} #{tmset}:00")
	daylen = dayset.to_i - dayrise.to_i
	@day_lens << daylen

	puts "#{yyyy}, #{mm}, #{dd}: r:#{tmrise} s:#{tmset} l:#{daylen}"
end

@avgdaylen = 0
@day_lens.each do |dl|
	@avgdaylen += dl
end
@avgdaylen /= @day_lens.count

puts "DEBUG: @avgdaylen_sec = #{@avgdaylen}" if DEBUG
puts "DEBUG: @avgdaylen_min = #{@avgdaylen / 60.0}" if DEBUG

@minsperday = ((@avgdaylen / 60.0) / @framesperday).to_i
puts "DEBUG: @minsperday = #{@minsperday}" if DEBUG

