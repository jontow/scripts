#!/bin/sh
#
# 2013-09-25 -- jontow@zenbsd.net
#
# Build motion jpeg timelapse video from director(y|ies) of images, named by their capture time.
#
#
# Some tricks:
#
################################################################################
#
# To use an input list, of a custom set of files, say 07:00-17:00 daily, try this:
#
# for DAY in 2013-10-01 2013-10-02 2013-10-03 2013-10-04; do
#  echo {${DAY}/${DAY}_0[7-9]:*,${DAY}/${DAY}_1[0-7]:*} | xargs -n 1 >>input-list.txt
# done
#
#  [verify your input file...]
#
# ./make-timelapse.sh '@input-list.txt' 800x600 25 outputfile.avi
#
################################################################################

if [ "$4" = "" ]; then
	echo ""
	echo "Syntax: make-timelapse.sh '<fileglob>' <320x240> <fps> <outputfile.avi>"
	echo ""
	echo "ie. ./make-timelapse.sh '2013-09-*/*_*:*[05]:*.jpg' 800x600 25 output-5min.avi"
	echo "  Should make a timelapse video comprised of the whole month of september, 2013"
	echo "  using only 1 frame from every 5 minutes."
	echo ""
	exit
fi

WIDTH=`echo $2 | awk -Fx '{print $1}'`
HEIGHT=`echo $2 | awk -Fx '{print $2}'`

mencoder mf://$1 -mf w=${WIDTH}:h=${HEIGHT}:fps=$3:type=jpg -ovc copy -oac copy -o $4
