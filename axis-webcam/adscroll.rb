#!/usr/bin/env ruby
#
# 2010-07-07 -- jontow@zenbsd.net
#
# Utilize dynamic text overlay ability to scroll 40 character or less
# advertisements on an axis webcam
#

SETTEXT = "%H:%M:%S - Slic Network Solutions"

HTTPUSER = `cat .ax-username`.chomp
HTTPPASS = `cat .ax-password`.chomp
HTTPHOST = `cat .ax-host`.chomp

ads = []
#ads << "Free Pizza: Little Italy 265-5500"
#ads << "Free Beer: McDuff's 261-4089"
#ads << "Free WiFi: SSID slicnetwork"
ads << "Free Music: Downtown Potsdam"
ads << "Potsdam Summer Fest!"
ads << "Visit us at the Tech Tent on Saturday!"

adtext = ""
adtext = ads.join ' -- '

oldpos = 0
newpos = 40

counter = 0
usertext = ""

loop do
	counter += 1
	if (counter % 10) == 0
		puts "*** Running axwho: #{counter}"
		usercount = `./axwho | grep -c "HTTP.*VIDEO"`
		usertext = "(#{usercount.chomp}) "
		counter = 0
	end
	
	scrollad = adtext[oldpos..newpos]
	scrolltext = "#{SETTEXT} #{usertext} #{scrollad}"
	puts "(#{oldpos}:#{newpos}) #{scrolltext}"

	if adtext.length > newpos
		oldpos += 3
		newpos += 3
	else
		oldpos = 0
		newpos = 40
	end

	cmd = "wget -q -O /dev/null --http-user=#{HTTPUSER} --http-password=#{HTTPPASS} \"http://#{HTTPHOST}/axis-cgi/operator/dynamicoverlay.cgi?action=settext&text=#{scrolltext.chomp}\""
	#p cmd
	`#{cmd}`
	sleep 1
end
