#!/bin/sh

SCRIPT=/home/jtowne/SVN-jontow/scripts/ibm/ibm-cfg-parse.rb
RANCID=/var/rancid/oit/configs
OUTFILE=`mktemp /tmp/ibm-cfg.XXXXX`
ENDLOC=/var/www/html/gdc-tor.txt

if [ ! -f $SCRIPT ]; then
	echo "Script ibm-cfg-parse.rb (SCRIPT) does not exist."
	exit
fi

date +"%Y-%m-%d %H:%M:%S" >$OUTFILE
echo "" >>$OUTFILE
echo "" >>$OUTFILE
ruby $SCRIPT ${RANCID}/10.94.10.10 >>$OUTFILE
echo "" >>$OUTFILE
ruby $SCRIPT ${RANCID}/10.94.10.20 >>$OUTFILE
echo "" >>$OUTFILE
ruby $SCRIPT ${RANCID}/10.94.10.30 >>$OUTFILE
echo "" >>$OUTFILE
ruby $SCRIPT ${RANCID}/10.94.10.50 >>$OUTFILE
echo "" >>$OUTFILE
ruby $SCRIPT ${RANCID}/10.94.10.60 >>$OUTFILE
echo "" >>$OUTFILE
ruby $SCRIPT ${RANCID}/10.94.20.20 >>$OUTFILE
echo "" >>$OUTFILE
ruby $SCRIPT ${RANCID}/10.94.20.30 >>$OUTFILE
echo "" >>$OUTFILE
ruby $SCRIPT ${RANCID}/10.94.20.40 >>$OUTFILE

if [ -f $ENDLOC ]; then
	cp $OUTFILE $ENDLOC
else
	echo "ENDLOC $ENCLOC does not exist: create for safety."
fi

rm $OUTFILE
