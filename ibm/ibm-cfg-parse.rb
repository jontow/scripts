#!/usr/bin/env ruby
#
# 2014-10-16 -- jtowne@
#
# Parse IBM/BNT RackSwitch G8052 configs (very cisco-ish) to get
# interface description, config and vlans
#
################################################################################

if ARGV[0].nil? or ARGV[0].empty?
	puts "Syntax: ibm-cfg-parse <switchconfigfile.txt>"
	exit
end

@cfgfile = ARGV[0]

if !File.exists?(@cfgfile)
	puts "Config '#{@cfgfile}' does not exist."
	exit
end

# First, suck in entire config file to an array for
# manipulation and seek-forward reasons
@rawlines = []
File.open(@cfgfile, 'r').each do |line|
	# Skip config comments
	next if line =~ /^!/

	@rawlines << line.chomp
end

# Setup some scoping for prerequisites and persistant data
@hostname = ""
@ifhash = {}
@vlanhash = {}
count = 0
@rawlines.each do |line|
	cfghash = nil
	tmpcount = 0

	# Grab hostname of the device
	if line =~ /^hostname (.*)$/
		@hostname = $1.gsub(/"/, '')
	end

	# Let's work on interface configs:
	if line =~ /^interface port (.*)/
		# Save port name from regexp match that got us here
		port = $1

		# If we've got a previous config for this port, don't override it
		# with a [blank] new copy, keep it and add to it.
		# IBM quirk: ports can show up more than once if nonstandard config
		# is present (LACP/PortChannel LAG config, for example)
		if @ifhash[port].nil?
			cfghash = {"desc" => "", "mode" => "", "vlans" => ""}
		else
			cfghash = @ifhash[port]
		end

		# Now look ahead in the config by a calculated amount (line at a time),
		# attempting to pull just the relevant port config out, since we can't rely
		# on valid terminators being present, we use the available "\t" (tab):
		# if it isn't there all of a sudden, we're in a new config block.
		done = false
		tmpcount = count
		#puts "Found port #{port}"
		until done
			tmpcount += 1
			# If we no longer have a tab, we're done
			if @rawlines[tmpcount] =~ /^[^\t]/
				done = true
				next
			# If we see 'exit' (may not be there), skip it
			elsif @rawlines[tmpcount] =~ /^\texit/
				next
			# Otherwise, we're inside the config block and are interested:
			else
				# XXX: Note that this currently ignores many port configs, including
				# native vlan/pvid, LACP settings, etc.  These can be added later.

				# Save some typing later
				rl = @rawlines[tmpcount].gsub(/\t/, '')

				# Get the description, if we see it
				if rl =~ /description (.*)/
					cfghash["desc"] = $1.gsub(/"/, '')
				end

				# Found a trunk port [doesn't contain all attributes..]
				if rl == "switchport mode trunk"
					cfghash["mode"] = "TRUNK"
				end

				# Found trunk allowed vlans, but could find more!
				if rl =~ /switchport trunk allowed vlan ([0-9,-]+)/
					cfghash["vlans"] = $1
				end

				# Tricky part: these switches, much like Cisco switches, will split
				# apart an extremely long "allowed vlan" line into multiples, using 'add'
				# to chain them for display.  Parse this, substitute a comma for "add" and
				# *APPEND* to the existing vlan list
				if rl =~ /switchport trunk allowed vlan add ([0-9,-]+)/
					cfghash["vlans"] += "," + $1
				end

				# Boring old access port, set mode and the single vlan
				if rl =~ /switchport access vlan (\d+)/
					cfghash["mode"] = "ACCESS"
					cfghash["vlans"] = $1
				end

				# Catalog status of shutdown ports as shutdown, do it at
				# the end so that it overwrites any other mode (shutdown is
				# a hard state, no matter what other config is present)
				if rl == "shutdown"
					cfghash["mode"] = "SHUTDOWN"
				end
			end
		end
		#p @rawlines[count]

		# Copy generated cfghash into permanent location
		@ifhash[port] = cfghash
	end

	# Now, lets work on vlan configs: use the same look-ahead technique that we
	# used with interface configs.
	if line =~ /^vlan (\d+)/
		cfghash = {"name" => ""}
		vlan = $1
		#puts "Found VLAN #{vlan}"
		done = false
		tmpcount = count
		until done
			tmpcount += 1
			# If we no longer have a tab, we're done
			if @rawlines[tmpcount] =~ /^[^\t]/
				done = true
				next
			# If we see 'exit' (may not be there), skip it
			elsif @rawlines[tmpcount] =~ /^\texit/
				next
			else
				# Save some typing later
				rl = @rawlines[tmpcount].gsub(/\t/, '')

				# VLAN name, keep it: should be the only thing we need.
				if rl =~ /name (.*)/
					cfghash["name"] = $1.gsub(/"/, '')
				end
			end
		end
		#p @rawlines[count]

		# Copy generated cfghash into permanent location
		@vlanhash[vlan] = cfghash
	end

	count += 1
end

# Nice little hack to put a hostname tagged separator in.. used again later.
printf "%-80s\n", "#{@hostname}:--------------------------------------------------------------------------------"

# Create new [sorted] hash sorting correctly by numeric and alphabetic
# criteria: if the key is a digit, convert it to an integer and sort
# it first; otherwise sort second.
@sortedifhash = @ifhash.sort_by do |k,v|
	if k =~ /^\d+$/
		[1, $&.to_i]
	else
		[2, k]
	end
end

@sortedifhash.each do |k,v|
	#printf "%-12s %-16s %-42s %-8s(%s)\n", @hostname, "PORT #{k}", @ifhash[k]["desc"], @ifhash[k]["mode"], @ifhash[k]["vlans"]
	printf "%-18s %-42s %-8s(%s)\n", "#{@hostname}:#{k}", @ifhash[k]["desc"], @ifhash[k]["mode"], @ifhash[k]["vlans"]
end

printf "%-80s\n", "#{@hostname}:--------------------------------------------------------------------------------"

# See comment above about hash sorting.
@sortedvlanhash = @vlanhash.sort_by do |k,v|
	if k =~ /^\d+/
		[1, $&.to_i]
	else
		[2, k]
	end
end

@sortedvlanhash.each do |k,v|
	printf "%-18s %-24s %-8s\n", "#{@hostname}:VLAN", @vlanhash[k]["name"], k
end

#p @ifhash
#p @vlanhash
