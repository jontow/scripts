#!/usr/bin/env ruby
#
# 2012-09-20 -- jontow@slic.com
#
# Given input file, separated by colons, choose random item from
# each line.
#

MAX_RAND = 60
DEBUG = false

if ARGV[0].nil? or ARGV[0].empty?
	puts "Syntax: rand-pick <inputfile> [-n]"
	exit
end

INPUTFILE = ARGV[0]

class Array
  def random_element
	self[rand(length)]
  end
end

File.open(INPUTFILE, 'r').each do |line|
	next if line.chomp =~ /^#/
	next if line.chomp =~ /^$/
	choices = line.chomp.split(':')
	if DEBUG
		puts line.chomp + " --> " + choices.random_element
	else
		puts choices.random_element
	end
end

# Also, generate a random number in case of a tiebreaker field:
puts rand(MAX_RAND) if ARGV[1] == "-n"
