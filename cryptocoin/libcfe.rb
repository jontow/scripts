#!/usr/bin/env ruby
################################################################################
#
# 2014-03-02 -- jontow@mototowne.com
#
# Frontend for Cryptsy API, uses cryptsy-api gem.
#
################################################################################

require 'cryptsy/api'
require 'net/smtp'

################################################################################

class CryptsyFE
  attr_accessor :cfg_loaded, :c_api, :cfgdebug, :apiurl, :pubkey, :prvkey
  attr_accessor :markets, :interval, :defaultinterval, :selltargetlist, :buytargetlist
  attr_accessor :lastalert, :time_between_alerts

  #
  # New Class instance, setup the defaults, load the config file.
  #
  # RETURNS: nothing (at least, not intentionally)
  #
  def initialize(cfgfile)
	# @cfgdebug is used to troubleshoot loading of the config file.
	@cfgdebug = false

	@apiurl = ""
	@pubkey = ""
	@prvkey = ""
	@markets = ""
	@interval = 0
	@defaultinterval = 0
	@lastalert = 0
	@time_between_alerts = nil

	@color_rise = ""
	@color_fall = ""
	@end_color = ""

	@cfg_loaded = false
	@c_api = nil

	load_config(cfgfile)

	if cfg_loaded?
		@c_api = Cryptsy::API::Client.new(@pubkey, @prvkey)
	end
  end

  #
  # Shortcut method for config load status
  #
  # RETURNS: true/false
  #
  def cfg_loaded?
	@cfg_loaded
  end

  #
  # Load supplied configuration file: uses eval() which is less than ideal.
  #
  # Would be nice to redo this in a more sane way that allows for better
  # error/sanity checking...
  #
  # RETURNS: true/false
  #
  def load_config(cfgfile)
	if File.exists?(cfgfile)
		File.open(cfgfile, 'r').each do |cfgline|
			next if cfgline.chomp =~ /^#/
			next if cfgline.chomp =~ /^$/

			# Suck in the line, *run* it.. don't screw up, now.
			eval(cfgline.chomp)
			p cfgline if @cfgdebug
		end
	end

	# Verify that we have a few required values from the config file,
	# or else we best fail and complain.
	if @apiurl.nil? or
	   @pubkey.nil? or
	   @prvkey.nil? or
	   @markets.nil? or
	   @interval.nil?
		STDERR.puts "Missing config data."
        	STDERR.puts "Bailing: failed to load config."
		return false
	else
		@cfg_loaded = true
        	STDERR.puts "Loaded config." if @debug
		return true
	end
  end

  #
  # Send an alert via email, designed to be SMS-via-email
  #
  # RETURNS: true/false
  #
  def send_alert(msgtxt)
	if @emailfrom.nil? or @emailto.nil?
		return false
	end

	if @smtp_server.nil?
		puts "VERBOSE: missing SMTP server (@smtp_server) from config file" if @verbose
		return false
	end

	# Initialize these to awful values:
	# be evil and send alerts all the time if no blackout times configured.
	start_alerts = Time.now - 86400
	stop_alerts = Time.now + 86400

	# Build valid time objects for blackout times, if configured.
	if !@time_start_alerts.nil? and !@time_stop_alerts.nil?
		now = Time.now
		start_alerts = Time.mktime(now.strftime("%Y"), now.strftime("%m"), now.strftime("%d"),
			@time_start_alerts.split(':', 2)[0], @time_start_alerts.split(':', 2)[1]) 
		stop_alerts = Time.mktime(now.strftime("%Y"), now.strftime("%m"), now.strftime("%d"),
			@time_stop_alerts.split(':', 2)[0], @time_stop_alerts.split(':', 2)[1]) 
	end

	if (now > start_alerts) and (now < stop_alerts)
		# Note, this requires a running/valid SMTP server.
		# This should not be an outrageous requirement on a decent host.
		Net::SMTP::start(@smtp_server, 25, @smtp_server) do |smtp|
			smtp.open_message_stream(@emailfrom, [@emailto]) do |stream|
				# Allow custom alert subject if configured, or default it.
				if @alert_subject.nil?
					stream.puts "Subject: CFE ALERT"
				else
					stream.puts "Subject: #{alert_subject}"
				end
				stream.puts
				stream.puts msgtxt
			end
		end

		# Print some confirmation here, so we know we're in the window.
		puts "*** ALERT MAIL SENT."
	else
		puts "*** OUTSIDE ALERT WINDOW, NOT ALERTING."
	end

	return true
  end

  ################################################################################
  #
  # Print status line: if you modify this, or livemon() output, you need
  # to make sure you keep the format strings intact, or it'll be garbage.
  #
  # RETURNS: nothing
  #
  def livemon_printstat
	puts Time.now.strftime("%Y-%m-%d %H:%M:%S")
        printf "%-10s %-10s %-25s %-18s %-18s %-22s %-18s\n", "label", "risefall", "last_trade", "low_trade", "high_trade", "current_volume", "target"
  end

  #
  # Live monitor: fetch and print price info on configured coins/markets.
  #
  # RETURNS: nothing (infinite loop)
  #
  def livemon
	# XXX: MAGIC NUMBER ALERT, didn't feel this needed a config item.
	# Number of runs before status header is printed
	@statruns = 3

	# transient storage :)
	@defaultinterval = @interval
	count = @statruns
	missed = 0
	lasttrade = {}

	# Now hang out in a fetch-process-print loop, indefinitely.
	loop do
		# Print status line if appropriate
	        if count == @statruns and missed == 0
			livemon_printstat
			count -= 1
		elsif count == 0
			count = @statruns
		else
			count -= 1
		end

		# Grab data in bulk, continuing if we get garbage results.
		begin
			jsblock = @c_api.getmarkets
		rescue => e
			#puts e.message
			missed += 1     # Increment 'missed' count
			print "."
			@interval /= 5  # Shorten interval due to failure
			count = @statruns # Reset counter
		else
			# Report on garbage results, if found.
			if missed > 0
				print "-> #{missed} broken runs\n"
				missed = 0
				livemon_printstat
			end

			# Now reset interval to normal value
			if @interval != @defaultinterval
				@interval = @defaultinterval
			end

			if !jsblock['return'].nil?
				jsblock['return'].each do |mkt|
					p mkt['label'] if @debug
					p mkt if @reallydebug

					risefall = ""
					if @markets.include?(mkt['label'])
						if lasttrade[mkt['label']].nil?
							lasttrade[mkt['label']] = mkt['last_trade']
							risefall = ""
						else
							# Determine price rise/fall status
							if mkt['last_trade'].to_f > lasttrade[mkt['label']].to_f
								risefall = "#{@color_rise}RISE#{@end_color}"
							elsif mkt['last_trade'].to_f < lasttrade[mkt['label']].to_f
								risefall = "#{@color_fall}FALL#{@end_color}"
							else
								# Price stayed the same, elminate visual pollution
								# by simply not printing anything.
								risefall = ""
							end
						end
						# Here's a good place to decide if its a buy or sell target
						target = at_sell_target?(mkt['label'], mkt['last_trade'])

						# Match these format specifiers to livemon_printstat() or go crazy.
						printf "%-10s ", mkt['label']
						printf "%-10s ", risefall
						printf "%-25s ", mkt['last_trade']
						printf "%-18s ", mkt['low_trade']
						printf "%-18s ", mkt['high_trade']
						printf "%-22s ", "%.4f" % mkt['current_volume']

						# "%.Nf" % float -- neat trick to suppress E format, and
						# limit decimal to N places: same as sprintf format.
						if !target.nil?
							printf "%-18s\n", "%.8f" % target
						else
							printf "\n"
						end

						# Save last trade for rise/fall comparison on next run.
						lasttrade[mkt['label']] = mkt['last_trade']

					end

				end
			end
		end

		puts ""
		sleep @interval.to_i

	end
  end

  ################################################################################
  #
  # Get last market trade
  #
  # RETURNS: float (last trade price), or nil on failure
  #
  def getlasttrade(market)
	begin
		jsblock = @c_api.getmarkets
	rescue => e
		return nil
	end

	if !jsblock['return'].nil?
		jsblock['return'].each do |mkt|
			if mkt['label'] == market
				return mkt['last_trade'].to_f
			end
		end
	end
  end

  ################################################################################
  #
  # Get all of my outstanding orders
  #
  # RETURNS: true on success, nil on failure
  def allorders
	begin
		jsblock = @c_api.allmyorders['return']
	rescue => e
		return nil
	end

	printf "%-5s %-20s %-10s %-14s %-14s %-14s\n", "Type", "Date", "Market", "Remaining", "Original", "Price" 
	jsblock.each do |order|
		printf "%-5s %-20s %-10s %-14s %-14s %-14s\n",
		   order['ordertype'], order['created'], order['marketid'], order['quantity'],
		   order['orig_quantity'], order['price']
	end

	return true
  end

  ################################################################################
  #
  # Translate market label to market id number
  #
  # RETURNS: integer (market id number), or false/nil on failure
  #
  def market_label_to_id(market)
	if market.nil?
		return false
	end

	begin
		jsblock = @c_api.getmarkets
	rescue => e
		return nil
	end

	if !jsblock['return'].nil?
		jsblock['return'].each do |mkt|
			p mkt if @reallydebug

			if mkt['label'] == market
				p mkt['marketid'] if @debug
				return mkt['marketid']
			end
		end
	end
  end

  ################################################################################
  #
  # Get trades for given market, with optional limit
  #
  # RETURNS: array of trades, or nil on failure
  #
  def get_mkt_trades(market, limit=nil)
	if market.nil?
		puts "VERBOSE: market not specified" if @verbose
		return nil
	end

	# Translate our label to an id, markettrades() takes id only
	mktid = market_label_to_id(market)

	# Returned JSON looks like this:
	#["success", "1"]
	#["return", [
	#  {
	#    "tradeid"=>"28617761",
	#    "datetime"=>"2014-03-06 18:50:47",
	#    "tradeprice"=>"0.00000555",
	#    "quantity"=>"16.24965676",
	#    "total"=>"0.00009019",
	#    "initiate_ordertype"=>"Sell"
	#  },  #(repeat {})
	#]]
	begin
		jsblock = @c_api.markettrades(mktid)
	rescue => e
		return nil
	end

	if !jsblock.nil? and jsblock['success'] != '1'
		puts "VERBOSE: get_mkt_trades(): query failed." if @verbose
		return nil
	end

	count = 0
	tradearray = []
	jsblock['return'].each do |trade|
		count += 1

		# Accumulate trade into return array
		tradearray << trade

		# Allow early exit after 'limit' trades
		if !limit.nil? and limit >= count
			puts "DEBUG: Hit limit (#{count}/#{limit})" if @debug
			return tradearray
		end

		p trade if @reallydebug
	end

	return tradearray
  end

  ################################################################################
  #
  # Calculate good "BUY" price, using live market data
  #
  # RETURNS: true, as this is just a stub for now.
  #
  def calc_good_buy_price(market)
	puts "calc_good_buy_price(): stub"

	return true
  end

  #
  # Calculate good "SELL" price, using live market data
  #
  # RETURNS: array [qty, good_price], or false/nil on failure
  #
  def calc_good_sell_price(market)
	if market.nil?
		return false
	end

	##################################################
	#
	# Calculate available coin quantity
	#

	# 'market' must be formatted like ABC/BTC
	coin = market.split('/', 2)[0]
	qty_own = nil

	begin
		jsblock = @c_api.getinfo
	rescue => e
		return nil
	end

	jsblock.each do |info|
		if info[0] == 'return'
			if info[1]['balances_available'][coin]
				p info[1]['balances_available'][coin] if @debug
				qty_own = info[1]['balances_available'][coin].to_f
			end
		end
	end

	# Save some cycles by exiting early if we don't have these
	if qty_own == 0.0
		puts "VERBOSE: No balance of #{market}" if @verbose
		return nil
	end

	# Translate our label to an id, markettrades() takes id only
	mktid = market_label_to_id(market)

	##################################################
	#
	# Calculate my last trades for this coin: currently
	# relies on cryptsy sending them in date-descending
	# order, which is specified per the API docs.
	#

	tx_list = nil
	jsblock = ""

	# XXX: MAGIC NUMBER ALERT: defaulting to assumed 'sane' value
	# second parameter to c.mytrades(x, y) is a limit on returned items.
	begin
		if !@use_last_transactions.nil?
			jsblock = @c_api.mytrades(mktid, @use_last_transactions)
		else
			jsblock = @c_api.mytrades(mktid, 50)
		end
	rescue => e
		return nil
	end

	result = parse_trades_sell(jsblock, "Buy", qty_own)
	#result[0] == tx_list
	#result[1] == qty_traded
	if !result.nil?
		tx_list = result[0]
		qty_traded = result[1]
	end

	# We still have jsblock as 'mytrades', reuse it instead of requerying.
	if qty_traded == 0.0
		result = parse_trades_sell(jsblock, "Sell", qty_own)
		#result[0] == tx_list
		#result[1] == qty_traded

		if !result.nil?
			tx_list = result[0]
			qty_traded = result[1]
		end
	end

	if qty_traded == 0.0 or tx_list.nil?
		puts "VERBOSE: No buy or sell transactions to use.  Cannot autotarget for #{market}." if @verbose
		return nil
	end

	print "ENDLOOP DEBUG: " if @debug
	p tx_list if @debug

	##########################################################################################
	#
	# Weighted average math, interactive explanation:
	# first multiple each tx together: price * qty, accumulate results into price_accum
	# then add each qty together: qty1 + qty2 + qty3 + qty4 = qty_accum
	# finally, divide price_accum by qty_accum, end with a weighted average price.
	#
	#irb(main):001:0> 0.00082987 * 0.53
	#=> 0.0004398311
	#irb(main):002:0> 0.00083579 * 1.0
	#=> 0.00083579
	#irb(main):003:0> 0.00095165 * 1.06376090
	#=> 0.001012328060485
	#irb(main):004:0> 0.00095166 * 0.32623910
	#=> 0.000310468701906
	#irb(main):005:0> 0.0004398311 +  0.00083579 + 0.001012328060485 + 0.000310468701906
	#=> 0.002598417862391
	#irb(main):006:0> 0.53 + 1.0 + 1.06376090 + 0.32623910
	#=> 2.92
	#irb(main):007:0> 0.002598417862391 / 2.92
	#=> 0.000889869130955822
	##########################################################################################

	price_accum = 0.0
	qty_accum = 0.0
	tx_list.each do |tx|
		tx_qty = tx[0]
		tx_price = tx[1]
		puts "VERBOSE: #{tx_qty} bought at #{tx_price}" if @verbose

		price_accum += (tx_price * tx_qty)
		qty_accum += tx_qty
	end

	puts "PRICE ACCUM: #{price_accum}" if @debug
	puts "QTY ACCUM:   #{qty_accum}" if @debug
	good_price = price_accum / qty_accum

	if !@sell_tx_fee.nil?
		puts "VERBOSE: Sell TX fee: #{good_price * @sell_tx_fee}" if @verbose
		good_price += (good_price * @sell_tx_fee)
	end

	puts "GOOD PRICE: #{good_price}" if @debug

	return [qty_accum, good_price]
  end

  #
  # Parse 'trades' list for given tradetype
  #
  # RETURNS:
  #    result[0] == tx_list
  #    result[1] == qty_traded
  # 
  def parse_trades_sell(jsblock, tradetype, qty_own)
	qty_traded = 0.0
	tx_list = []

	jsblock.each do |trade|
		if trade[0] == 'return'
			trade[1].each do |tx|
				p tx if @reallydebug
				if tx['tradetype'] == tradetype
					puts "#{tx['datetime']}: #{tx['quantity']} @ #{tx['tradeprice']} == #{tx['total']}" if @debug
					puts "qty_own (#{qty_own.to_s}) // qty_traded (#{qty_traded.to_s})" if @debug

					# if this transaction contains less qty than we own, and our buy-accumulator
					# isn't overflowing our balance, add this tx verbatim
					if (tx['quantity'].to_f <= (qty_own - qty_traded)) and (qty_traded < qty_own)
						puts "if--1" if @reallydebug
						tx_list << [tx['quantity'].to_f, tx['tradeprice'].to_f]
						qty_traded += tx['quantity'].to_f
					# if this transaction contains less qty than we own, and our buy-accumulator
					# will overflow, chop it into pieces.
					elsif (tx['quantity'].to_f <= qty_own) and ((tx['quantity'].to_f + qty_traded) > qty_own)
						puts "if--2" if @reallydebug
						qty_add = qty_own - qty_traded
						tx_list << [qty_add, tx['tradeprice'].to_f]
						qty_traded += qty_add
					# if transaction contains more qty than we own, but we still have
					# room in the buy-accumulator, see if we can make some valid numbers from
					# it by chopping it into pieces.
					elsif (tx['quantity'].to_f > qty_own) and (qty_traded < qty_own)
						puts "if--3" if @reallydebug
						qty_add = qty_own - qty_traded
						tx_list << [qty_add, tx['tradeprice'].to_f]
						qty_traded += qty_add
					# otherwise, we should have found the case where transaction contains
					# more than we own, but buy accumulator is also full
					else
						puts "if--4" if @reallydebug
						next
					end

					p tx_list if @reallydebug
				end
			end
		end
	end

	return [tx_list, qty_traded]
  end

  ################################################################################
  #
  # Calculate target 'BUY' price
  #
  # RETURNS: true, as this is a stub.
  #
  def calc_buy_target(market)
	puts "calc_buy_target(): stub"
	return true
  end

  #
  # Determine whether we're at the 'BUY' target price
  #
  # RETURNS: true, as this is a stub.
  #
  def at_buy_target?(market)
	puts "at_buy_target?(): stub"
	return true
  end

  ################################################################################
  #
  # Calculate target 'SELL' price
  #
  # RETURNS: float (target price), or nil on failure
  #
  def calc_sell_target(market)
	result = calc_good_sell_price(market)

	if result.nil?
		puts "GOODPRICE: nothing to sell, or no Buy found in tx list." if @debug
		return nil
        else
                puts "GOODPRICE: Sell #{result[0]} above #{result[1]}" if @verbose
        end

	qty = result[0]
	price = result[1]

	puts "DEBUG: BEFORE AUTOTARGET #{price}" if @debug
	if @targetpercent.nil?
		# default to +5% if not specified
		price += (price * 0.05)
	else
		price += (price * @targetpercent)
	end
	puts "DEBUG: AFTER AUTOTARGET #{price}" if @debug

	return price
  end

  #
  # Determine whether we're at the 'SELL' target price
  #
  # RETURNS: float (target price), or nil on failure
  #
  def at_sell_target?(market, current)
	target = 0.0

	if !@autocalc_sell_target.nil? and @autocalc_sell_target
		target = calc_sell_target(market)
		if target.nil?
			puts "ERROR: calc_sell_target() failed" if @verbose
			return nil
		end
	else
		if !@selltargetlist.nil? and !@selltargetlist[market].nil?
			target = @selltargetlist[market]
		else
			puts "ERROR: No such market: #{market}" if @verbose
			return nil
		end
	end

	puts "DEBUG: #{target}" if @debug

	if current.to_f >= target.to_f
		# Send an alert if we've hit it big, etc
		alert = "*** ALERT: #{market} price at #{current}, hit target #{target}"
		puts alert

		# Give us an annoying default, should we not have it in the config.
		if @time_between_alerts.nil?
			@time_between_alerts = @interval
		end

		# send a mail
		if @lastalert > 0 and @lastalert < @time_between_alerts
			@lastalert += @interval
			puts "@lastalert = #{@lastalert} (#{@time_between_alerts})" #if @debug
		else
			if send_alert(alert)
				# Reset when last alert was sent
				@lastalert = 1
			else
				# Note: don't reset when last alert was sent, this is an error: resend.
				puts "*** ALERT MAIL ERROR."
			end
		end
	end

	return target
  end

  ################################################################################
  #
  # Fancy math to determine which is more profitable: selling a coin directly for
  # its current BTC value, or selling first for its LTC value, then selling the
  # resulting LTC for BTC.  Takes into account sell transaction fees, if properly
  # configured.
  #
  # RETURNS: string (formatted answer), or nil on failure.
  #
  def sell_as_btc_or_ltc?(market, qty=1.0)
	if market.nil?
		return nil
	end

	# FST/BTC, take just the 'FST' part
	coin = market.split('/', 2)[0]

	coin_btc_price = getlasttrade(coin + "/BTC")
	coin_ltc_price = getlasttrade(coin + "/LTC")
	# Now get our conversion price
	ltc_btc_price = getlasttrade("LTC/BTC")

	if coin_btc_price.nil? or coin_ltc_price.nil?
		puts "VERBOSE: missing either BTC or LTC market for #{coin}" if @verbose
		return nil
	end

	puts "#{coin}: #{"%.8f" % coin_btc_price} / #{"%.8f" % coin_ltc_price} / #{"%.8f" % ltc_btc_price}" if @debug

	btc_profit = (qty.to_f * coin_btc_price)
	ltc_profit = (qty.to_f * coin_ltc_price)

	if !@sell_tx_fee.nil?
		btc_profit -= (btc_profit * @sell_tx_fee)
		ltc_profit -= (ltc_profit * @sell_tx_fee)
		convert_profit = (ltc_profit * ltc_btc_price)
		convert_profit -= (convert_profit * @sell_tx_fee)
	else
		convert_profit = (ltc_profit * ltc_btc_price)
	end

	puts "#{coin}: AS_BTC:#{"%.8f" % btc_profit} / AS_LTC:#{"%.8f" % convert_profit}" #if @verbose

	if btc_profit > convert_profit
		return "Sell as BTC directly, #{"%.8f" % (btc_profit - convert_profit)} margin"
	else
		return "Sell as LTC, convert to BTC, #{"%.8f" % (convert_profit - btc_profit)} margin"
	end
  end

  ################################################################################

end
