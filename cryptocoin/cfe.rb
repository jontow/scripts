#!/usr/bin/env ruby
################################################################################
#
# 2014-03-02 -- jontow@mototowne.com
#
# Frontend for Cryptsy API, uses cryptsy-api gem.
#
################################################################################

require './libcfe'

################################################################################

c = CryptsyFE.new('cryptsy.cfg.rb')

case ARGV[0]

# ARGS: None
when "livemon"
	c.livemon

# ARGS: market refprice
when "pricediff", !ARGV[2].nil?
	price = c.getlasttrade(ARGV[1])
	refprice = ARGV[2].to_f
	
	# Get percent difference in price
	#((now - orig).to_f / orig) * 100

	diff = ((price - refprice) / refprice) * 100
	puts "Market: #{ARGV[1]}"
	puts "Reference Price: #{refprice}"
	puts "Current Price:   #{price}"
	printf "Difference:      %.2f%%\n", diff
	exit

when "myorders"
	c.allorders
	exit

when "goodprice", !ARGV[1].nil?
	result = c.calc_good_sell_price(ARGV[1])
	if result.nil?
		puts "GOODPRICE: nothing to sell, or no Buy found in tx list."
	else
		puts "GOODPRICE: Sell #{result[0]} above #{"%.8f" % result[1]}"
	end
	exit

when "mkttrades", !ARGV[1].nil?
	result = c.get_mkt_trades(ARGV[1])
	if result.nil?
		puts "MKTTRADES: none returned."
	else
		puts "MKTTRADES: "
		p result
	end
	exit
when "get_last_trade", !ARGV[1].nil?
	result = c.get_mkt_trades(ARGV[1], 1)
	if result.nil?
		puts "MKTTRADES: none returned."
	else
		puts "MKTTRADES: "
		p result
	end
	exit

when "sell_as_btc_or_ltc", !ARGV[2].nil?
	result = c.sell_as_btc_or_ltc?(ARGV[1], ARGV[2])
	puts result
	exit

else
	puts ""
	puts "Syntax: cryptsy-fe.rb <operation> [arguments]"
	puts "  Operations:"
	puts "     livemon                   (no arguments)"
	puts "     myorders                  (no arguments)"
	puts "     pricediff <marketlabel> <refprice>"
	puts "     goodprice <marketlabel>"
	puts "     mkttrades <marketlabel>"
	puts "     get_last_trade <marketlabel>"
	puts "     sell_as_btc_or_ltc <marketlabel> <qty>"
	puts ""
	puts "  Detail:"
	puts "     <marketlabel>: ABC/BTC"
	puts "     <refprice>: 0.00001 or 1.0e-05"
	puts ""
	exit
end

