################################################################################
#
# 2014-02-18 -- jontow@mototowne.com
#
# Sample configuration for ruby/cryptsy API clients
# Syntax is the same as Ruby.
#
################################################################################

# Verbose/Debug variables.. use as necessary
# BOOLEAN
@verbose = true
@debug = false
@reallydebug = false

# This URL obtained from site, shouldn't change
# STRING
@apiurl="https://api.cryptsy.com/api"

# Your public key: generated from settings page
# STRING
@pubkey="ffffffffffffffffffffffffffffffffffffffff"

# Your private key: generated from settings page
# STRING
@prvkey="00000000000000000000000000000000000000000000000000000000000000000000000000000000"

################################################################################
#
# Output (Visual) Config
#

# ANSI Color code for 'falling' tag, comment after ends color
# STRING (ANSI escape sequence)
@color_fall="[1;31m" #[0;0m

# ANSI Color code for 'rising' tag, comment after ends color
# STRING (ANSI escape sequence)
@color_rise="[1;32m" #[0;0m

# End color
# STRING (ANSI escape sequence)
@end_color="[0;0m"

################################################################################
#
# Market Data
#

# Markets to watch: labels, comma separated
# ARRAY OF STRINGS
@markets=["FST/BTC","CGB/BTC","DOGE/BTC"]

# Current Cryptsy "Buy" transaction fee
# FLOAT (percent)
@buy_tx_fee=0.002

# Current Cryptsy "Sell" transaction fee
# FLOAT (percent)
@sell_tx_fee=0.003

# Use this many of your last transactions (trades) during calculations
# INTEGER
@use_last_transactions=50

################################################################################
#
# Alert Section
#

# Target prices to alert on, must be all on one line
# HASH OF {'label'=>'price'}
#@targetlist={ 'FST/BTC'=>0.000007, 'CGB/BTC'=>0.0008, 'DOGE/BTC'=>0.0000018 }

# Auto-target sell price using goodprice()?
# BOOLEAN
@autocalc_sell_target=true

# Define percent price increase for targeting
# FLOAT (percent)
@targetpercent=0.05

# Email address alerts are marked 'From:'
# STRING
#@emailfrom = "cfe.alerts@example.com"

# Email address alerts are destined 'To:'
# STRING
#@emailto = "me@example.com"

# SMTP server address
# STRING
@smtp_server = "localhost"

# Alert subject, if desired (defaults to "CFE ALERT")
# STRING
#@alert_subject = "CFE ALERT"

################################################################################
#
# Timing Settings
#

# Interval between runs
# INTEGER
@interval=300

# Interval between alerts
# INTEGER
@time_between_alerts=3600

# Time of day to start alerting (format: strftime style HH:MM)
# STRING
@time_start_alerts="06:30"

# Time of day to stop alerting (format: strftime style HH:MM)
# STRING
@time_stop_alerts="22:30"
