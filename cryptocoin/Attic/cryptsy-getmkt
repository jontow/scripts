#!/usr/bin/env ruby1.9.1
#
# 2014-02-18 -- jontow@mototowne.com
#
# Uses cryptsy.com public (unauthenticated) API to watch status of a particular
# market, in a loop.
#

require 'net/http'
require 'json'
require 'uri'

@uri = 'http://pubapi.cryptsy.com/api.php?method=singlemarketdata&marketid='
@defaultmarket = '44'

# Set these two values the same: in seconds
@defaultinterval = 300
@interval = 300

# Number of lines before status/labels are printed again
@statlines = 10

if ARGV[0].nil?
	@uri += @defaultmarket
else
	@uri += ARGV[0]
end

if !ARGV[1].nil?
	@defaultinterval = ARGV[1]
	@interval = ARGV[1]
end

def printstat
	printf "%-10s %-25s %-25s %-20s\n", "label", "lasttradeprice", "lasttradetime", "volume"
end

count = @statlines
missed = 0
loop do
	if count == @statlines and missed == 0
		printstat
		count -= 1
	elsif count == 0
		count = @statlines
	else
		count -= 1
	end

	response = Net::HTTP.get(URI.parse(@uri))
	#p response

	begin
		jsblock = JSON.parse(response)
	rescue => e
		#puts e.message
		missed += 1	# Increment 'missed' count
		print "."
		@interval /= 5	# Shorten interval due to failure
		count = @statlines # Reset counter
	else
		if missed > 0
			print "-> #{missed} broken runs\n"
			missed = 0
			printstat
		end

		# Now reset interval to normal value
		if @interval != @defaultinterval
			@interval = @defaultinterval
		end

		if !jsblock['return'].nil?
			printf "%-10s ", jsblock['return']['markets']['FST']['label']
			printf "%-25s ", jsblock['return']['markets']['FST']['lasttradeprice']
			printf "%-25s ", jsblock['return']['markets']['FST']['lasttradetime']
			printf "%-20s\n", jsblock['return']['markets']['FST']['volume']
		end
	end

	sleep @interval.to_i
end
