#!/usr/bin/env ruby

################################################################################
#
# 2014-12-20 -- jontow@zenbsd.net
#
# Quick little game to help a kid with math problems.
# Uses 'figlet' to amuse.
#
################################################################################

@scorefile = ENV['HOME'] + "/.math-scores"
@max = 10
@figletopt = "-f roman"
@name = "unknown"
@total = 0
@score = 0
@operands = {}
@algebra = false

@timestart = Time.now.strftime("%Y-%m-%d %H:%M:%S")

# First argument: 
if !ARGV[0].nil?
	if ARGV[0] == '-h'
		puts "Syntax: mathgame.rb [maxnumber] [mdasx]"
		puts "   Examples:"
		puts "      mathgame.rb 5 as"
		puts "      mathgame.rb 10 mdas"
		puts "      mathgame.rb 5 mx"
		puts "      mathgame.rb 10 mdasx"
		puts ""
		puts "      'mdas' are the common math operands"
		puts "      'x' is algebraic form: solve for x"
		exit
	end

	puts ""
	puts "Largest number to use: #{ARGV[0]}"
	@max = ARGV[0].to_i
end

if !ARGV[1].nil?
	mdasopt = ARGV[1].chomp
	counter = 0

	if mdasopt =~ /m/
		@operands[counter] = '*'
		counter += 1
	end

	if mdasopt =~ /d/
		@operands[counter] = '/'
		counter += 1
	end

	if mdasopt =~ /a/
		@operands[counter] = '+'
		counter += 1
	end

	if mdasopt =~ /s/
		@operands[counter] = '-'
		counter += 1
	end

	if mdasopt =~ /x/
		@algebra = true
	end
else
	@operands = {
		0 => '*',
		1 => '/',
		2 => '+',
		3 => '-'
	}
end

print "Games to play: "
@gamesplayed = ""
@operands.each do |k,v|
	@gamesplayed = "#{@gamesplayed}#{v} "
end

if @algebra
	@gamesplayed = "#{@gamesplayed}algebra"
end

puts @gamesplayed

puts ""

def genproblem
	problemtype = @operands[rand(@operands.length)]

	# Needs to be '+ 1' or its "lt", not "le"
	num1 = rand(@max + 1)
	num2 = rand(@max + 1)

	#
	# Series of special conditions so as to not confuse
	# children with 'weird' division problems..
	#
	# Neither number can be '0', and the first number
	# must be larger than the second.
	#
	if problemtype == '/'
		#puts "DEBUG: division problem"
		while (num1 < num2) or ((num1 == 0) or (num2 == 0)) do
			num1 = rand(@max + 1)
			num2 = rand(@max + 1)
			#puts "DEBUG: num1:#{num1}  num2:#{num2}"
		end
	end

	if problemtype == '-'
		#puts "DEBUG: subtraction problem"
		while (num1 < num2) do
			num1 = rand(@max + 1)
			num2 = rand(@max + 1)
			#puts "DEBUG: num1:#{num1}  num2:#{num2}"
		end
	end

	problem = "#{num1} #{problemtype} #{num2}"
	#puts "DEBUG1: #{problem}"
	answer = eval problem
	#puts "DEBUG2: #{answer}"

	# random boolean
	chance = rand(2) == 0 ? true : false
	if @algebra and chance
		# Don't match the operand (position 1) or the equals (position 3)
		missingpos = rand(5)
		until missingpos != 1 and missingpos != 3 do
			# 5 = position index + 1
			missingpos = rand(5)
		end

		pos = problem.split(' ', 3)
		pos << '='
		pos << answer
		#p pos
		#p missingpos

		answer = pos[missingpos].to_i

		# If missingpos is the last position, use "?" instead of "_"
		if missingpos == 4
			pos[missingpos] = "?"
		else
			pos[missingpos] = "_"
		end

		problem = pos.join(' ')

		[problem, answer]
	else
		[problem + " = ?", answer]
	end
end

def reval(problem, answer, guess)
	#puts "DEBUG: problem = #{problem}"
	#puts "DEBUG: guess = #{guess}"

	if problem =~ /_/
		newproblem = "#{problem.gsub(/_/, guess).gsub(/\ =\ [0-9?]+/, '')}"
		#puts "DEBUG: newproblem = #{newproblem}"

		realanswer = problem.gsub(/^.*\ =\ /, '').to_i
		newanswer = eval newproblem
		#puts "DEBUG: answer = #{answer}"
		#puts "DEBUG: newanswer = #{newanswer}"
		#puts "DEBUG: realanswer = #{realanswer}"
		if newanswer == realanswer
			return true
		else
			return false
		end
	else
		if answer.to_i == guess.to_i
			return true
		end
	end
end

puts ""
print "What is your name?  "
@name = STDIN.gets.chomp
puts ""
puts `figlet #{@figletopt} "Hello #{@name}!"`
puts ""

loop do
	answer = -1
	problem = "unknown"
	while answer < 0
		pset = genproblem
		#p pset
		problem = pset[0]
		answer = pset[1]
	end

	puts ""
	puts `figlet #{@figletopt} "#{problem}"`
	print "(#{@score}/#{@total}) Answer (or 'quit')?  "
	guess = STDIN.gets.chomp

	if guess == "quit"
		puts ""
		puts ""
		puts `figlet #{@figletopt} "Good job\!"`
		puts ""
		puts ""
		puts "Total problems completed: #{@total.to_s}"
		puts "Correct answers: #{@score.to_i}"
		puts "Incorrect answers: #{(@total - @score).to_s}"
		puts ""
		puts ""
		break
	end

	# Since algebra is supported, and ambigous problems may be asked, we need
	# to re-evaluate the problem once we're given an answer.  If it's valid,
	# it should be supported.
	validity = reval(problem, answer, guess)

	if validity
		@score += 1
		puts ""
		puts `figlet #{@figletopt} "Correct\!"`
	else
		puts ""
		puts ""
		puts "Sorry!  Incorrect!"
		puts ""
		puts "The correct answer was: #{answer}"
		puts ""
		puts ""
	end

	@total += 1
end

puts "Saving your score to #{@scorefile}"
f = File.open(@scorefile, 'a')
@timestop = Time.now.strftime("%Y-%m-%d %H:%M:%S")
f.puts "START:#{@timestart}   STOP:#{@timestop}   PLAYER:#{@name}   TOTAL:#{@total}   SCORE:#{@score}   MAX:#{@max}   GAMESPLAYED:#{@gamesplayed}"
f.close

exit
