#!/usr/bin/env ruby
#
# 2008-10-01 -- jontow@
#
################################################################################
#
# CREATE TABLE whatever (
#	Login text,
#	Password text,
#	Name text,
#	CustomerKey text,
#	ServiceType text,
#	Status varchar(1)
# );
#
################################################################################


begin
	require 'mysql'
rescue LoadError
	require 'rubygems'
	require 'mysql'
end

require 'utilsconf.rb'

@mysqldbh = Mysql.real_connect(UtilsConf::DBHOST, UtilsConf::DBUSER, UtilsConf::DBPASS, UtilsConf::DBNAME)

def user_exists? (newtable, customer)
	result = @mysqldbh.query("SELECT Login FROM #{newtable} WHERE Login = '#{@mysqldbh.quote(customer)}'")
	if result.num_rows >= 1
		return true
	else
		return false
	end
end

def port_mysql(dbhost, dbname, dbuser, dbpass, newtable)
	dbh = Mysql.real_connect(dbhost, dbuser, dbpass, dbname)
	result = dbh.query("SELECT Login,Password,Name,CustomerKey,ServiceType,Status FROM ispService WHERE (Status = 'A' OR Status = 'N' OR Status = 'O') AND Login <> '' ORDER BY Login")
	result.each do |row|
		if user_exists?(newtable, row[0])
			#puts "User #{row[0]} already exists.."
		else
			p row
			newresult = @mysqldbh.query("INSERT INTO #{newtable} (Login,Password,Name,CustomerKey,ServiceType,Status) VALUES ('#{@mysqldbh.quote(row[0])}', '#{@mysqldbh.quote(row[1])}', '#{@mysqldbh.quote(row[2])}', '#{row[3]}', '#{@mysqldbh.quote(row[4])}', '#{row[5]}')")
		end
	end
end

port_mysql(UtilsConf::DBHOST, UtilsConf::SLICDB, UtilsConf::OLDDBUSER, UtilsConf::OLDDBPASS, UtilsConf::SLICTABLE)
port_mysql(UtilsConf::DBHOST, UtilsConf::CASTLETVDB, UtilsConf::OLDDBUSER, UtilsConf::OLDDBPASS, UtilsConf::CASTLETVTABLE)
port_mysql(UtilsConf::DBHOST, UtilsConf::CITTELEDB, UtilsConf::OLDDBUSER, UtilsConf::OLDDBPASS, UtilsConf::CITTELETABLE)
port_mysql(UtilsConf::DBHOST, UtilsConf::TOPSTELEDB, UtilsConf::OLDDBUSER, UtilsConf::OLDDBPASS, UtilsConf::TOPSTELETABLE)

exit
