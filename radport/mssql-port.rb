#!/usr/bin/env ruby
#
# 2008-08-21 -- jontow@
#

begin
	require 'dbi'
	require 'mysql'
rescue LoadError
	require 'rubygems'
	require 'dbi'
	require 'mysql'
end

require 'utilsconf.rb'

@mysqldbh = Mysql.real_connect(UtilsConf::DBHOST, UtilsConf::DBUSER, UtilsConf::DBPASS, UtilsConf::DBNAME)

def user_exists? (newtable, customer)
	result = @mysqldbh.query("SELECT Login FROM #{newtable} WHERE Login = '#{@mysqldbh.quote(customer)}'")
	if result.num_rows >= 1
		return true
	else
		return false
	end
end

def port_mssql(dbname, dbuser, dbpass, newtable)
	dbh = DBI.connect("DBI:ODBC:#{dbname}", dbuser, dbpass)
	dbh.select_all("SELECT Login,Password,Name,CustomerKey,ServiceType FROM ispService WHERE Status = 'A' AND Login <> '' ORDER BY Login") do |row|

		if user_exists?(newtable, row[0])
			#puts "User #{row[0]} already exists.."
		else
			p row
			result = @mysqldbh.query("INSERT INTO #{newtable} (Login,Password,Name,CustomerKey,ServiceType) VALUES ('#{@mysqldbh.quote(row[0])}', '#{@mysqldbh.quote(row[1])}', '#{@mysqldbh.quote(row[2])}', '#{row[3]}', '#{@mysqldbh.quote(row[4])}')")
		end
	end
end

port_mssql(UtilsConf::SLICMSDB, UtilsConf::MSDBUSER, UtilsConf::MSDBPASS, UtilsConf::SLICTABLE)
port_mssql(UtilsConf::CASTLETVMSDB, UtilsConf::MSDBUSER, UtilsConf::MSDBPASS, UtilsConf::CASTLETVTABLE)
port_mssql(UtilsConf::CITTELEMSDB, UtilsConf::MSDBUSER, UtilsConf::MSDBPASS, UtilsConf::CITTELETABLE)
port_mssql(UtilsConf::TOPSTELEMSDB, UtilsConf::MSDBUSER, UtilsConf::MSDBPASS, UtilsConf::TOPSTELETABLE)

exit
