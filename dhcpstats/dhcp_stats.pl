#!/usr/bin/perl

#######################################################################
#
# heavily modified between 2004 and 2013 by jontow@zenbsd.net
#
#######################################################################
#                                                                     #
#            Welcome to CSUMB's DHCP Statistics Script                #
#                                                                     #
#  This script was developed internally as a way to monitor our dhcp  #
#  usage. Some of the features in this script may not work properly   #
#  on configurations that vary from our own. Feel free to modify it   #
#  in any way to better fit your needs.                               #
#                                                                     #
#                                                                     #
#  Some Notes:                                                        #
#                                                                     #
#   - If you use pico to edit these files, make sure it was launched  #
#     with the -w option to prevent hard-wrapping of long lines.      #
#                                                                     #
#   - View the README file for installation instructions.             #
#                                                                     #
#   - Network Descriptions are grabbed from the line preceding the    #
#     subnet declaration in dhcpd.conf, so you may need to modify     #
#     your configuration to fit the following format:                 #
#        ## Network Description                                       #
#        subnet 192.168.1.0 netmask 255.255.255.0 {                   #
#                                                                     #
#   - If you have multiple range statements for a subnet, the         #
#     statistics may not be accurate.                                 #
#                                                                     #
#   - The 'Abandoned' column of the web page only reports leases      #
#     with a binding state of Free. These are leases for which the    #
#     lease time has expired without a new request. It is possible    #
#     for a lease to have a binding state of Abandoned, meaning       #
#     that the given ip responded to a ping and the host is most      #
#     likely using a self-assigned static ip. The script in its       #
#     current form does not do any reporting on this state.           #
#                                                                     #
#   This software is released under the MIT license.                  #
#   Copyright (c) 2003 Isaac Davis-King                               #
#                                                                     #
#   Permission is hereby granted, free of charge, to any person       #
#   obtaining a copy of this software and associated documentation    #
#   files (the "Software"), to deal in the Software without           #
#   restriction, including without limitation the rights to use,      #
#   copy, modify, merge, publish, distribute, sublicense, and/or      #
#   sell copies of the Software, and to permit persons to whom the    #
#   Software is furnished to do so, subject to the following          #
#   conditions:                                                       #
#                                                                     #
#   The above copyright notice and this permission notice shall be    #
#   included in all copies or substantial portions of the Software.   #
#                                                                     #
#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,   #
#   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES   #
#   OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND          #
#   NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT      #
#   HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,      #
#   WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING      #
#   FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR     #
#   OTHER DEALINGS IN THE SOFTWARE.                                   #
#                                                                     #
#   http://www.opensource.org/licenses/mit-license.html               #
#                                                                     #
#######################################################################


use Benchmark;
use strict;

#################  Script Configuration  #################

# Debug print config
# 1 = true, 0 = false
my $debug_config = 1;	# VERY verbose, possibly useless
my $debug_math = 1;
my $debug_lease = 0;	# VERY verbose, possibly useless
my $debug_lease_count = 1; # VERY verbose, good detail
my $debug_html = 0;

# Print roaming addresses?
my $print_roamers = 0;

# How many unique MACs to print?
# 0 = don't print, -1 = print all, N = print top N
my $print_unique_macs = 20;

# Path to dhcpd.conf
my $conf_path = "./tmp/dhcpd.conf";
  
# Path to dhcpd.leases
my $leases_path = "./tmp/dhcpd.leases";
  
# Path to vendor definitions
my $vendor_path = "./vendor_descs.txt";

# Path to the highs database
# Usually the working directory for the script should be just fine.
my $db_path = "./highs";

# Title for the html page
my $title = "DHCP Statistics";

# Used to modify the sorting behavior of the network names.
# 'by_num' extracts the numbers from network names
# 'by_abc' sorts purely alphanumerically
# Keep your spacing/tab style consistant, space sorts differently than tab.
# Valid values: 'by_abc' and 'by_num'
my $sort_order = 'by_abc';

# Threshold level (percentage) and color definitions
# Networks that are full up to the given percentage will display the corresponding color.
my %thresholds = ('red' => 100,
                  'orange' => 85,
                  'blue' => 60,
                  'green' => 40 );
                  
my %colors = (    'red' => '#AA0000',
                  'orange' => '#CC6600',
                  'blue' => '#0000AA',
                  'green' => '#008800',
                  'light_grey' => '#BFBFBF',
                  'drk_grey' => '#DFDFDF',
                  'light_blue' => '#88AADD');

# Number of months before roaming leases expire
# The larger this value, the longer it may take the page to load
# The expiration time will always be rounded down the the beginning of the month
# So if the date is 5/26/03 and the expire time is set to 3 months, leases before 2/01/03 will be ignored
my $expire_time = 3;


################# End of Script Configuration #################







# used to track the efficiency of the following code
my $t0 = new Benchmark;


my %vendor_descs;
open VENDORS, $vendor_path or die "Could not open vendor descriptions file: $vendor_path";
while (<VENDORS>)
{
    my $vendor_id = lc(substr($_, 0, 6));
    my $vendor_desc = substr($_, 7);
    
    chomp($vendor_desc);
    
    $vendor_id =~ s/(\w{2})/$1:/g;
    chop $vendor_id;
    
    $vendor_descs{$vendor_id} = $vendor_desc;
}
close VENDORS;

#######################################################
# Gather information about the networks being served
#######################################################

# stores the previously read line
# used to gather the name of the networks
my $prev_line;

# stores the ip of network whose config is currently being read
my $current_network;

# hash, keyed by network ip address
# stores name and range information about each network
my %networks;

print qq(<!--\n) if $debug_html;

print qq(<!-- debug_config\n) if $debug_config or $debug_math;
# read in the dhcpd.conf file
open DHCPDCONF, $conf_path or die "Could not open dhcpd.conf: $conf_path";  
while ( <DHCPDCONF> )
{
   
    # if the line is a subnet declaration gather the network and subnet information
    if ( !/^\s*\#/ and /^\s*subnet (\d+.\d+.\d+.\d+) netmask (\d+.\d+.\d+.\d+)/)
    {
        $networks{$1}->{'name'} = $prev_line;
        $current_network = $1;
    }
    
    # if the line is a range statement gather the low and high ip
    # only takes into account first and last ip in range statement
    # doesn't handle multiple range statements
    if (/^\s*range\s+(\d+\.\d+\.\d+\.\d+)[^ ]$/) {
	print qq(DEBUG-CONF: SINGLE ADDRESS RANGE DETECTED, IGNORING?\n) if $debug_config;

	#if (!$networks{$current_network}->{'addrcount'}) {
	#  $networks{$current_network}->{'addrcount'} += 1;
	#} else {
	#  $networks{$current_network}->{'addrcount'} = 1;
	#}

	next;
    }

    if ( !/^\s*\#/ and /^\s*range\s+([^;]+)/ )
    {
        my @range = split / /, $1;
        
        #$networks{$current_network}->{'low_ip'} = $range[0] unless
        #          exists($networks{$current_network}->{'low_ip'});
        $networks{$current_network}->{'low_ip'} = $range[0];
        $networks{$current_network}->{'high_ip'} = $range[-1];
	print qq(DEBUG-CONF: $1\n) if $debug_config;
	print qq(DEBUG-MATH: $networks{$current_network}->{'low_ip'} -- $networks{$current_network}->{'high_ip'}\n) if $debug_math;

	my @low_ip = split/\./, $networks{$current_network}->{'low_ip'};
	my @high_ip = split/\./, $networks{$current_network}->{'high_ip'};
	my $quad1 = ($high_ip[0] - $low_ip[0]) * 256**3;
	my $quad2 = ($high_ip[1] - $low_ip[1]) * 256**2;
	my $quad3 = ($high_ip[2] - $low_ip[2]) * 256;
	my $quad4 = ($high_ip[3] - $low_ip[3]);
	my $range_ip_count  =   $quad1;
	   $range_ip_count +=   $quad2;
	   $range_ip_count +=   $quad3;
	   $range_ip_count +=   $quad4;

	print qq(DEBUG-MATH: ($high_ip[0] - $low_ip[0]) * 256**3 = $quad1\n) if $debug_math;
	print qq(DEBUG-MATH: ($high_ip[1] - $low_ip[1]) * 256**2 = $quad2\n) if $debug_math;
	print qq(DEBUG-MATH: ($high_ip[2] - $low_ip[2]) * 256 = $quad3\n) if $debug_math;
	print qq(DEBUG-MATH: $high_ip[3] - $low_ip[3] = $quad4\n) if $debug_math;

	if ($networks{$current_network}->{'addrcount'}) {
	  print qq(DEBUG-ADDRCOUNT: bumping $networks{$current_network}->{'addrcount'} by $range_ip_count\n) if $debug_math;
	  $networks{$current_network}->{'addrcount'} += $range_ip_count;
	} else {
	  $networks{$current_network}->{'addrcount'} = $range_ip_count;
	}
	print qq(DEBUG-ADDRCOUNT: $current_network $networks{$current_network}->{'addrcount'}\n) if $debug_math;
    }
    
    # set the $prev_line, exclude newline and comment characters
    chomp($prev_line = $_);
    $prev_line =~ s/\#\s*//g;
    print qq(DEBUG-CONF: $prev_line\n\n) if $debug_config;
    
}
close DHCPDCONF;
print qq(debug_config -->\n) if $debug_config or $debug_math;


################################################
# Gather information about the leases
################################################

# track lease whose current information is being read
my $current_lease;

# hash keyed by lease ip address
# stores status and mac address
my %leases;

# hash keyed by vendor mac id
# counts occurences of vendor ids
my %vendors;

# hash keyed by device mac address
# stores anon. hash keyed by network address, which stores lease ip address
my %hardware;

# parse the leases file
open LEASES, $leases_path or die "Could not open dhcpd.leases: $leases_path";
while ( <LEASES> )
{
   next if /^\s*\#/;
   
    # if the line is the beginning of a lease statement
    # gather lease ip and initialize anon hash for lease
    if ( /^lease (\d+.\d+.\d+.\d+)/ )
    {
        $current_lease = $1;
        $leases{$current_lease} = { };
    }
    # if lease has expired binding state eq 'free'
    # if it is active binding state eq 'active'
    if ( /^\s*binding state (\w+)/ )
    {
        $leases{$current_lease}->{'status'} = $1;
    }
    # gather mac address of current lease, increment vendor id
    # lease mac address used to track leases across networks
    if ( /^\s*hardware ethernet (\w{2}:\w{2}:\w{2})(:\w{2}:\w{2}:\w{2})/ )
    {
        $leases{$current_lease}->{'MAC'} = $1 . $2;
        $vendors{$1}++; 
    }
    # gather the client hostname
    # used to help identify roaming leases  
    if ( /^\s*client-hostname \"([^"]*)\"/ )
    {
        $leases{$current_lease}->{'client-hostname'} = $1;
    }
    # gather the date/time that the lease began (all times are in GMT)
    if ( /^\s*starts \d ([^;]*)/ )
    {
        $leases{$current_lease}->{'starts'} = $1;
    }
    # gather the date/time that the lease will expire
    if ( /^\s*ends \d ([^;]*)/ )
    {
        $leases{$current_lease}->{'ends'} = $1;
    }
    

}
close LEASES;

################################################
# Calculate statistics
################################################
print qq(<!-- debug_lease\n) if $debug_lease or $debug_lease_count;
foreach my $lease (keys %leases)
{
    # the lease, low, high, and network ips need to be formatted so that
    # they can be compared numerically
    my $lease_ip = sprintf "%03d%03d%03d%03d", split/\./, $lease;
    foreach my $network (keys %networks)
    {
        my $low_ip = sprintf "%03d%03d%03d%03d", split/\./, $networks{$network}->{'low_ip'};
        my $high_ip = sprintf "%03d%03d%03d%03d", split/\./, $networks{$network}->{'high_ip'};
        my $network_ip = sprintf "%03d%03d%03d%03d", split/\./, $network;
	print qq(DEBUG-LEASE: $low_ip $high_ip $network_ip\n) if $debug_lease;
        
        # if the lease falls within the range for this network
        if ( $lease_ip >= $low_ip and $lease_ip <= $high_ip )
        {
            # tie the lease to its network
            $leases{$lease}->{'network'} = $network;
            
            # increment eith the used leases or abandoned leases for this network
            if ( $leases{$lease}->{'status'} eq 'free' ) {
		$networks{$network}->{'freed_lease_count'}++;
		print qq(DEBUG-LEASE-FREE: $network/$lease $networks{$network}->{'freed_lease_count'}\n) if $debug_lease_count;
	    } else {
		$networks{$network}->{'lease_count'}++;
		print qq(DEBUG-LEASE-USED: $network/$lease $networks{$network}->{'lease_count'}\n) if $debug_lease_count;
	    }
            
            # keep track of all the networks this mac address belongs to
            my $mac = $leases{$lease}->{'MAC'};
            $hardware{$mac}->{$network} = $lease;
            
            # exit the network loop
            last;
        }
    
    }
}
print qq(debug_lease -->\n) if $debug_lease or $debug_lease_count;


################################################
# Print the stats
################################################

# Start with light-grey background for the rows
my $color = $colors{'light_grey'};

# Hash to store cumulative lease statistics spanning all networks
my %totals;

# print the html header info
print qq(-->\n) if $debug_html;
print qq(
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>$title</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<meta http-equiv="refresh" content="300"/>
</head>
<body bgcolor="#FFFFFF">
<a name="Utilization"></a><h2>Utilization Statistics - <a href="#Vendor">Vendor Statistics</a> - 
<a href="#Roaming">Roaming Hardware</a></h2><p>\n);

# Begin the table 
print qq(     
      <table border="0" cellpadding="2" cellspacing="0">
       <tr bgcolor="$colors{light_blue}" align="center" valign="center">
       <td width="3"></td>
       <td><h4>Size</h4></td>
       <td width="4"></td>
       <td><h4>Used</h4></td>
       <td width="4"></td>
       <td><h4>Free</h4></td>
       <td width="4"></td>
       <td><h4>Abandon</h4></td>
       <td width="4"></td>
       <td><h4>%Full </h4></td>
       <td width="4"></td>
       <td align="left"><h4> Subnet</h4></td>
       <td width="4"></td>
       <td align="left"><h4>Subnet Name </h4></td>
       <td width="4"></td>
       </tr>);
       
# The highs database tracks the maximum number of active leases recorded for each network
dbmopen(my %HIGHS, "$db_path", 0666) or die "Cannot create highs database";

my $accum_networks = 0;
my $accum_total_ip = 0;
my $accum_used = 0;
my $accum_free = 0;
my $accum_abandon = 0;

# Print a table row for each network, alternating between grey and light-grey background
# the alph_and_num function makes sure the networks are sorted by vlan number, rather than alphabetically
foreach my $network (sort $sort_order keys %networks )
{
   next if $networks{$network}->{'name'} =~ /OMIT:/;

   # Switch background color
    $color = ($color eq $colors{'light_grey'}) ? $colors{'drk_grey'} : $colors{'light_grey'};
    
    # Add new highs data to database if needed
    $HIGHS{$network} = $networks{$network}->{'lease_count'} if
         $HIGHS{$network} < $networks{$network}->{'lease_count'};

   # Count the number of ip's served by this network
    my @low_ip = split/\./, $networks{$network}->{'low_ip'};
    my @high_ip = split/\./, $networks{$network}->{'high_ip'};
    print qq(<!--\nDEBUG-STATS: $low_ip[0].$low_ip[1].$low_ip[2].$low_ip[3] :: $high_ip[0].$high_ip[1].$high_ip[2].$high_ip[3]\n-->\n) if $debug_html;
    my $total_ip_count  =   ($high_ip[0] - $low_ip[0]) * 256**3;
       $total_ip_count +=   ($high_ip[1] - $low_ip[1]) * 256**2;
       $total_ip_count +=   ($high_ip[2] - $low_ip[2]) * 256;
       $total_ip_count +=    $high_ip[3] - $low_ip[3];
    #$networks{$network}->{'total_ip_count'} = ++$total_ip_count;
    $networks{$network}->{'total_ip_count'} = ++$networks{$network}->{'addrcount'};
    
    
    my $percent = sprintf "%2d", 
      ( (eval { $networks{$network}->{'lease_count'} / $networks{$network}->{'total_ip_count'} * 100 }) || 0 );
    
    # Color the percent field depending on how full the network is
    my $threshold;
    $threshold = $colors{'red'} if $percent <= $thresholds{'red'};
    $threshold = $colors{'orange'} if $percent <= $thresholds{'orange'};
    $threshold = $colors{'blue'} if $percent <= $thresholds{'blue'};
    $threshold = $colors{'green'} if $percent <= $thresholds{'green'};
    
    # Print the table row
    printf qq(<tr bgcolor="$color">
             <td></td>
             <td align="center">%d</td>
             <td></td>
             <td align="center">%d</td>
             <td></td>
             <td align="center">%d</td>
             <td></td>
             <td align="center">%d</td>
             <td></td>
             <td align="center"><font color=$threshold>$percent%</font></td>
             <td></td>
             <td>$network </td>\n
             <td></td>
             <td> $networks{$network}->{'name'} </td>
             <td></td>
             </tr>), $networks{$network}->{'total_ip_count'}, $networks{$network}->{'lease_count'},
                     ($networks{$network}->{'total_ip_count'} - $networks{$network}->{'lease_count'}),
                     $networks{$network}->{'freed_lease_count'};

    $accum_total_ip += $networks{$network}->{'total_ip_count'};
    $accum_used += $networks{$network}->{'lease_count'};
    $accum_free += ($networks{$network}->{'total_ip_count'} - $networks{$network}->{'lease_count'});
    $accum_abandon += $networks{$network}->{'freed_lease_count'};
    
    # Increment the totals data
    $totals{'size'} += $networks{$network}->{'total_ip_count'};
    $totals{'used'} += $networks{$network}->{'lease_count'};
    $totals{'high'} += $HIGHS{$network};
    $totals{'abandon'} += $networks{$network}->{'freed_lease_count'};

}

my $percent = sprintf "%2d", ( (eval { $accum_used / $accum_total_ip * 100 }) || 0 );
my $threshold;
$threshold = $colors{'red'} if $percent <= $thresholds{'red'};
$threshold = $colors{'orange'} if $percent <= $thresholds{'orange'};
$threshold = $colors{'blue'} if $percent <= $thresholds{'blue'};
$threshold = $colors{'green'} if $percent <= $thresholds{'green'};

printf qq(<tr bgcolor="white">
          <td></td>
          <td align="center">%d</td>
          <td></td>
          <td align="center">%d</td>
          <td></td>
          <td align="center">%d</td>
          <td></td>
          <td align="center">%d</td>
          <td></td>
          <!-- TOTAL --><td align="center"><font color=$threshold>$percent%</font></td>
          <td></td>
          <td></td>
          <td></td>
          <td>-- Totals</td>
          <td></td>
          </tr>), $accum_total_ip, $accum_used, $accum_free, $accum_abandon;

dbmclose %HIGHS;
print qq(</table>\n);


my $percent = (eval { $totals{'used'} / $totals{'size'} * 100 }) || 0;

#print "<pre>
#Totals:  Size   Used   High  Abandon  %Full";
#print "\n        ------ ------ ------ ------- ------
#        ";
#printf "%6d ", $totals{'size'};
#printf "%6d ", $totals{'used'};
#printf "%6d ", $totals{'high'};
#printf "%7d ", $totals{'abandon'};
#printf "%5d%% ", $percent;  
#print "\n\n\n\n</pre>\n";


#################################################
# Print Vendor Statistics
#################################################
if (($print_unique_macs > 0) or ($print_unique_macs == -1)) {
    my $count = keys %hardware;
    print qq(<a name="Vendor"></a><h2><a href="#Utilization">Utilization Statistics</a> - Vendor Statistics - <a href="#Roaming">Roaming Hardware</a></h2><p>
         <pre>Unique hardware addresses: $count, printing top $print_unique_macs<hr width="200" align=left>); 

    $count = 0;
    foreach my $vendor_id (sort { $vendors{$b} <=> $vendors{$a} } keys %vendors)
    {   
	$count++;
	if (($count <= $print_unique_macs) and ($print_unique_macs != -1)) {
	    printf "\n%4d  %s  %s", $vendors{$vendor_id}, $vendor_id, $vendor_descs{$vendor_id}; 
	} elsif ($print_unique_macs == -1) {
	    printf "\n%4d  %s  %s", $vendors{$vendor_id}, $vendor_id, $vendor_descs{$vendor_id}; 
	}
    }
} #else { don't print anything: print_unique_macs == 0
print "\n\n\n\n</pre>\n";


####################################################
# print roaming mac addresses, if configured
####################################################
if ($print_roamers) {
    print qq(<a name="Roaming"></a><h2><a href="#Utilization">Utilization Statistics</a> - 
	<a href="#Vendor">Vendor Statistics</a> - Roaming Hardware</h2><p>\n);

    print qq(<table border="0" cellpadding="2" cellspacing="0">\n);

    # Sort by number of networks first, then alphabetically by hostname
    foreach my $mac (sort { keys(%{$hardware{$b}}) <=> keys(%{$hardware{$a}}) or
                        lc($leases{$hardware{$a}->{(sort (keys(%{$hardware{$a}})))[0]}}->{'client-hostname'}) cmp
                        lc($leases{$hardware{$b}->{(sort (keys(%{$hardware{$b}})))[0]}}->{'client-hostname'}) } keys %hardware)
    {

	my @networks = grep { !expired($expire_time, $leases{$hardware{$mac}->{$_}}->{'ends'}) } keys(%{$hardware{$mac}});
	next if @networks < 2;
    
	$color = $colors{'light_grey'};
    
 
	print qq(<tr bgcolor="$colors{light_blue}">
          <td align ="right"><b>$mac</b></td>
          <td width ="2"></td>
          <td><b>$vendor_descs{substr($mac, 0, 8)}</b></td>
          <td width ="2"></td>
          <td><b>Hostname</b></td>
          <td width ="2"></td>
          <td><b>Start/Expire Date</b></td>
          </tr>);
   
   
	foreach my $network (sort { $leases{$hardware{$mac}->{$a}}->{'status'} cmp
                        $leases{$hardware{$mac}->{$b}}->{'status'} or
                        $leases{$hardware{$mac}->{$b}}->{'ends'} cmp
                        $leases{$hardware{$mac}->{$a}}->{'ends'} } @networks)
	{
	    my $status = $leases{$hardware{$mac}->{$network}}->{'status'};
      
	    $color = ($color eq $colors{'light_grey'}) ? $colors{'drk_grey'} : $colors{'light_grey'};
      
	    printf "<tr bgcolor=\"$color\">\n
                <td align=\"right\">%s</td>\n
                <td></td>\n
                <td><font size=\"-1\">%s</font></td>\n
                <td></td>\n
                <td>%s</td>\n
                <td></td>\n
                <td>%s</font></td>",
             ( ( $status eq 'active') and "**") . $network,
             $networks{$network}->{'name'},
             $leases{$hardware{$mac}->{$network}}->{'client-hostname'},
             ( $status eq 'free') ? "<font color=\"#FF0000\">Expired:  $leases{$hardware{$mac}->{$network}}->{'ends'}" :
                                    "<font color=\"#009900\">Started: $leases{$hardware{$mac}->{$network}}->{'starts'}";   
	}

    }
    print "</table>\n";
}

# end benchmark and tailing html
my $t1 = new Benchmark;
my $td = timediff($t1,$t0);
print "\n\n<p>The code took " . timestr($td) . ".\n";
print "</pre></html>";



####################################
#        Extra Subroutines         #
####################################

# Extract the number from a string
# Used to sort Network names by vlan number, rather than alphabetically
sub by_num
{
   my($num_a,$num_b) = ($networks{$a}->{'name'} =~ /(\d+)/, $networks{$b}->{'name'} =~ /(\d+)/);
   return $num_a <=> $num_b or $networks{$a}->{'name'} cmp $networks{$b}->{'name'}; 
}

# Sort normally
sub by_abc
{
   $networks{$a}->{'name'} cmp $networks{$b}->{'name'};
}


# Returns true if a lease expired before the configured time period
# Used to keep the roaming leases list from getting out of control
sub expired
{
        my ($expire_time, $lease_date) = @_;

        my ($sec, $min, $hour, $day, $mo) =
        map { sprintf "%02d", $_ } localtime;
            $mo -= ($expire_time - 1);
        my $yr = (localtime)[5] + 1900;
        if ($mo < 0)
        {
                $yr--;
                $mo += 11;
        }

        $mo = sprintf "%02d", $mo;

        my $exp_date = "$yr/$mo/01 $hour:$min:$sec";

        return $lease_date lt $exp_date;

}
