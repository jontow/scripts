#!/bin/sh
#
# 2010-06-10 -- jontow@zenbsd.net
#
# Build debian vserver guest
#

VSERVERCFG="/etc/vservers"
VSERVERROOT="/home/vserver"
DEBMIRROR="http://ftp.us.debian.org/debian/"
POSTINST="post-install-script.sh"

if [ "$3" = "" ]; then
	echo "Syntax: build-guest <type> <fqdnhostname> <int/ipstring>"
	echo ""
	echo "Example: build-guest debian vtest1.example.com eth0:192.168.1.10/24"
	echo ""
	exit
fi

GTYPE="$1"
GHOST="$2"
GNAME="$(echo $GHOST | awk -F. '{print $1}')"
GIPSTR="$3"

if [ -d "${VSERVERCFG}/${GHOST}" ]; then
	echo "vserver by that name already exists."
	exit
fi

if [ "$GTYPE" = "debian" ]; then
	vserver ${GNAME} build -m debootstrap --hostname ${GHOST} --interface ${GIPSTR} -- -d squeeze -m ${DEBMIRROR}
elif [ "$GTYPE" = "centos" ]; then
	vserver ${GNAME} build --force -m yum --hostname ${GHOST} --interface ${GIPSTR} -- -d centos5
else
	echo "That guest type not currently supported."
	exit
fi

################################################################################
#
# Post-processing
#

# Add interface name
echo "0" >${VSERVERCFG}/${GNAME}/interfaces/0/name

# Add some default disk limits (10GB)
mkdir -p ${VSERVERCFG}/${GNAME}/dlimits/root
echo "${VSERVERROOT}/${GNAME}" >${VSERVERCFG}/${GNAME}/dlimits/root/directory
echo "150000" >${VSERVERCFG}/${GNAME}/dlimits/root/inodes_total
echo "5" >${VSERVERCFG}/${GNAME}/dlimits/root/reserved
echo "10485760" >${VSERVERCFG}/${GNAME}/dlimits/root/space_total

# Add some default memory limits (512MB): rss figure is in clusters (divide total by 4)
echo "VIRT_MEM" >>${VSERVERCFG}/${GNAME}/flags
mkdir -p ${VSERVERCFG}/${GNAME}/rlimits
echo "131072" >${VSERVERCFG}/${GNAME}/rlimits/rss

if [ -f ${VSERVERCFG}/${POSTINST} ]; then
	cp ${VSERVERCFG}/${POSTINST} ${VSERVERROOT}/${GNAME}/${POSTINST}

	echo ""
	echo ""
	echo " *** START VSERVER AND RUN ${POSTINST} NOW"
	echo ""
	echo ""
fi

# Add generic nameservers
echo "nameserver 173.245.144.5" >${VSERVERROOT}/${GNAME}/etc/resolv.conf
echo "nameserver 173.245.144.6" >>${VSERVERROOT}/${GNAME}/etc/resolv.conf
echo "nameserver 173.245.144.7" >>${VSERVERROOT}/${GNAME}/etc/resolv.conf

exit
