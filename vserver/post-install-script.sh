#!/bin/sh
################################################################################
#
# 2010-06-21 -- jontow@zenbsd.net
#
# To be run from a newly-built vserver, should do some configuration, etc.
#
################################################################################

apt-get update
apt-get -y remove exim4
apt-get -y install locales sudo screen zsh less wget openssh-server host
useradd -d /home/jontow -m -g 0 -p '$1$MGK5b/gn$snHC4uJqxozkyKYirrnTx1' -s /bin/zsh jontow
wget -O /home/jontow/.zshrc http://svn.slic.com/ext/jontow/rcfiles/dot.zshrc
wget -O /home/jontow/.zprofile http://svn.slic.com/ext/jontow/rcfiles/dot.zprofile
wget -O /home/jontow/.zshenv http://svn.slic.com/ext/jontow/rcfiles/dot.zshenv
rm /etc/localtime
ln -s /usr/share/zoneinfo/America/New_York /etc/localtime

if [ "$(grep '^%root' /etc/sudoers)" = "" ]; then
	echo '%root	ALL=(ALL) ALL' >>/etc/sudoers
fi
