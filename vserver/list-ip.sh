#!/bin/sh
#
# 2010-06-15 -- jontow@zenbsd.net
#
# I got sick of trying to remember what's going on, so this should help.
# Also: dirty hack, but fairly handy.
#

VSERVERCFG="/etc/vservers"

USEDLIST=""

for VSRV in `vserver-stat | grep -v "^CTX" | awk '{print $8}'`; do
	if [ "$1" = "" ]; then
		printf "%20s  : " $VSRV
	fi

	for IPADDR in `cat ${VSERVERCFG}/${VSRV}/interfaces/*/ip`; do
		USEDLIST="${USEDLIST} ${IPADDR}"
		if [ "$1" = "" ]; then
			echo -n " ${IPADDR}"
		fi
	done

	if [ "$1" = "" ]; then
		echo ""
	fi
done

if [ "$1" != "" ]; then
	if [ "$(echo "$USEDLIST" | grep $1)" = "" ]; then
		echo "$1 is unused."
		exit 0
	else
		echo "$1 is in use."
		exit 1
	fi
fi
