#!/bin/sh
#
# Example run for making a chart from a csv
#

if [ "$1" = "" ]; then
	echo "Syntax: make-chart.sh <chartname>"
	exit
fi

CHARTNAME="$1"

if [ ! -f "${CHARTNAME}.csv" ] && [ ! -f "${CHARTNAME}.txt" ]; then
	echo "Missing ${CHARTNAME}.{csv,txt}"
	exit
fi

./parsedir.rb ${CHARTNAME}.csv >${CHARTNAME}.tmp
cat header.html ${CHARTNAME}.tmp footer.html >${CHARTNAME}.html
