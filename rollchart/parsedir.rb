#!/usr/bin/env ruby

################################################################################
#
# 2014-07-10 -- jontow@zenbsd.net
#
# Hugely complex (possibly overcomplex) roll chart generator, taking a CSV as input
# and spitting out HTML+CSS.
#
# Use like this:
#
# $ ./parsedir dirlist.csv >dirs.tmp
# $ cat header.html dirs.tmp footer.html >dirs.html
#
################################################################################

DEBUG = false

# A few constants.. you can tweak them if you use weird paper, I guess.

# This is how many rows are considered to be in a full page
FULLPAGE = 7

# Adjust number of columns per page by modifying COLS and '@cols' below and
# tweaking the number of empty arrays contained
COLS = 3
#@cols = [],[],[]

# Each page should be a copy of @cols with (FULLPAGE * COLS) items
@pages = []

################################################################################

if ARGV[0].nil? or ARGV[0].empty?
	puts "Syntax: parsedir.rb <dirlist.csv>"
	exit
end

@dirlist = ARGV[0]

if !File.exists?(@dirlist)
	puts "Can't find #{@dirlist}"
	exit
end

################################################################################
#
# Parse the input file ('dirlist.csv') and generate a bigass hash of its
# contents.
#

@dirs = []

File.open(@dirlist, 'r').each do |line|
	next if line =~ /^#/
	next if line =~ /^$/

	dirhash = {}

	(seq,bearing,totaldistance,wpdistance,latlong,wpname,tulipfile,turntxt) = line.chomp.split(',', 8)

	dirhash = {
		'seq' => seq,
		'bearing' => bearing,
		'totaldistance' => totaldistance,
		'wpdistance' => wpdistance,
		'latlong' => latlong,
		'wpname' => wpname,
		'tulipfile' => tulipfile,
		'turntxt' => turntxt
	}

	@dirs << dirhash
end

################################################################################
#
# Generate 'ordered' columns from the '@dirs' hash we built above
#

@count = 0
def do_cols(pagenum, pagesize)
	cols = [],[],[]
	x = 0
	y = 0
	#i = (FULLPAGE * COLS) * pagenum
	i = @count
	offset = @count

	fullcount = @count + (FULLPAGE * COLS)
	remaining = @dirs.length - @count + 1
	pageitems = remaining

	puts "do_cols_loop: count:#{@count}/fullcount:#{fullcount}" if DEBUG

	while @count < fullcount
		@count += 1
		remaining -= 1

		puts "going around (i:#{i}/x:#{x}/y:#{y}/cols:#{cols.length}/dirs:#{@dirs.length}/count:#{@count}/fullcount:#{fullcount})" if DEBUG

		# Toggle 'found' if we've already inserted this direction
		found = false
		cols.each do |c|
			if c.include?(@dirs[i])
				found = true
			end
		end

		# Don't re-insert it if we already found it..
		if !found
			cols[y] << @dirs[i]
		else
			puts "Already found y:#{y}/i:#{i} in cols" if DEBUG
			remaining += 1
		end

		# Setup to work with next column
		y += 1

		# Wrap back to column 0 after the last one, and
		# advance to the next row
		if y >= cols.length
			y = 0
			x += 1
		end

		if i > @dirs.length or y == 0 and pagesize != :part
			puts "*** why did I hit this? ..i=x+offset  (#{i} > #{@dirs.length} or #{y} == 0 (and full page))" if DEBUG
			i = x + offset
		else
			if pagesize == :full
				puts "pre-fullpage (i:#{i})" if DEBUG
				i += FULLPAGE
				puts "post-fullpage (i:#{i})" if DEBUG
			elsif pagesize == :part
				puts "pre-partpage (i:#{i}/remaining:#{remaining}" if DEBUG
				
				# only enough data for a single column
				if pageitems <= FULLPAGE
					y = 0
					i += 1
				# enough data for more than one column
				else
					if i > @dirs.length or y == 0
						puts "*** why did I hit this? ..i=x+offset  (#{i} > #{@dirs.length} or #{y} == 0 (and part page))" if DEBUG
						i = x + offset
					else
						# Don't overrun the depth of @dirs
						next if (i + FULLPAGE) > @dirs.length

						i += FULLPAGE
					end
				end
				puts "post-partpage (i:#{i}/remaining:#{remaining}" if DEBUG
			else
				puts "Unknown page size."
				exit
			end
		end
		puts "REMAINING: #{remaining}" if DEBUG
		break if remaining == 0
	end

	return cols
end

################################################################################
#
# Now take the generated columns and gsub their contents into the templates
# to make "real html"..
#

def gen_page(pagenumber)
	puts "gen_Page(#{pagenumber})" if DEBUG
	p @pages if DEBUG
	breakout = 0

	p @pages if DEBUG
	puts "PAGENUMBER: #{pagenumber}" if DEBUG
	puts "PAGELEN: #{@pages[pagenumber][0].length}/#{@pages[pagenumber][1].length}/#{@pages[pagenumber][2].length}" if DEBUG

	until @pages[pagenumber][0].empty? and @pages[pagenumber][1].empty? and @pages[pagenumber][2].empty?
		puts "\n\n" if DEBUG

		if breakout >= @pages[pagenumber].length
			#p breakout if DEBUG
			break
		end

		#
		# New row
		#
		puts "<div class=\"row\">"
		0.upto(@pages[pagenumber].length - 1).each do |c|
			p c if DEBUG
			dir = @pages[pagenumber][c].shift

			next if dir.nil?
			p dir if DEBUG

			# This is huge and amazing and ugly... beware, I guess.
			puts `cat col#{c + 1}.template | sed -e '1,$s/_SUBSEQ_/#{dir['seq']}/g' -e '1,$s/_SUBTOTAL_/#{dir['totaldistance']}/g' -e '1,$s/_SUBDISTANCE_/#{dir['wpdistance']}/g' -e '1,$s/_SUBLATLONG_/#{dir['latlong']}/g' -e '1,$s/_SUBTULIP_/img\\/#{dir['tulipfile']}\.png/g' -e '1,$s/_SUBTURN_/#{dir['turntxt']}/g'`
		end
		puts "</div>"
	end
end

fullpages = @dirs.length / (FULLPAGE * COLS)
partpages = @dirs.length % (FULLPAGE * COLS)
pagecount = 0

while pagecount < fullpages
	puts "*** do_cols(#{pagecount}, :full)" if DEBUG
	@pages[pagecount] = do_cols(pagecount, :full)
	pagecount += 1
end

if partpages > 0
	puts "*** do_cols(#{pagecount}, :part)" if DEBUG
	@pages[pagecount] = do_cols(pagecount, :part)
end

#p @cols[0] if DEBUG
#puts "" if DEBUG
#p @cols[1] if DEBUG
#puts "" if DEBUG
#p @cols[2] if DEBUG

# fullpages + partpages (should always be only 1 partpage)
i = 0
while i < (pagecount + 1)
	if i != 0
		puts `cat pagebreak.template`
	end

	gen_page(i)
	i += 1
end
