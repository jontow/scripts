#!/usr/bin/ruby
require 'pcaplet'
httpdump = Pcaplet.new('-n -i eth3 -s 1500')

HTTP_REQUEST  = Pcap::Filter.new('ip and tcp and dst port 80', httpdump.capture)
HTTP_RESPONSE = Pcap::Filter.new('ip and tcp and src port 80', httpdump.capture)

httpdump.add_filter(HTTP_REQUEST | HTTP_RESPONSE)
httpdump.each_packet {|pkt|
  data = pkt.tcp_data

  #p pkt.tcp_data

  case pkt
  when HTTP_REQUEST
    ip = ""
    host = ""
    path = ""
    if data and data =~ /^GET\s+(\S+)/
      path = $1
      ip = pkt.dst.to_s
      ip << ":#{pkt.dst_port}" if pkt.dport != 80
      #s = "#{pkt.src}:#{pkt.sport} > GET http://#{ip}#{path}"
      now = Time.now.strftime("%Y-%m-%d %H:%M:%S")
      s = "#{now} GET http://#{ip}#{path}"
    end
    if data and data =~ /^Host:\s+(\S+)/
      host = $1
      #s = "#{pkt.src}:#{pkt.sport} > GET http://#{host}#{path} (#{ip})"
      now = Time.now.strftime("%Y-%m-%d %H:%M:%S")
      s = "#{now} GET http://#{host}#{path} (#{ip})"
    end
  when HTTP_RESPONSE
    if data and data =~ /^(HTTP\/.*)$/
      status = $1
      #s = "#{pkt.dst}:#{pkt.dport} < #{status}"
    end
  end
  puts s if s
}
