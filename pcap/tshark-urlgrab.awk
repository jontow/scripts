#
# 2014-01-31 -- stian@sletner.com and jontow@zenbsd.net
#
# Run like this:
#
# % sudo tshark -n -i eth0 -O http 2>/dev/null | awk -f tshark-urlgrab.awk
#
# NOTE: Requires tshark 1.10.x+:
#       older versions don't support -O, and
#       output format is much different.
#

BEGIN {
	url = ""
}
/Internet Protocol/ {
	ip = $9
}
/Full request URI/ {
	url = $4
	sub(".$", "", url)
}
url != "" {
	print strftime("%Y-%m-%dT%H:%M:%S") " " url " (" ip ")"
	url = ""
}
