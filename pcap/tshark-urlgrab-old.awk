#
# 2014-01-31 -- stian@sletner.com and jontow@zenbsd.net
#
# Call it kinda like this:
#
# % sudo tshark -n -i eth3 -V -f 'tcp' 2>/dev/null | awk -f tshark-urlgrab.awk
#
# Tested with tshark 1.2.7 and 1.10.2 on Ubuntu
#

BEGIN {
	http = 0
	url = ""
}
/Destination: / {
	ip = $2
}
/^Hypertext Transfer Protocol/ {
	http = 1
	next
}
http == 1 {
	if ($1 == "GET" || $1 == "POST") {
		method = $1
		url = $2
	}
	http = 0
}
url != "" && /^ *Host:/ {
	host = $2
	sub("....$", "", host)
	print strftime("%Y-%m-%dT%H:%M:%S") " " method " http://" host url " (" ip ")"
	url = ""
}
