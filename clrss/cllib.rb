#!/usr/bin/env ruby
#
# 2010-06-03 -- jontow@zenbsd.net
#
# Ruby RSS magic for http://www.craigslist.org/
#

require 'rss'
require 'open-uri'

class CLRSS
	attr_reader :rss
	attr_accessor :clist, :category, :regexp, :daysold, :profile

	def initialize
		@clist = nil
		@category = nil
		@regexp = nil
		@daysold = nil
		@profile = nil
		@rss_content = nil
		@rss = nil
	end
	
	def fetch
		if @clist.nil? or @category.nil?
			raise ArgumentError, "Must set clist and category"
		end

		open("http://#{@clist}.craigslist.org/#{@category}/index.rss") do |f|
			@rss_content = f.read
		end

		@rss = RSS::Parser.parse(@rss_content, false)
	end

	def print_rss
		@rss.items.each do |item|
			if !@daysold.nil?
				next if (Time.now - item.date) > (@daysold.to_i * 86400)
			end

			if item.title =~ /#{@regexp}/i
				puts "#{item.date.strftime("%Y-%m-%d %H:%M")}: #{item.title}: #{item.description.gsub(/(\r|\n|<\/?[^>]*>)/, ' ')}"[0..109]
			else
				next
			end
		end

		return true
	end

	def set_filter(filter)
		if filter.nil?
			return false
		elsif filter == "FILTER:camper"
			@regexp = "(camper|popup|pop-up|travel\ trailer)"
		elsif filter == "FILTER:jeep"
			@regexp = "(jeep|wrangler|cherokee|CJ|YJ|XJ)"
		else
			@regexp = filter
		end
	end
end
