#!/usr/bin/env ruby
#
# 2010-06-04 -- jontow@zenbsd.net
#

require 'cgi'
require 'cllib'

cgi = CGI.new
cl = CLRSS.new
now = Time.now

ALLOWED_USERS = ["65.172.243.140"]

puts "Content-type: text/html\r\n\r\n"

if !ALLOWED_USERS.include?(ENV['REMOTE_ADDR'])
	puts "<html><head><title>FAIL</title></head><body><center><h1>PERMISSION DENIED..</h1></center></body></html"
	exit
end

if !cgi["refresh"].nil? and !cgi["refresh"].empty?
	if cgi["refresh"].to_i < 300
		puts "<html>\n<head><title>Refresh interval too low</title>"
		puts "<meta http-equiv=\"refresh\" content=\"0;url=#{ENV['SCRIPT_LOCATION']}\">"
		puts "</head><body></body></html>"
		exit
	end

	puts "<html>\n<head><title>Craigslist RSS-Powered Search</title>"
	puts "<meta http-equiv=\"refresh\" content=\"#{cgi["refresh"]}\"></head>\n<body>"
else
	puts "<html>\n<head><title>Craigslist RSS-Powered Search</title></head>\n<body>"
end

if !cgi["debug"].nil? and cgi["debug"] == "true"
	puts "<b>DEBUG</b>: "
	p cgi.params
	puts "<br/><br/>"
end

if !cgi["action"].nil? and cgi["action"] == "submitted"
	if !cgi["category"].nil?
		cl.category = cgi["category"]
	else
		puts "<b>ERROR: category unset?</b>"
		puts "</body></html>"
		exit
	end

	if !cgi["regexp"].nil?
		cl.set_filter(cgi["regexp"])
	end

	if !cgi["daysold"].nil?
		cl.daysold = cgi["daysold"]
	end

	lists = []

	if !cgi["potsdam"].nil? and cgi["potsdam"] == "checked"
		lists << "potsdam"
	end

	if !cgi["watertown"].nil? and cgi["watertown"] == "checked"
		lists << "watertown"
	end

	if !cgi["plattsburgh"].nil? and cgi["plattsburgh"] == "checked"
		lists << "plattsburgh"
	end

	if !cgi["burlington"].nil? and cgi["burlington"] == "checked"
		lists << "burlington"
	end

	if !cgi["syracuse"].nil? and cgi["syracuse"] == "checked"
		lists << "syracuse"
	end

	if !cgi["nh"].nil? and cgi["nh"] == "checked"
		lists << "nh"
	end

	lists.each do |l|
		cl.clist = l
		cl.fetch

		puts "<h2>#{l}</h2>"
		puts "<ul>"
		cl.rss.items.each do |item|
			if !cl.daysold.nil?
				next if (now - item.date) > (cl.daysold.to_i * 86400)
			end

			if item.title =~ /#{cl.regexp}/i
				puts "<li>#{item.date.strftime("%Y-%m-%d %H:%M")}: <a href='#{item.link}' target='_blank'>#{item.title}</a>: #{item.description.gsub(/(\r|\n|<\/?[^>]*>)/, ' ')}"[0..190]
			else
				next
			end
		end
		puts "</ul>"
	end
else
	puts "<form method=\"get\" action=\"?\">"
	puts "Search these lists:<br/>"
	puts "<table border=\"0\" cellspacing=\"5\">"
	puts "<tr>"
	puts "<td><input type=\"checkbox\" name=\"potsdam\" value=\"checked\" checked>Potsdam</td>"
	puts "<td><input type=\"checkbox\" name=\"watertown\" value=\"checked\" checked>Watertown</td>"
	puts "</tr><tr>"
	puts "<td><input type=\"checkbox\" name=\"plattsburgh\" value=\"checked\" checked>Plattsburgh</td>"
	puts "<td><input type=\"checkbox\" name=\"burlington\" value=\"checked\" checked>Burlington</td>"
	puts "</tr><tr>"
	puts "<td><input type=\"checkbox\" name=\"syracuse\" value=\"checked\">Syracuse</td>"
	puts "<td><input type=\"checkbox\" name=\"nh\" value=\"checked\">New Hampshire</td>"
	puts "</tr><tr>"
	puts "</table>"
	puts "Category:"
	puts "<select name=\"category\">"
	puts "<option value=\"sss\" selected>for sale (all)</option>"
	puts "<option value=\"app\">appliances</option>"
	puts "<option value=\"atq\">antiques</option>"
	puts "<option value=\"art\">arts+crafts</option>"
	puts "<option value=\"pts\">auto parts</option>"
	puts "<option value=\"bab\">baby+kids</option>"
	puts "<option value=\"bar\">barter</option>"
	puts "<option value=\"hab\">beauty+health</option>"
	puts "<option value=\"bik\">bikes</option>"
	puts "<option value=\"boa\">boats</option>"
	puts "<option value=\"bks\">books</option>"
	puts "<option value=\"bfs\">business</option>"
	puts "<option value=\"cta\">cars+trucks</option>"
	puts "<option value=\"emd\">cds/dvd/vhs</option>"
	puts "<option value=\"mob\">cell phones</option>"
	puts "<option value=\"clo\">clothes+acc</option>"
	puts "<option value=\"clt\">collectibles</option>"
	puts "<option value=\"sys\">computers</option>"
	puts "<option value=\"ele\">electronics</option>"
	puts "<option value=\"grd\">farm+garden</option>"
	puts "<option value=\"zip\">free</option>"
	puts "<option value=\"fua\">furniture</option>"
	puts "<option value=\"gms\">garage sale</option>"
	puts "<option value=\"for\">general</option>"
	puts "<option value=\"hsh\">household</option>"
	puts "<option value=\"jwl\">jewelry</option>"
	puts "<option value=\"mat\">materials</option>"
	puts "<option value=\"mcy\">motorcycles</option>"
	puts "<option value=\"msg\">music instr</option>"
	puts "<option value=\"pho\">photo+video</option>"
	puts "<option value=\"rvs\">rvs</option>"
	puts "<option value=\"spo\">sporting goods</option>"
	puts "<option value=\"tix\">tickets</option>"
	puts "<option value=\"tls\">tools</option>"
	puts "<option value=\"tag\">toys+games</option>"
	puts "<option value=\"wan\">wanted</option>"
	puts "<option value=\"vgm\">video gaming</option>"
	puts "</select><br/>"
	puts "Days Old: <input type=\"text\" name=\"daysold\" value=\"7\"/><br/>"
	puts "Refresh: <input type=\"text\" name=\"refresh\" value=\"86400\"<br/>"
	puts "Search Term: <input type=\"text\" name=\"regexp\" value=\"\"/><br/>"
	puts "<input type=\"hidden\" name=\"action\" value=\"submitted\"/>"
	puts "<input type=\"submit\" name=\"submit\" value=\"Submit\"/>"
	puts "</form>"
	puts "<br/><br/>\n<b>Hints/etc</b>:\n<ul>"
	puts "<li>This search uses <a href=\"http://www.craigslist.org/\">http://www.craigslist.org/</a>.  Hats off to them, its a wonderful place to shop."
	puts "<li>This search uses CL's RSS feeds to do its dirty work.  This is limited to the first 100 results from each list."
	puts "<li>Term \"FILTER:camper\" will search for 'camper', 'popup', 'pop-up', and 'travel trailer'."
	puts "<li>Term \"FILTER:jeep\" will search for 'jeep', 'wrangler', 'cherokee', 'cj', 'yj' and 'xj'."
	puts "<li>Regular expressions are allowed in the search terms.  This means \"(foo|bar|baz)\" will search for 'foo', 'bar', and 'baz' in the same query."
	puts "<li>I'm in no way responsible for your actions.  Don't blame me if you get ripped off."
	puts "<li>This search engine is an aggregator of someone else's content.  I don't own any of it, and can't answer questions about any of it, really."
	puts "<li>Good luck."
	puts "</ul>"
end

puts "</body>\n</html>"

exit
