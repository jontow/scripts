#!/usr/bin/env ruby
#
# 2010-06-03 -- jontow@zenbsd.net
#
# Ruby RSS magic for http://www.craigslist.org/
#

require 'rss'
require 'open-uri'

if ARGV[1].nil? or ARGV[1].empty?
	puts "Syntax: clrss.rb <category> <regexp|word> [#daysold]"
	exit
end

@category = ARGV[0]

if ARGV[1] == "camper"
	STDERR.puts "PROFILE: camper"
	@regexp = "(camper|popup|pop-up|travel\ trailer)"
elsif ARGV[1] == "jeep"
	STDERR.puts "PROFILE: jeep"
	@regexp = "(jeep|wrangler|cherokee|YJ|XJ)"
else
	@regexp = ARGV[1]
end

if !ARGV[2].nil? and !ARGV[2].empty?
	@daysold = ARGV[2]
end

rss_feeds = [
  ["Potsdam", "http://potsdam.craigslist.org/#{@category}/index.rss"],
  ["Watertown", "http://watertown.craigslist.org/#{@category}/index.rss"],
  ["Plattsburgh", "http://plattsburgh.craigslist.org/#{@category}/index.rss"],
  ["Burlington", "http://burlington.craigslist.org/#{@category}/index.rss"]
]

rss_content = ""

puts "<html>"
puts "<head><title>Craigslist NNY RSS Aggregator</title></head>"
puts "<body>"

# Read the feed into rss_content
rss_feeds.each do |rf|
	open(rf[1]) do |f|
		rss_content = f.read
	end

	# Parse the feed, dumping its contents to rss
	rss = RSS::Parser.parse(rss_content, false)

	# Output the feed title and website URL
	puts "<h3>Area: #{rf[0]}: #{rss.items.size}</h3>"

	puts "<ul>"
	rss.items.each do |item|
		if !@daysold.nil?
			next if (Time.now - item.date) > (@daysold.to_i * 86400)
		end

		if item.title =~ /#{@regexp}/i
			puts "<li>#{item.date.strftime("%Y-%m-%d %H:%M")}: <a href='#{item.link}'>#{item.title}</a>: #{item.description.gsub(/(\r|\n|<\/?[^>]*>)/, ' ')}"[0..200]
		else
			next
		end
	end
	puts "</ul>"
end

puts "</body></html>"
