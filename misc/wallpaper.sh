#!/bin/sh
#
# 2013-10-25 -- jontow@zenbsd.net
#
# Swap out desktop background at specified interval, with random file from
# a collection.
#

INTERVAL="60"

if [ "$1" != "" ]; then
	INTERVAL="$1"
fi

( \
while [ 0 ]; do
	cd $HOME/.wallpaper
	if [ -f $HOME/.stop-wallpaper ]; then
		rm $HOME/.stop-wallpaper
		exit
	fi
	feh -Z --bg-fill -z *
	sleep $INTERVAL
done \
) &
