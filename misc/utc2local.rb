#!/usr/bin/env ruby
#
# 2013-10-03 -- jontow@zenbsd.net
#
# Goofy little script to save me some thinking cycles.
#

require 'time'

if ARGV[0].nil?
	puts "Syntax: utc2local <timestamp>"
	exit
end

puts Time.parse(ARGV[0]).localtime
