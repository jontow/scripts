#!/usr/bin/env ruby
################################################################################
#
# 2014-02-10 -- jontow@zenbsd.net
#
# When given a directory, attempt to keep FS usage below "X" percent.
# Uses some shell hacks.  Beware portability monsters inside.
#
################################################################################

if ARGV[1].nil?
	puts "Syntax: maintain-space-free.rb <directory> <freepercent>"
	puts "    ie. maintain-space-free.rb /tmp 80"
	exit
end

@dtree = ARGV[0]
@fpcnt = ARGV[1]

if !Dir.exist?(@dtree) or !File.directory?(@dtree)
	puts "Directory '#{@dtree}' nonexistant or not a directory."
	exit -1
end

def fs_iter
	rawspacefree = `df #{@dtree} | grep -v '^Filesystem'`.chomp
	(blkdev, totalsz, usedsz, availsz, usedpcnt, mountpt) = rawspacefree.split(' ', 6)
	usedpcnt.gsub!(/\%/, '')

	retstatus = nil
	if usedpcnt.to_i > @fpcnt.to_i
		puts "Not enough space free: #{usedpcnt} > #{@fpcnt}"
	else
		puts "Enough space free: #{usedpcnt} <= #{@fpcnt}"
		return true
	end

	flist = `find #{@dtree} -type f | sort -n`.split
	puts "Removing #{flist[0]}"
	`rm #{flist[0]}`
	#flist.each do |f|
	#	puts "remove #{f}"
	#end

	return false
end

while !fs_iter
	puts "Going around again.."
	sleep 0.2
end
