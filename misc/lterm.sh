#!/bin/sh

# $Id: lterm,v 1.5 2002/05/20 16:13:34 jontow Exp $
#
# Lock Terminal; and display customized text
#
#- feel free to use figlet, home-cooked ascii art,
#  or whatever you feel like.


# #####################
# NOTE: THESE ARE THE DEFAULTS!
#	override by placing these settings in $HOME/.ltermrc
# #####################

# Set this to one of the following:
#	-	figlet
#	-	aart
#	-	other
TEXTDISP="other"

# *** IF USING aart : set this..
# Set this to the file/command to be displayed..
AARTFILE="/usr/local/etc/lterm.txt"

# *** IF USING figlet : set this..
# Set this to the figlet string you wish to display
FIGLETSTR="FreeBSD i386"

# *** IF USING other : write your code into the body, marked..
# 	with the character string "OTHER COMMAND BODY" in a comment.

# Now begins the fun..

# suck in user-overrides, if they exist
if [ -f $HOME/.ltermrc ]; then
	. $HOME/.ltermrc
fi

# clear screen initially..
clear

# decide on the mode we're running in ..
if [ "$TEXTDISP" = "aart" ]; then
	if [ "$AARTFILE" != "" ]; then
		cat $AARTFILE
	fi
fi

if [ "$TEXTDISP" = "figlet" ]; then
	if [ "$FIGLETSTR" != "" ]; then
		figlet $FIGLETSTR
	fi
fi

if [ "$TEXTDISP" = "other" ]; then

	# !!!!!!!!!!!!!!!!!!
	# OTHER COMMAND BODY
	# !!!!!!!!!!!!!!!!!!

	cat $AARTFILE
	figlet $FIGLETSTR

fi

# lock this shit :)
lock -pn

exit
