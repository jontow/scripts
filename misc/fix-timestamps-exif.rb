#!/usr/bin/env ruby

require 'rubygems'
require 'exifr'
require 'exifr/jpeg'

if !ARGV[0].nil? and ARGV[0] == '-a'
	Dir.entries(".").grep(/^[^\.].*\.jpg$/i).each do |f|
		t = EXIFR::JPEG.new(f).date_time_original
		if t.nil?
			puts "#{f}: no EXIF timestamp found.. skipping."
		else
			if File.mtime(f) != t
				puts "touch -t #{t.strftime('%Y%m%d%H%M.%S')} #{f}"
				`touch -t #{t.strftime('%Y%m%d%H%M.%S')} #{f}`
			end
		end
	end
elsif !ARGV[0].nil?
	if File.exists?(ARGV[0])
		f = ARGV[0]
		t = EXIFR::JPEG.new(f).date_time_original
		if t.nil?
			puts "#{f}: no EXIF timestamp found.. skipping."
		else
			if File.mtime(f) != t
				puts "touch -t #{t.strftime('%Y%m%d%H%M.%S')} #{f}"
				`touch -t #{t.strftime('%Y%m%d%H%M.%S')} #{f}`
			end
		end
	else
		puts "No such file: #{ARGV[0]}"
		exit
	end
else
	puts "Syntax: fix-timestamps-exif <file|-a>"
	exit
end
