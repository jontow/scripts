#!/bin/sh

SVNPATH="$HOME/SVN-jontow"
EXPORTTO="$HOME/work-scripts"

cd ${SVNPATH}

if [ ! -d $EXPORTTO ]; then
	mkdir -p $EXPORTTO
else
	echo "Remove ${EXPORTTO} first."
	exit
fi

svn export ./jontow/cdr-util ${EXPORTTO}/cdr-util
svn export ./jontow/cstools ${EXPORTTO}/cstools-rails
mkdir -p ${EXPORTTO}/freeswitch
svn export ./jontow/freeswitch/cdr-tools ${EXPORTTO}/freeswitch/cdr-tools
svn export ./jontow/freeswitch/conference ${EXPORTTO}/freeswitch/conference
svn export ./jontow/freeswitch/lua ${EXPORTTO}/freeswitch/lua
svn export ./jontow/freeswitch/misc ${EXPORTTO}/freeswitch/misc
svn export ./jontow/freeswitch/voicemail ${EXPORTTO}/freeswitch/voicemail
svn export ./jontow/graphviz ${EXPORTTO}/graphviz
svn export ./jontow/mail-migrate ${EXPORTTO}/mail-migrate
svn export ./jontow/nagios-plugins ${EXPORTTO}/nagios-plugins
svn export ./jontow/occamtools ${EXPORTTO}/occamtools
mkdir -p ${EXPORTTO}/scripts
svn export ./jontow/scripts/dhcpstats ${EXPORTTO}/scripts/dhcpstats
svn export ./jontow/scripts/magicmail ${EXPORTTO}/scripts/magicmail
svn export ./jontow/scripts/owtemplog ${EXPORTTO}/scripts/owtemplog
svn export ./jontow/scripts/radport ${EXPORTTO}/scripts/radport
svn export ./jontow/scripts/slicauth ${EXPORTTO}/scripts/slicauth
svn export ./jontow/scripts/vserver ${EXPORTTO}/scripts/vserver
