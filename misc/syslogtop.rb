#!/usr/bin/env ruby
################################################################################
#
# 2013-03-07 -- jtowne@clarkson.edu
#
# "syslogtop"--watch output files for write activity
#
################################################################################

DEBUG=false
LOGBASE="/var/log/syslog"

# Set up a few defaults
@delay = 1
@master_list = {}

@pidofsyslog = `/usr/bin/pgrep syslog-ng`

if @pidofsyslog.nil? or @pidofsyslog.empty?
	puts "Couldn't find running syslog-ng process."
	exit
end

if !ARGV[0].nil?
	@delay = ARGV[0].to_i
end

################################################################################

def get_file_list
	puts "get_file_list" if DEBUG
	file_hash = {}
	raw_file_list = `/usr/sbin/lsof -p #{@pidofsyslog}`
	raw_file_list.each do |line|
		file = line.chomp.split(' ', 9)[8]
		if file =~ /#{LOGBASE}/
			file_hash[file] = []
		end
	end
	#p file_hash
	p file_hash.length if DEBUG
	return file_hash
end

def calc_diff(cur)
	puts "calc_diff" if DEBUG
	unsorted_list = []
	cur.each do |k,v|
		fst = File.stat(k)

		# Add it to the running list, if it hasn't been seen yet
		if @master_list[k].nil?
			@master_list[k] = [[fst.size, fst.mtime]]
			next
		else
			if @master_list[k].length > 1
				@master_list[k][0] = @master_list[k][1]
			end

			@master_list[k][1] = [fst.size, fst.mtime]
		end

		# Calculate length, arrays are 0 indexed, so remove 1
		flen = @master_list[k].length - 1

		# if mtime is the same as the last run, might as well skip it
		next if @master_list[k][flen] == @master_list[k][flen - 1]
		elapsed = @master_list[k][flen][1] - @master_list[k][flen - 1][1]
		oldsize = @master_list[k][flen - 1][0]
		newsize = @master_list[k][flen][0]
		oldtime = @master_list[k][flen - 1][1]
		newtime = @master_list[k][flen][1]
		bps = (newsize - oldsize) / elapsed

		unsorted_list << [(newsize - oldsize), k, bps, elapsed, oldtime, newtime]

	end

	return unsorted_list.sort.reverse
end

def clear_screen
	print "\e[2J\e[f"
end

################################################################################

file_list = {}

loop do
	if @delay == 0
		get_file_list
		break
	end

	p @master_list if DEBUG
	clear_screen if !DEBUG

	# Shuffle file lists
	file_list = get_file_list

	printf "%-62s %16s %8s %8s %12s %12s\n", "FILENAME", "BYTES", "ELAPSED", "BPS", "OLDTIME", "NEWTIME"
	printf "#############################################################################################################################\n"
	calc_diff(file_list).each do |f|
		printf "%-62s %16s %8s %8.0f %12s %12s\n", f[1], f[0], f[3], f[2], f[4].strftime("%H:%M:%S"), f[5].strftime("%H:%M:%S")
	end
	sleep @delay
end
