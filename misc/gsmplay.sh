#!/bin/sh

if [ "$1" = "" ]; then
	echo "Need a filename."
	exit
fi

FILE="$1"
FNAME=`echo $FILE | awk -F. '{print $1}'`
EXTEN=`echo $FILE | awk -F. '{print $2}'`

if [ ! -f $FILE ]; then
	echo "File not found."
	exit
fi

FWAV="${FNAME}.wav"

if [ -f $FWAV ]; then
	echo "Output file (${FWAV}) exists!"
	exit
fi

sox ${FILE} -t wav ${FWAV}
audacity ${FWAV}

echo -n "Delete .wav? (y/n) "
read YESNO

if [ "$YESNO" = "y" ]; then
	rm ${FWAV}
fi

exit
