#!/usr/bin/env ruby
#
# 2011-09-27 -- jontow@zenbsd.net
#
# Grab a "show ipv6 neighbors" from a JunOS device and report which are
# actually reachable
#
# JunOS has a nasty habit of adding a linebreak in very long IPv6 addresses,
# so we have to deal with that in awkward ways.
#
# I highly recommend that you figure out a way to run this from a source
# machine thats trusted enough to run with a passphrase-less ssh key, or
# things can get pretty damned annoying.
#

DEBUG = false
VERBOSE = true

if ARGV[1].nil?
	puts "Syntax: junos-neighbor-poker routeraddress sshkey"
	exit
end

output = `ssh -i #{ARGV[1]} #{ARGV[0]} "show ipv6 neighbors"`

ourprefix = /2605:cc00:/

# Raw output lines
@neighborlines = []

# Parsed IPv6 addresses (without fe80:: link-locals)
@active = []
# Parsed IPv6 addresses (only fe80:: link-locals)
@inactive = []

# Alive addresses
@alive = []
# Dead addresses
@dead = []

counter = 0
output.each do |out|
	next if out =~ /^IPv6/
	p "XXX: " + out if DEBUG

	if out =~ /^\s+/
		@neighborlines[counter - 1] += out.chomp
	else
		@neighborlines[counter] = out.chomp
	end

	counter += 1
end

# remove nil items due to multi-line neighbor entrie
@neighborlines.compact!

p @neighborlines if DEBUG

@neighborlines.each do |nline|
	(ip6, mac, state, expire, router, secure, interface) = nline.split(' ', 7)

	local = false
	if ip6 =~ ourprefix
		local = true
		puts "#{ip6}: active" if VERBOSE
		@active << ip6
	elsif ip6 =~ /fe80::/
		local = true
		puts "#{ip6}: inactive" if VERBOSE
		@inactive << ip6
	else
		# these addresses are potentially not local
		# ie. bgp peering addresses, etc
		# -- we won't count them.
		local = false
		puts "#{ip6}: unknown" if VERBOSE
	end

	if local
		status = :unknown

		# This is a stub for testing these locally..
		#`ping6 -c 2 #{ip6}`
		#puts $?
		#if $? == ....; status = :something

		# This is the method for testing them from the router
		pingoutput = `ssh -i #{ARGV[1]} #{ARGV[0]} "ping #{ip6} count 2" | grep "packets received"`
		rcvdpkts = pingoutput.split(' ', 9)[3].to_i
		if rcvdpkts > 0
			status = :alive
			@alive << ip6
		else
			status = :dead
			@dead << ip6
		end
	end
end

if DEBUG
	puts "\n\n"
	printf "ALIVE: "
	p @alive
	puts "\n\n"
	printf "DEAD: "
	p @dead
	puts "\n\n"
end

puts "Total local addresses: #{@active.length + @inactive.length}"
puts "Total routable addresses: #{@active.length}"
puts "Total link-local addresses: #{@inactive.length}"
puts "Total alive addresses: #{@alive.length}"
puts "Total dead addresses: #{@dead.length}"
puts "Total alive percent: #{(@alive.length.to_f / (@active.length + @inactive.length).to_f) * 100}"
