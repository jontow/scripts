#!/bin/sh
#
# $Id$
#
# 2006-01-20 -- Jonathan Towne (jontow@adminforrent.com)
#
# 1 = ip
# 2 = pass
# 3 = filename

IPADDR="$1"
PORTNM="$2"
PASSWD="$3"
FILENM="$4"

CURDATE=`date +"%Y-%m-%d-%H-%M"`

if [ "$4" = "" ]; then
	echo ""
	echo "Need 4 arguments:"
	echo ""
	echo "esoftlog [IP-address] [port] [admin-password] [filename]"
	echo ""
	echo "ie. esoftlog 192.168.1.1 8001 adminpass EVERYTHING.log"
	echo ""
	exit
fi

NFILENM="${FILENM}"

########################################################################
### Firewall Logs
if [ "x`echo ${FILENM} | grep "^firewall\."`" != "x" ]; then
	NFILENM="firewall/${FILENM}"
fi

### Administrator Web Logs
if [ "x`echo ${FILENM} | grep "^secure"`" != "x" ]; then
	NFILENM="redphish/${FILENM}"
fi

if [ "x`echo ${FILENM} | grep "^ssl\."`" != "x" ]; then
	NFILENM="redphish/${FILENM}"
fi

### Web Access Control Logs
if [ "x`echo ${FILENM} | grep "^cache\."`" != "x" ]; then
	NFILENM="squid/${FILENM}"
fi

### Webserver Logs
if [ "x`echo ${FILENM} | grep "^webmail"`" != "x" ]; then
	NFILENM="httpd/redphishWeb/${FILENM}"
fi

### General Web-related log error
if [ "x`echo ${FILENM} | grep "^access\."`" != "x" ]; then
	echo ""
	echo ""
	echo "Ambiguous choice, which do you want?"
	echo ""
	echo "	Administrator Web logs :  httpd/redphish/${FILENM}"
	echo "	Web Access Control logs:  squid/${FILENM}"
	echo "	Webserver logs         :  httpd/redphishWeb/${FILENM}"
	echo ""
	echo ""
	exit
fi

if [ "x`echo ${FILENM} | grep "^error\."`" != "x" ]; then
	echo ""
	echo ""
	echo "Ambiguous choice, which do you want?"
	echo ""
	echo "	Administrator Web logs :  httpd/redphish/${FILENM}"
	echo "	Web Access Control logs:  squid/${FILENM}"
	echo "	Webserver logs         :  httpd/redphishWeb/${FILENM}"
	echo ""
	echo ""
	exit
fi

### Anti-Virus logs
if [ "x`echo ${FILENM} | grep "^admin"`" != "x" ]; then
	NFILENM="mmsmtp/${FILENM}"
fi

if [ "x`echo ${FILENM} | grep "^mmsmtp"`" != "x" ]; then
	NFILENM="mmsmtp/${FILENM}"
fi

### Exim logs
if [ "x`echo ${FILENM} | grep "^exim"`" != "x" ]; then
	NFILENM="exim/${FILENM}"
fi

### Windows Share logs
if [ "x`echo ${FILENM} | grep "^log\."`" != "x" ]; then
	NFILENM="samba/${FILENM}"
fi

### Gateway Anti-Spyware
if [ "x`echo ${FILENM} | grep "^gwswd\."`" != "x" ]; then
	NFILENM="gwswd.log"
fi

### deal with /'s in filename
if [ "x`echo ${FILENM} | grep "/"`" != "x" ]; then
	FILENM=`basename ${FILENM}`
fi

echo "$CURDATE : $USER : $IPADDR : $PASSWD" >>/var/tmp/e.log

########################################################################

if [ "${PORTNM}" = "8000" ]; then
	wget --http-user=admin --http-passwd=${PASSWD} --output-document=${FILENM} "http://${IPADDR}:8000/lib/download.php?file=/var/log/${NFILENM}"
	exit
fi

if [ "${PORTNM}" = "8001" ]; then
	wget --no-check-certificate --http-user=admin --http-passwd=${PASSWD} --output-document=${FILENM} "https://${IPADDR}:8001/lib/download.php?file=/var/log/${NFILENM}"
	exit
fi

echo ""
echo "UNKNOWN PORT: try 8000 or 8001"
echo ""
exit
