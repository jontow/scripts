#!/bin/sh
#
# Original work: stian@sletner.com
# Made more complicated and a bit (lot?) uglier by jontow@zenbsd.net
#
# Run script from the directory with images in it, either passing a file glob or
# relying on "*.jpg *.JPG" as a default.  If 'index.txt' exists, entries from it will
# be set as the mouseover text on the images.
#
# Can safely be re-run over top of previous runs, and if your file glob matches
# the old files as well, they'll be included (and not regenerated if their thumbs
# exist.)
# 
# index.txt format:
#
# IMG_1122.jpg:Picture of my dog
# IMG_1123.jpg:Picture of my cat
# IMG_1124.jpg:Blurry/out of focus picture of my goldfish
#

if [ $# -lt 1 ]
then
	files="*.jpg *.JPG *.gif *.png"
else
	files=$@
fi

newfile()
{
	echo "*** Creating new index page: $1"
	echo '<html>' >$1
	echo '<head><title>Thumbs</title></head>' >>$1
	echo '<body>' >>$1
}

# $1 = PAGELIST, $2 = THUMBFILE
endpage()
{
	echo '<br/><a href="nav.php">IMGNAV</a><br/>' >>$1
	echo '</body>' >>$1
	echo '</html>' >>$1
}

NAV="$HOME/SVN-jontow/jontow/scripts/misc/img_nav.php"
THUMBPERPAGE=50
COUNTER=0
PAGELIST="0"
DIVIZ=0
THUMBFILE="thumbs0.html"
ORIGTHUMBFILE=$THUMBFILE

if [ ! -e index.html ]; then
	ln -s $THUMBFILE index.html
fi

if [ ! -e thumbs.html ]; then
	ln -s $THUMBFILE thumbs.html
fi

if [ -e $NAV ]; then
	cp $NAV nav.php
fi

newfile $THUMBFILE

echo "Fixing timestamps via EXIF data..."
fix-timestamps-exif -a

for file in $(ls -rt $files 2>/dev/null)
do
	if [ -f .thumb_$file ]; then
		echo "Thumbnail for $file already exists"
	else
		if [ -d $file ]; then
			echo "Skipping directory: $file"
			continue
		elif [ "`echo $file | grep '^.thumb_'`" != "" ]; then
			echo "Skipping generating a thumbnail for a thumbnail: $file"
			continue
		elif [ "`echo $file | grep -e '.html$'`" != "" ]; then
			echo "Unsupported filetype: $file"
			continue
		else
			convert $file -resize 320x200 .thumb_$file
			chmod a+r .thumb_$file
		fi
	fi

	COUNTER=`expr $COUNTER + 1`
	if [ $COUNTER -ge $THUMBPERPAGE ]; then
		DIVIZ=`expr $COUNTER / $THUMBPERPAGE`
		MODULUS=`expr $COUNTER \% $THUMBPERPAGE`
		THUMBFILE="thumbs$DIVIZ.html"

		if [ $MODULUS = 0 ]; then
			PAGELIST="$PAGELIST $DIVIZ"
			newfile $THUMBFILE
		fi
	fi

	if [ -f index.txt ]; then
		IMGDESC=`grep "^${file}:" index.txt | awk -F: '{print $2}'`
		echo "<a href=$file><img src=.thumb_$file alt=$file title=\"$IMGDESC\"></a>" >>$THUMBFILE
	else
		echo "<a href=$file><img src=.thumb_$file alt=$file></a>" >>$THUMBFILE
	fi
done

echo "<br/><ul>" >>$THUMBFILE
for file in $(ls | grep -i -e '\.mov$' -e '\.mpg$' -e '\.wmv$' -e '\.avi$' -e '\.3gp$' -e '\.mp4$' | sed '1,$s/\/$//g')
do
	echo -n "Found video file $file: "
	if [ -f index.txt ]; then
		IMGDESC=`grep "^${file}:" index.txt | awk -F: '{print $2}'`
		echo "<li><a href=\"$file\">(VIDEO) $file: $IMGDESC</a></li>" >>$THUMBFILE
		echo "and description."
	else
		echo "<li><a href=\"$file\">(VIDEO) $file: No description</a></li>" >>$THUMBFILE
		echo "."
	fi

done
echo "</ul>" >>$THUMBFILE


echo "<br/><ul>" >>$THUMBFILE
#for DIR in $(find . -type d | sed '1,$s/^\.\///g')
#for DIR in $(ls -rtF | grep '/$' | sed '1,$s/\/$//g')
echo -n "Found directories: "
for DIR in $(ls -F | grep -e '/$' -e '@$' | sed -e '1,$s/\/$//g' -e '1,$s/@//g')
do
	echo -n "$DIR "
	if [ -f $DIR/$ORIGTHUMBFILE ] && [ $DIR != "." ] && [ ! -f $DIR/hidden.txt ]; then
		echo "<li><a href=\"$DIR/$ORIGTHUMBFILE\">$DIR</a></li>" >>$THUMBFILE
	fi
done
echo ""
echo "</ul>" >>$THUMBFILE

echo '</body>' >>$THUMBFILE
echo '</html>' >>$THUMBFILE

chmod a+r $THUMBFILE

#
# REALLY ugly method to generate page navigation (per page).. but it works.
# Sorry to the next bastard that has to deal with this.. probably me. -- jontow@
#
PAGES=`expr $(expr $COUNTER / $THUMBPERPAGE) + 1`
OUTTMPPAGE=0
while [ $OUTTMPPAGE -lt $PAGES ]; do
	echo "<br/><br/>" >>thumbs$OUTTMPPAGE.html
	INTMPPAGE=0
	while [ $INTMPPAGE -lt $PAGES ]; do
		echo "<a href='thumbs$INTMPPAGE.html'>--$INTMPPAGE--</a>" >>thumbs$OUTTMPPAGE.html
		INTMPPAGE=`expr $INTMPPAGE + 1`
	done

	echo "<br/><br/>" >>thumbs$OUTTMPPAGE.html
	endpage thumbs$OUTTMPPAGE.html
	OUTTMPPAGE=`expr $OUTTMPPAGE + 1`
done

echo "Done: indexed ${COUNTER} files, generated ${PAGES} thumb pages."
