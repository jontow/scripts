#!/bin/sh
#
# 2013-07-04 -- jontow@zenbsd.net
#
# Renames files in current directory: changes ' ' to '_'
#
################################################################################

IFS='
'
for FILE in `ls`; do
echo -n "$FILE --> "
NEWFILE=`echo $FILE | sed '1,$s/\ /_/g'`
echo "$NEWFILE"
mv $FILE $NEWFILE
done
