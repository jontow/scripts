#!/bin/sh

PLATFORM=`uname`
PORTNAME="$1"

usage()
{
	echo ""
	echo "Need a port name that exists in the local directory!"
	echo ""
}

case $PLATFORM in
	"FreeBSD")
		TOPLEVEL="/usr/ports"
		INFOFILE="pkg-descr"
		;;
	"NetBSD")
		TOPLEVEL="/usr/pkgsrc"
		INFOFILE="DESCR"
		;;
	*)
		TOPLEVEL=""
		INFOFILE=""
		usage
		exit
		;;
esac

if [ "$PORTNAME" = "" ]; then
	usage
	exit
else
	if [ ! -d ./${PORTNAME} ]; then
		echo ""
		echo "Invalid choice -- does not exist in `pwd`!"
		echo ""
		exit
	else
		cat ${PORTNAME}/${INFOFILE}
		exit
	fi
fi

# ENOTREACHED
exit
