#!/bin/sh

if [ "$1" = "" ]; then
	echo "Need a filename."
	exit
fi

FILE="$1"
FNAME=`echo $FILE | awk -F. '{print $1}'`
EXTEN=`echo $FILE | awk -F. '{print $2}'`

if [ ! -f $FILE ]; then
	echo "File not found."
	exit
fi

FGSM="${FNAME}.gsm"

if [ -f $FGSM ]; then
	echo "Output file (${FGSM}) exists!"
	exit
fi

sox ${FILE} -r 8000 -c 1 ${FGSM} resample -ql

exit
