#!/usr/bin/env ruby
#
# Takes 1 parameter (4|6), prints out subnetting table for that AF.
#

ssize = 0
counter = 1

if ARGV[0].nil? or ARGV[0].empty?
	puts "Syntax: subnet-table.rb <4|6>"
	exit
end

if ARGV[0] == "4"
	ssize = 32
elsif ARGV[0] == "6"
	ssize = 128
else
	puts "Syntax: subnet-table.rb <4|6>"
	exit
end

while ssize > 0
	if ARGV[0] == "4"
		if ssize < 24
			puts "#{counter} : /#{ssize} (#{counter / 256} x /24s)"
		else
			puts "#{counter} : /#{ssize}"
		end
	elsif ARGV[0] == "6"
		if ssize < 64
			# How's this for a shitty constant?  .. 18446744073709551616 == 1*/64
			puts "#{counter} : /#{ssize} (#{counter / 18446744073709551616} x /64s)"
		else
			puts "#{counter} : /#{ssize}"
		end
	else
		exit
	end

	counter *= 2
	ssize -= 1
end
