<html>
<head>
<title>IMGNAV</title>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>
$(document).ready(function(){
  var image = $("img.content-image")
  var width = image.width();
  var height = image.height();
  var maxWidth = image.parent().width();
  var maxHeight = image.parent().height();
  var alreadyclicked = false;

  image.click(function(){
    if (alreadyclicked) {
      image.css('width', 'auto');
      image.css('height', '95%');
      alreadyclicked = false;
    } else {
      image.css('width', 'auto');
      image.css('height', 'auto');
      alreadyclicked = true;
    }
});
});
</script>
<script type="text/javascript">
var prmstr = window.location.search.substr(1);
var prmarr = prmstr.split ("&");
var params = {};
var prevfile = ''
var nextfile = ''

for ( var i = 0; i < prmarr.length; i++) {
    var tmparr = prmarr[i].split("=");
    params[tmparr[0]] = tmparr[1];
}

// alert(params.file);

document.onkeydown = function() {
	switch (window.event.keyCode) {
		case 37:
			window.location.href = '?file=' + prevfile;
			break;
		case 38:
			alert('up');
			break;
		case 39:
			window.location.href = '?file=' + nextfile;
			break;
		case 40:
			alert('down');
			break;
	}
};

</script>
</head>

<body>
<?php
if (isset($_GET['file']) && ($_GET['file'] != '')) {
	if ($handle = opendir('.')) {
		$prevfile = '';
		$nextfile = '';
		while (false !== ($file = readdir($handle))) {
			if (!preg_match("/^\\./", $file)
			   && !preg_match("/\.(php|html|txt|mov|wmv|avi|mpg)$/i", $file)
			   && !preg_match("/^thumb_/", $file)
			   && !preg_match("/\.html$/", $file)
			   && !is_dir($file)) {
				if ($file == $_GET['file']) {
					echo "<script type=\"text/javascript\">prevfile=\"" . $prevfile . "\";</script>";
					//echo "prevfile: " . $prevfile . "<br/>\n";
					//echo "curfile: " . $file . "<br/>\n";
					//echo "<center><a href=\"" . $file . "\"><img src=\"" . $file . "\" height=\"95%\"/></a></center><br/>\n";
					echo "<center><div id=\"content\"><div id=\"content-wrapper\"><img class=\"content-image\" src=\"" . $file . "\" height=\"95%\"/></div></div></center><br/>\n";

					$carryon = true;
					while ($carryon) {
						if (false !== ($nextfile = readdir($handle))) {
							if (!preg_match("/^\\./", $nextfile)
							   && !preg_match("/\.(php|html)$/", $nextfile)
							   && !preg_match("/^thumb_/", $nextfile)
							   && !is_dir($nextfile)) {
								echo "<script type=\"text/javascript\">nextfile=\"" . $nextfile . "\";</script>";
								//echo "nextfile: " . $nextfile . "<br/>\n";
								$carryon = false;
							} else {
								$carryon = true;
							}
						} else {
							echo "<script type=\"text/javascript\">nextfile=\"\";</script>";
							echo "nextfile: none<br/>\n";
						}
					}
				}
				$prevfile = $file;
			}
   	 	}
		closedir($handle);
	}
} else {
	if ($handle = opendir('.')) {
		while (false !== ($file = readdir($handle))) {
			if (!preg_match("/^\\./", $file)
			   && !preg_match("/\.(php|html|txt|mov|wmv|avi|mpg)$/i", $file)
			   && !preg_match("/^thumb_/", $file)
			   && !preg_match("/\.html$/", $file)
			   && !is_dir($file)) {
				$nextfile = readdir($handle);
				echo "<script type=\"text/javascript\">window.location.href = '?file=" . $file . "';</script>\n";
				//echo "<center><a href=\"" . $file . "\"><img src=\"" . $file . "\" height=\"95%\"/></a></center><br/>\n";
				echo "<center><div id=\"content\"><div id=\"content-wrapper\"><img class=\"content-image\" src=\"" . $file . "\" height=\"95%\"/></div></div></center><br/>\n";
				break;
			}
		}
		closedir($handle);
	}
}
?>
<a href="index.html">Index</a><br/>
</body>
</html>
