#!/bin/sh
#
# 2006-06-27 -- jontow
#
# Sanitize a lengthy scrollback buffer and logout cleanly, without
# displaying any remnants of the previously logged in user.
#

MYTTY=`tty`
PSTTY=`basename ${MYTTY} | sed 's/tty//g'`
SHELLPID=`pgrep -t ${PSTTY} zsh`

clear
sysctl -a
sysctl -a
sysctl -a
sysctl -a
sysctl -a
sysctl -a
sysctl -a
sysctl -a
sysctl -a
sysctl -a
sysctl -a
clear

( clear ; exec kill -9 ${SHELLPID} ) &

exit
