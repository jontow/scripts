#!/usr/local/bin/ruby
#
### $Id$
#
# Watch an mbox-style mailbox for activitiy, report to a pager/email
# if nothing happens in a specified interval.  Run via cron.
#
# 2006-03-28 -- Jonathan Towne (jontow@hijacked.us)
#
##########################################################################

### include SMTP classes
require 'net/smtp'


### Send alert to
emailto = "fbpagers@hijacked.us"
### Address alert from
emailfrom = "jontow@hijacked.us"
### Mailbox (mbox) file to monitor
mailboxfile = "/usr/home/jontow/.muttMail/cosmofeedback"
### Number of seconds to watch without alerting
timeago = 5400


# Grab modification time from mailbox
mailbox = File.stat(mailboxfile).mtime
# Generate time <timeago> seconds ago
barrier = Time.new - timeago

# If mailbox is newer than timeago seconds..
if barrier < mailbox
	printf "message recently received at: %s\n", mailbox
	exit 0
else
	printf "send a page: %s\n", mailbox

	# Generate SMTP connection to server
	Net::SMTP::start('localhost', 25, "localhost") do |smtp|
		smtp.open_message_stream(emailfrom,
					[ emailto ]
					) do |stream|
			stream.puts "Subject: Feedback Email Alert"
			stream.puts
			stream.puts "A feedback email reply has not come in lately.  Look into it."
		end
	end

	# Exit non-zero to denote an alert being sent
	exit 1
end
