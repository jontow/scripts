#!/bin/sh

CONSOLEFILE="$HOME/.comconsolerc"
PARAM="$1"

if [ "$PARAM" = "" ]; then
	echo ""
	echo "Usage:"
	echo ""
	echo "comconsole <machine|list>"
	echo ""
	exit
fi

if [ "$PARAM" = "list" ]; then
	echo ""
	echo "machine			device		speed"
	echo "------------------------------------------------"
	cat $CONSOLEFILE
	echo "------------------------------------------------"
	echo ""
	exit
fi

MACHINEPORT=`grep ^"${PARAM}" ${CONSOLEFILE} | awk '{print $2}'`
MACHINESPEED=`grep ^"${PARAM}" ${CONSOLEFILE} | awk '{print $3}'`

if [ "$MACHINEPORT" = "" ]; then
	echo "MACHINEPORT not parsed.. failing"
	exit
fi

if [ "$MACHINESPEED" = "" ]; then
	MACHINESPEED="9600"
fi

#echo "sudo cu -l/dev/${MACHINEPORT} -s${MACHINESPEED}"
sudo cu -l/dev/${MACHINEPORT} -s${MACHINESPEED}

exit
