#!/bin/sh
#
# 2009-11-28 -- jontow@zenbsd.net
#

if [ -f templog.cfg ]; then
	. ./templog.cfg
else
	echo "Missing config file: templog.cfg"
	exit
fi

if [ "$1" != "" ]; then
	$RRDTOOL create $RRDPATH/$1.rrd \
		--step ${STEP} \
		DS:temp:GAUGE:300:-40:250 \
		RRA:AVERAGE:0.5:1:1048320 \
		RRA:LAST:0.5:1:1048320
	exit
fi

for T in $(echo $PROBES); do
	$RRDTOOL create $RRDPATH/${T}.rrd \
		--step ${STEP} \
		DS:temp:GAUGE:300:-40:250 \
		RRA:AVERAGE:0.5:1:1048320 \
		RRA:LAST:0.5:1:1048320
done

$RRDTOOL create $RRDPATH/wunder-${WEATHER_CITY}.rrd \
	--step ${STEP} \
	DS:temp:GAUGE:300:-40:120 \
	RRA:AVERAGE:0.5:1:1048320 \
	RRA:LAST:0.5:1:1048320
