#!/usr/bin/env ruby
#
# 2009-11-27 -- jontow@zenbsd.net -- initial working revision (OpenBSD)
# 2011-09-27 -- jontow@zenbsd.net -- added Linux+digitemp support
# 2012-11-29 -- jontow@zenbsd.net -- added error condition support
#
# OpenBSD:
#   Data acquired from:
#   # sysctl hw.sensors.owtemp0.temp0
#
# Linux:
#   Approach is a little different, you have to have a daemon running that logs to
#   a file, as such:
#
#   # digitemp_DS2490 -q -sUSB -a -d 60 -n 0 -l ./templog.txt -o"%R %s %.2F"
#
#   Data acquired from './templog.txt', so put it where you need it.
#

URL="http://somewhere/owtemplog/update-db.php"
DEBUG = false

if !ARGV[0].nil? and ARGV[0] == "-u"
	UPLOAD = true
else
	UPLOAD = false
end

# OpenBSD config
	#'serialnumber => ['sensor description', 'error temperature']
sensors = {
	'000801ede729' => ['shed', "60.00"],
	'000801edacbf' => ['outsideb', "50.00"],
	'0008025b56ca' => ['outsidew', "50.00"],
	'000801edcf9b' => ['basement', "65.00"],
	'000801edd177' => ['boiler', "160.00"],
	'000801edb59d' => ['lroomtv', "65.00"]
}

# Linux config
#
# With some modifications below, this could be used for external temp gathering
# via an Arduino or similar board.
#
#@logfile = "/var/log/owtemp.log"
#sensors = {
#	'102DE7ED01080085' => ['underdesk', "65.00"]
#}

templist = []

case `uname -s`.chomp
when "OpenBSD"
	puts "Running on OpenBSD" if DEBUG and !UPLOAD
	temp = `/sbin/sysctl hw.sensors | grep -i owtemp`
	puts "RAW TEMP: #{temp}" if DEBUG and !UPLOAD
	#`echo "RAW TEMP: #{temp}" >>/tmp/rawtemp.tmp` if DEBUG and !UPLOAD
	temp.each do |t|
		errcondition = false
		#puts "RAW T: #{t}" if DEBUG and !UPLOAD
		tfields = t.split(' ', 4)
		sn = tfields[3].gsub(')', '').chomp
		degc = tfields[0].split('=', 2)[1]
		if degc.to_i == 85
			errcondition = true
			degf = sensors[sn][1]
		else
			degf = sprintf("%.2f", degc.to_f * 9.0 / 5.0 + 32.0)
		end

		# XXX: hack to up the boiler sensor..
		if sensors[sn][0] == "boiler"
			# don't adjust during error condition
			if errcondition
				puts "#{sensors[sn][0]}: #{sensors[sn][1]} (err)" if !UPLOAD
				templist << "#{sensors[sn][0]}=#{sensors[sn][1]}"
			else
				puts "#{sensors[sn][0]}: #{degf} (#{degf.to_f + (degf.to_f * 0.18)})" if !UPLOAD
				templist << "#{sensors[sn][0]}=#{degf.to_f + (degf.to_f * 0.18)}"
			end
		else
			puts "#{sensors[sn][0]}: #{degf}" if !UPLOAD
			templist << "#{sensors[sn][0]}=#{degf}"
		end
	end
when "Linux"
	puts "Running on Linux" if DEBUG and !UPLOAD
	if @logfile.nil? or @logfile.empty?
		puts "Did you uncomment the write section in the readtemp script?" if !UPLOAD
		puts "Hint: define your sensors array and the logfile path." if !UPLOAD
		exit
	end
	numsensors = sensors.length
	temp = `/usr/bin/tail -n #{numsensors} #{logfile}`
	p temp if DEBUG and !UPLOAD

	temp.each do |t|
		tfields = t.split(' ', 3)
		degf = tfields[2]
		sn = tfields[0]

		templist << "#{sensors[sn][0]}=#{degf}"
	end 

	p templist if DEBUG and !UPLOAD
else
	puts "Running on UKNOWN OS!  Fail!" if DEBUG and !UPLOAD
	exit
end

update="#{URL}?update=true&#{templist.join("&")}"

# this whole bit probably doesn't work on linux...
# FUTURE: swap it to wget or use native ruby HTTP libs
puts "\n/usr/bin/ftp -o /dev/null '#{update}' >/dev/null" if !UPLOAD
`/usr/bin/ftp -o /dev/null '#{update}' >/dev/null` if UPLOAD
