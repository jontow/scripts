#!/bin/sh

if [ -f templog.cfg ]; then
	. ./templog.cfg
else
	echo "Missing config file: templog.cfg"
	exit
fi

wget -q -O ${TMPDIR}/${WEATHER_TMPFILE} \
  http://www.wunderground.com/auto/rss_full/${WEATHER_STATE}/${WEATHER_CITY}.xml

mv ${TMPDIR}/${WEATHER_TMPFILE} ${TMPDIR}/${WEATHER_FILE}
