#!/bin/sh
#
# 2009-11-28 -- jontow@zenbsd.net
#

if [ -f templog.cfg ]; then
	. ./templog.cfg
else
	echo "Missing config file: ./templog.cfg"
	exit
fi

COLOR_WEATHER="880000"
COLOR_OUTSIDE="ff0000"
COLOR_SHED="000000"
COLOR_BASEMENT="00ff00"
COLOR_LROOMTV="0088ff"
COLOR_BOILER="0000ff"
COLOR_THERMOSTAT="000000"

for TIMEFRAME in `echo "hour:3600:60 12hour:43200:90 day:86400:120 week:604800:300 month:2419200:600 6month:14515200:3600 year:29030400:7200 2year:58060800:36000 3year:87091200:72000"`; do
	TFNAME=`echo $TIMEFRAME | awk -F: '{print $1}'`
	TFSECS=`echo $TIMEFRAME | awk -F: '{print $2}'`

	$RRDTOOL graph $IMGPATH/boiler-${TFNAME}.png \
		--start now-${TFSECS} -e now --step ${STEP} \
		DEF:boiler=${RRDPATH}/boiler.rrd:temp:LAST \
		LINE1:boiler#${COLOR_BOILER}:"Boiler " \
		VDEF:boilermax=boiler,MAXIMUM \
		VDEF:boileravg=boiler,AVERAGE \
		VDEF:boilercur=boiler,LAST \
		GPRINT:boilermax:"Max %3.2lf" \
		GPRINT:boileravg:"Avg %3.2lf" \
		GPRINT:boilercur:"Cur %3.2lf\n" \
		2>/dev/null >/dev/null

	$RRDTOOL graph $IMGPATH/indoor-${TFNAME}.png \
		--start now-${TFSECS} -e now --step ${STEP} \
		DEF:thermostat=${RRDPATH}/thermostat.rrd:temp:LAST \
		DEF:lroomtv=${RRDPATH}/lroomtv.rrd:temp:LAST \
		LINE1:thermostat#${COLOR_THERMOSTAT}:"Thermostat " \
		VDEF:thermomax=thermostat,MAXIMUM \
		VDEF:thermoavg=thermostat,AVERAGE \
		VDEF:thermocur=thermostat,LAST \
		GPRINT:thermomax:"Max %3.2lf" \
		GPRINT:thermoavg:"Avg %3.2lf" \
		GPRINT:thermocur:"Cur %3.2lf\n" \
		LINE1:lroomtv#${COLOR_LROOMTV}:"LRoom (TV) " \
		VDEF:lroomtvmax=lroomtv,MAXIMUM \
		VDEF:lroomtvavg=lroomtv,AVERAGE \
		VDEF:lroomtvcur=lroomtv,LAST \
		GPRINT:lroomtvmax:"Max %3.2lf" \
		GPRINT:lroomtvavg:"Avg %3.2lf" \
		GPRINT:lroomtvcur:"Cur %3.2lf\n" \
		2>/dev/null >/dev/null

	$RRDTOOL graph $IMGPATH/othertemp-${TFNAME}.png \
		--start now-${TFSECS} -e now --step ${STEP} \
		DEF:weather=${RRDPATH}/wunder-potsdam.rrd:temp:LAST \
		DEF:outside=${RRDPATH}/outside.rrd:temp:LAST \
		DEF:shed=${RRDPATH}/shed.rrd:temp:LAST \
		DEF:basement=${RRDPATH}/basement.rrd:temp:LAST \
		DEF:lroomtv=${RRDPATH}/lroomtv.rrd:temp:LAST \
		LINE1:weather#${COLOR_WEATHER}:"Weather " \
		VDEF:weathermax=weather,MAXIMUM \
		VDEF:weatheravg=weather,AVERAGE \
		VDEF:weathercur=weather,LAST \
		GPRINT:weathermax:"Max %3.2lf" \
		GPRINT:weatheravg:"Avg %3.2lf" \
		GPRINT:weathercur:"Cur %3.2lf\n" \
		LINE1:outside#${COLOR_OUTSIDE}:"Outside " \
		VDEF:outsidemax=outside,MAXIMUM \
		VDEF:outsideavg=outside,AVERAGE \
		VDEF:outsidecur=outside,LAST \
		GPRINT:outsidemax:"Max %3.2lf" \
		GPRINT:outsideavg:"Avg %3.2lf" \
		GPRINT:outsidecur:"Cur %3.2lf\n" \
		LINE1:shed#${COLOR_SHED}:"Shed " \
		VDEF:shedmax=shed,MAXIMUM \
		VDEF:shedavg=shed,AVERAGE \
		VDEF:shedcur=shed,LAST \
		GPRINT:shedmax:"Max %3.2lf" \
		GPRINT:shedavg:"Avg %3.2lf" \
		GPRINT:shedcur:"Cur %3.2lf\n" \
		LINE1:basement#${COLOR_BASEMENT}:"Basement " \
		VDEF:basementmax=basement,MAXIMUM \
		VDEF:basementavg=basement,AVERAGE \
		VDEF:basementcur=basement,LAST \
		GPRINT:basementmax:"Max %3.2lf" \
		GPRINT:basementavg:"Avg %3.2lf" \
		GPRINT:basementcur:"Cur %3.2lf\n" \
		LINE1:lroomtv#${COLOR_LROOMTV}:"LRoom (TV) " \
		VDEF:lroomtvmax=lroomtv,MAXIMUM \
		VDEF:lroomtvavg=lroomtv,AVERAGE \
		VDEF:lroomtvcur=lroomtv,LAST \
		GPRINT:lroomtvmax:"Max %3.2lf" \
		GPRINT:lroomtvavg:"Avg %3.2lf" \
		GPRINT:lroomtvcur:"Cur %3.2lf\n" \
		2>/dev/null >/dev/null
done

exit
