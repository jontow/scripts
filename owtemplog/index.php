<?php

$mobile_browser = '0';
 
if(preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
    $mobile_browser++;
}
 
if((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml')>0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
    $mobile_browser++;
}    
?>

<html>
<head>
<title>Temperatures</title>

<script language="javascript"> 

function get_cookie (cookie_name) {
	var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );

	if ( results )
		return ( unescape ( results[2] ) );
	else
		return null;
}

function set_cookie ( name, value ) {
	var cookie_string = name + "=" + escape ( value );
	document.cookie = cookie_string;
}

function toggle_graph (graphinterval) {
	if (! graphinterval) {
		return null;
	} else if (graphinterval == "1hour") {
		var ele = document.getElementById("1hourDIV");
		var text = document.getElementById("1hourDisp");
	} else if (graphinterval == "12hour") {
		var ele = document.getElementById("12hourDIV");
		var text = document.getElementById("12hourDisp");
	} else if (graphinterval == "1day") {
		var ele = document.getElementById("1dayDIV");
		var text = document.getElementById("1dayDisp");
	} else if (graphinterval == "1week") {
		var ele = document.getElementById("1weekDIV");
		var text = document.getElementById("1weekDisp");
	} else if (graphinterval == "1month") {
		var ele = document.getElementById("1monthDIV");
		var text = document.getElementById("1monthDisp");
	} else if (graphinterval == "6month") {
		var ele = document.getElementById("6monthDIV");
		var text = document.getElementById("6monthDisp");
	} else if (graphinterval == "1year") {
		var ele = document.getElementById("1yearDIV");
		var text = document.getElementById("1yearDisp");
	} else if (graphinterval == "2year") {
		var ele = document.getElementById("2yearDIV");
		var text = document.getElementById("2yearDisp");
	} else if (graphinterval == "3year") {
		var ele = document.getElementById("3yearDIV");
		var text = document.getElementById("3yearDisp");
	} else {
		return null;
	}

	if (ele.style.display == "block") {
		set_cookie(graphinterval, "none");
    		ele.style.display = "none";
		//text.innerHTML = "show";
  	} else {
		set_cookie(graphinterval, "block");
		ele.style.display = "block";
		//text.innerHTML = "hide";
	}
}

function display_graphs () {
	var yum = document.cookie;

	var div = document.getElementById('1hourDIV');
	if (get_cookie('1hour') == "block") {
		div.style.display = "block";
	} else {
		div.style.display = "none";
	}

	div = document.getElementById('12hourDIV');
	if (get_cookie('12hour') == "block") {
		div.style.display = "block";
	} else {
		div.style.display = "none";
	}

	div = document.getElementById('1dayDIV');
	if (get_cookie('1day') == "block") {
		div.style.display = "block";
	} else {
		div.style.display = "none";
	}

	div = document.getElementById('1weekDIV');
	if (get_cookie('1week') == "block") {
		div.style.display = "block";
	} else {
		div.style.display = "none";
	}

	div = document.getElementById('1monthDIV');
	if (get_cookie('1month') == "block") {
		div.style.display = "block";
	} else {
		div.style.display = "none";
	}

	div = document.getElementById('6monthDIV');
	if (get_cookie('6month') == "block") {
		div.style.display = "block";
	} else {
		div.style.display = "none";
	}

	div = document.getElementById('1yearDIV');
	if (get_cookie('1year') == "block") {
		div.style.display = "block";
	} else {
		div.style.display = "none";
	}

	div = document.getElementById('2yearDIV');
	if (get_cookie('2year') == "block") {
		div.style.display = "block";
	} else {
		div.style.display = "none";
	}

	div = document.getElementById('3yearDIV');
	if (get_cookie('3year') == "block") {
		div.style.display = "block";
	} else {
		div.style.display = "none";
	}
}
</script>

<?php
	if ($mobile_browser > 0) {
		?><meta http-equiv="refresh" content="300"/><?php
	} else {
		?><meta http-equiv="refresh" content="60"/><?php
	}
?>

</head>

<body onload="display_graphs();">

<?php

	if ($mobile_browser > 0) {
		?>
		<img src="img/othertemp-12hour.png"/><br/>
		<img src="img/indoor-12hour.png"/><br/>
		<img src="img/boiler-12hour.png"/><br/>
		<?php
	} else {
		?>
		1hour: <a id="1hrDisp" href="javascript:toggle_graph('1hour');">toggle</a>
		<div id="1hourDIV">
			<img src="img/othertemp-hour.png"/><img src="img/indoor-hour.png"/><br/>
			<img src="img/boiler-hour.png"/><br/>
		</div>

		12hour: <a id="12hrDisp" href="javascript:toggle_graph('12hour');">toggle</a>
		<div id="12hourDIV">
			<img src="img/othertemp-12hour.png"/><img src="img/indoor-12hour.png"/><br/>
			<img src="img/boiler-12hour.png"><br/>
		</div>

		1day: <a id="1dayDisp" href="javascript:toggle_graph('1day');">toggle</a>
		<div id="1dayDIV">
			<img src="img/othertemp-day.png"/><img src="img/indoor-day.png"/><br/>
			<img src="img/boiler-day.png"><br/>
		</div>

		1week: <a id="1weekDisp" href="javascript:toggle_graph('1week');">toggle</a>
		<div id="1weekDIV">
			<img src="img/othertemp-week.png"/><img src="img/indoor-week.png"/><br/>
			<img src="img/boiler-week.png"><br/>
		</div>

		1month: <a id="1monthDisp" href="javascript:toggle_graph('1month');">toggle</a>
		<div id="1monthDIV">
			<img src="img/othertemp-month.png"/><img src="img/indoor-month.png"/><br/>
			<img src="img/boiler-month.png"><br/>
		</div>

		6month: <a id="6monthDisp" href="javascript:toggle_graph('6month');">toggle</a>
		<div id="6monthDIV">
			<img src="img/othertemp-6month.png"/><img src="img/indoor-6month.png"/><br/>
			<img src="img/boiler-6month.png"><br/>
		</div>

		1year: <a id="1yearDisp" href="javascript:toggle_graph('1year');">toggle</a>
		<div id="1yearDIV">
			<img src="img/othertemp-year.png"/><img src="img/indoor-year.png"/><br/>
			<img src="img/boiler-year.png"><br/>
		</div>

		2year: <a id="2yearDisp" href="javascript:toggle_graph('2year');">toggle</a>
		<div id="2yearDIV">
			<img src="img/othertemp-2year.png"/><img src="img/indoor-2year.png"/><br/>
			<img src="img/boiler-2year.png"><br/>
		</div>

		3year: <a id="3yearDisp" href="javascript:toggle_graph('3year');">toggle</a>
		<div id="3yearDIV">
			<img src="img/othertemp-3year.png"/><img src="img/indoor-3year.png"/><br/>
			<img src="img/boiler-3year.png"><br/>
		</div>

		<?php
	}
?>
</body>
</html>
