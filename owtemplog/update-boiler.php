<?php
	$rrdtool = "/usr/local/bin/rrdtool";
	$shed_rrd = "/home/jontow/public_html/graph/rrd/shed.rrd";
	$outside_rrd = "/home/jontow/public_html/graph/rrd/outside.rrd";
	$basement_rrd = "/home/jontow/public_html/graph/rrd/basement.rrd";
	$boiler_rrd = "/home/jontow/public_html/graph/rrd/boiler.rrd";
	$lroomtv_rrd = "/home/jontow/public_html/graph/rrd/lroomtv.rrd";

	header("Content-type: text/plain");

	if (isset($_GET['update'])) {
		$breakout = true;
		$debug = false;

		if (isset($_GET['debug'])) {
			$debug = true;
		}
	
		if (isset($_GET['shed']) && ($_GET['shed'] != '')) {
			$rrdup = "{$rrdtool} update {$shed_rrd} N:{$_GET['shed']}";
			if ($debug) { echo "$rrdup\n"; }
			system($rrdup);
			$breakout = false;
		}

		if (isset($_GET['outside']) && ($_GET['outside'] != '')) {
			$rrdup = "{$rrdtool} update {$outside_rrd} N:{$_GET['outside']}";
			if ($debug) { echo "$rrdup\n"; }
			system($rrdup);
			$breakout = false;
		}

		if (isset($_GET['basement']) && ($_GET['basement'] != '')) {
			$rrdup = "{$rrdtool} update {$basement_rrd} N:{$_GET['basement']}";
			if ($debug) { echo "$rrdup\n"; }
			system($rrdup);
			$breakout = false;
		}

		if (isset($_GET['boiler']) && ($_GET['boiler'] != '')) {
			$rrdup = "{$rrdtool} update {$boiler_rrd} N:{$_GET['boiler']}";
			$logboiler = "echo \"`date +\"%Y-%m-%d %H:%M:%S\"` {$_GET['boiler']}\" >>boiler-log.txt";
			if ($debug) { echo "$rrdup\n"; }
			system($rrdup);
			system($logboiler);
			
			$breakout = false;
		}

		if (isset($_GET['lroomtv']) && ($_GET['lroomtv'] != '')) {
			$rrdup = "{$rrdtool} update {$lroomtv_rrd} N:{$_GET['lroomtv']}";
			$loglroomtv = "echo \"`date +\"%Y-%m-%d %H:%M:%S\"` {$_GET['lroomtv']}\" >>lroomtv-log.txt";
			if ($debug) { echo "$rrdup\n"; }
			system($rrdup);
			system($loglroomtv);
			$breakout = false;
		}


		if ($breakout == true) {
			echo "No update action passed.\n";
		}
	} else {
		echo "No action passed.\n";
	}
?>
