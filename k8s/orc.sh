#!/bin/sh
#
# Silly wrapper to hopefully make multiple kubernetes/openshift cluster
# administration easier.  Symlink the script as "orc" and "korc" to use
# for either one.
#
################################################################################

# Actual path to oc(1) and kubectl(1)
OC="oc"
KUBECTL="kubectl"


CMD_MODE=""
script_name=$(basename "$0")
case "${script_name}" in
    "orc")
        #echo "DEBUG: running in orc/oc mode" >&2
        CMD_MODE="${OC}"
        ;;
    "korc")
        #echo "DEBUG: running in korc/kubectl mode" >&2
        CMD_MODE="${KUBECTL}"
        ;;
    *)
        echo "Error: unknown command mode: ${script_name}" >&2
        exit 1
        ;;
esac

if [ -z "$1" ] || [ "$1" = "login" ]; then
    echo "Syntax: orc <clustername> <additional passthrough oc syntax...>" >&2
    exit 1
fi

cluster="$1"

# Allow symlinking (or whatever) a custom binary per-cluster
if [ -e "$HOME/.kube/${CMD_MODE}.${cluster}" ]; then
    CMD_MODE="$HOME/.kube/${CMD_MODE}.${cluster}"
fi

if [ "$2" = "login" ]; then
    shift
    ${CMD_MODE} "$@"
    cp "$HOME/.kube/config" "$HOME/.kube/orc.${cluster}.config"
    exit 0
else
    KUBECONFIG=$HOME/.kube/orc.${cluster}.config
    if [ ! -e "${KUBECONFIG}" ]; then
        echo "Error: ${KUBECONFIG} does not exist, try logging in." >&2
        exit 1
    fi

    shift
    KUBECONFIG=${KUBECONFIG} ${CMD_MODE} "$@"
    exit 0
fi

# Why we get here?
exit 1
