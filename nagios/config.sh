#!/bin/sh
################################################################################
#
# 2008-03-15 -- jontow@zenbsd.net
#
# Configuration data for nagios scripts
#
################################################################################

CFGDIR="/etc/nagios"
TEMPLATES="${CFGDIR}/templates"

HOST_TEMPLATE="${TEMPLATES}/HOST.TEMPLATE"
