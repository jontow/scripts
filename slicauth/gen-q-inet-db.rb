#!/usr/bin/env ruby
#
# 2009-12-10 -- jontow@zenbsd.net
#
# Merge multiple data sources to form an import file to jam into 
# the quintrex internet database in the format:
#
#  10dig-acctnum, username, password
#

begin
	require 'mysql'
rescue LoadError
	require 'rubygems'
	require 'mysql'
end

require File.dirname(__FILE__) + '/utilsconf.rb'

def get_pass_from_db(username)
	dbh = Mysql.real_connect(UtilsConf::DBHOST, UtilsConf::NEWDBUSER, UtilsConf::NEWDBPASS, UtilsConf::NEWDBNAME)

	result = dbh.query("SELECT password FROM users WHERE username = '#{username}' AND clientid = '1' LIMIT 1")

	if result.num_rows >= 1
		result.each do |row|
			return row[0]
		end
	else
		return nil
	end
end

userhash = {}

if !File.exist?('emaildump.txt')
	puts "Missing emaildump.txt"
	exit
end

if !File.exist?('dsl-db.csv')
	puts "Missing dsl-db.csv"
	exit
end


# First use Prism dump..

File.open('emaildump.txt', 'r').each do |line|
	next if line =~ /^CustomerID/

	rawacctnum, emailaddress = line.split('	', 2)
	padacctnum = "001" + rawacctnum
	username = emailaddress.split('@', 2)[0]
	password = get_pass_from_db(username)

	if password.nil?
		password = "__NO_PASSWORD_FOUND__"
	end
	
	#puts "#{padacctnum}	#{username}	#{password}"
	userhash[username] = {'acctnum' => padacctnum, 'password' => password}
end

# Now override with DSL database
File.open('dsl-db.csv', 'r').each do |line|
	username = nil
	password = nil

	rawusername, accountid, status, rawpassword, firstname, lastname, address1, address2, city, state, zip, country, phone1label, primaryphone, secondaryphone, primaryemail, secondaryemail, faxnumber, mobilenumber, crv, capair, dsl, surg, dsus, occam, blank1, line, ipaddress, fakemac, realmac, login, blank2, wpswd, blank3, blank4, blank5 = line.split(',', 36)

	if rawusername =~ /^".*\/.*"$/
		#p rawusername
		# complicated one.. do goofy things, i tell ya.. goofy things.
		user1, user2 = rawusername.gsub(/"/, '').split(' / ', 2)
		pass1, pass2 = rawpassword.gsub(/"/, '').split(' / ', 2)

		if !userhash[user1].nil? and !primaryphone.nil?
			userhash[user1]['acctnum'] = primaryphone
		else
			userhash[user1] = {'acctnum' => primaryphone, 'password' => pass1}
		end

		if !userhash[user2].nil? and !primaryphone.nil?
			userhash[user2]['acctnum'] = primaryphone
		else
			userhash[user2] = {'acctnum' => primaryphone, 'password' => pass2}
		end

		next
	else
		# simple one..
		username = rawusername
		password = rawpassword

		if !userhash[username].nil? and !primaryphone.nil?
			userhash[username]['acctnum'] = primaryphone

			#puts "#{username}, #{primaryphone}"
		end
	end

	#puts "#{username}, #{password}, #{primaryphone}" if rawusername =~ /^"/
end

#exit

userhash.each do |k, v|
	username = k
	password = v['password']
	acctnum = v['acctnum']

	puts "#{acctnum}	#{username}	#{password}"
end
