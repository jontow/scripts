#!/bin/sh
#
# 2010-02-18 -- jontow@zenbsd.net
#
# Generate new password files, drop them into production.
#

PDBDIR="/etc/raddb/passwd_files"
GENPW="/home/jontow/SVN-ext/jontow/scripts/slicauth/gen-rad-passfiles.rb"

for CLIENT in slic cittele topstele; do
	${GENPW} ${CLIENT} >${PDBDIR}/${CLIENT}_passwd.tmp

	if [ -f ${PDBDIR}/${CLIENT}_passwd ]; then
		mv ${PDBDIR}/${CLIENT}_passwd ${PDBDIR}/${CLIENT}_passwd.old
	fi

	PFSIZE=`ls -s ${PDBDIR}/${CLIENT}_passwd.tmp | awk '{print $1}'`

	# If the generation failed, move the original back into place and die
	if [ "$PFSIZE" = "0" ]; then
		mv ${PDBDIR}/${CLIENT}_passwd.old ${PDBDIR}/${CLIENT}_passwd
		exit
	fi

	mv ${PDBDIR}/${CLIENT}_passwd.tmp ${PDBDIR}/${CLIENT}_passwd
done

# Now re-read the files..
#pkill -1 radiusd
/etc/init.d/freeradius reload
