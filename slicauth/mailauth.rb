#!/usr/bin/env ruby
#
# 2008-10-03 -- jontow@
#
################################################################################
#
# CREATE TABLE mail (
#	Mailbox text NOT NULL,
#	Domain text NOT NULL,
#	Login text NOT NULL,
#	Password text NOT NULL
# );
#
################################################################################

begin
	require 'mysql'
rescue LoadError
	require 'rubygems'
	require 'mysql'
end

require File.dirname(__FILE__) + '/utilsconf.rb'

@runtype = nil
@statusflags = nil

case
	when ARGV[0] == "terminate"
		@runtype = "terminate"
		@statusflags = "(Status = 'I') AND"
	when ARGV[0] == "import"
		@runtype = "import"
		@statusflags = "(Status = 'N' OR Status = 'A' OR Status = 'O') AND"
	else
		puts "Must pass 'import' or 'terminate'."
		exit
end

@mysqldbh = Mysql.real_connect(UtilsConf::DBHOST, UtilsConf::DBUSER, UtilsConf::DBPASS, UtilsConf::DBNAME)

def user_exists? (newtable, customer, domain)
	result = @mysqldbh.query("SELECT Mailbox FROM #{UtilsConf::MAILTABLE} WHERE Mailbox = '#{customer}' AND Domain = '#{domain}'")
	if result.num_rows >= 1
		return true
	else
		return false
	end
end

def port_mysql(domain, utable, uquery)
	result = @mysqldbh.query(uquery)
	result.each do |row|
		mailbox = @mysqldbh.quote(row[1].gsub(/@.*/, ''))
		custname = @mysqldbh.quote(row[0])
		password = @mysqldbh.quote(row[2])

		if @runtype == "import"
			if user_exists?(utable, mailbox, domain)
				#puts "User #{mailbox} already exists.."
			else
				puts "INSERT INTO #{UtilsConf::MAILTABLE} (Mailbox,Domain,Login,Password) VALUES ('#{mailbox}', '#{domain}', '#{custname}', '#{password}')"
				newresult = @mysqldbh.query("INSERT INTO #{UtilsConf::MAILTABLE} (Mailbox,Domain,Login,Password) VALUES ('#{mailbox}', '#{domain}', '#{custname}', '#{password}')")
			end
		elsif @runtype == "terminate"
			puts "DELETE FROM #{UtilsConf::MAILTABLE} WHERE Mailbox = '#{mailbox}' AND Domain = '#{domain}' AND Password = '#{password}'"
			newresult = @mysqldbh.query("DELETE FROM #{UtilsConf::MAILTABLE} WHERE Mailbox = '#{mailbox}' AND Domain = '#{domain}' AND Password = '#{password}'")
		else
			# How'd you get here?
		end
	end
end

port_mysql("slic.com", UtilsConf::SLICTABLE, "SELECT Name, Login, Password FROM #{UtilsConf::SLICTABLE} WHERE #{@statusflags} (ServiceType = 'FREE' OR ServiceType = 'FREEMAIL' OR ServiceType = 'DSL CUSTOMER' OR ServiceType = 'EMPLOYEE' OR ServiceType = 'FAMILYPL' OR ServiceType = 'FAMILYXTRA' OR ServiceType = 'HMGOLD' OR ServiceType = 'HMPLAT' OR ServiceType = 'HMSILVER' OR ServiceType = 'PROFGOLD' OR ServiceType = 'PROFPLAT' OR ServiceType = 'SLC- FAMILY' OR ServiceType = 'SLC- HOMEGOLD' OR ServiceType = 'XTRAMAIL' OR ServiceType = 'HMDIAMOND' OR ServiceType = 'HMPLATDIAMOND' OR ServiceType = 'QUINFREEMAIL' OR ServiceType = 'DSLLIGHT' OR ServiceType = 'QUINXMAIL' OR ServiceType = 'EMPLOYEE DSL')")

port_mysql("castletv.com", UtilsConf::CASTLETVTABLE, "SELECT Name, Login, Password FROM #{UtilsConf::CASTLETVTABLE} WHERE #{@statusflags} (ServiceType = 'FREE' OR ServiceType = 'FREEMAIL' OR ServiceType = 'DSL CUSTOMER' OR ServiceType = 'EMPLOYEE' OR ServiceType = 'FAMILYPL' OR ServiceType = 'FAMILYXTRA' OR ServiceType = 'HMGOLD' OR ServiceType = 'HMPLAT' OR ServiceType = 'HMSILVER' OR ServiceType = 'PROFGOLD' OR ServiceType = 'PROFPLAT' OR ServiceType = 'SLC- FAMILY' OR ServiceType = 'SLC- HOMEGOLD' OR ServiceType = 'XTRAMAIL' OR ServiceType = 'HMDIAMOND' OR ServiceType = 'HMPLATDIAMOND')")

port_mysql("cit-tele.com", UtilsConf::CITTELETABLE, "SELECT Name, Login, Password FROM #{UtilsConf::CITTELETABLE} WHERE #{@statusflags} (ServiceType = 'FREE' OR ServiceType = 'FREEMAIL' OR ServiceType = 'FAMILYXTRA')")

port_mysql("tops-tele.com", UtilsConf::TOPSTELETABLE, "SELECT Name, Login, Password FROM #{UtilsConf::TOPSTELETABLE} WHERE #{@statusflags} (ServiceType = 'FREE' OR ServiceType = 'FREEMAIL')")

exit
