class UtilsConf
	DBUSER = 'authdbuser'
	DBPASS = 'authdbpassword'
	DBHOST = 'localhost'
	DBNAME = 'auth'

	MAILTABLE = 'mail'
	SLICTABLE = 'slic'
	CASTLETVTABLE = 'castletv'
	CITTELETABLE = 'cittele'
	TOPSTELETABLE = 'topstele'
end
