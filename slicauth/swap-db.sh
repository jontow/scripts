#!/bin/sh
#
# 2008-10-28 -- jontow@
#

if [ "$2" = "" ]; then
	echo "Syntax: swap-db.sh db1 db2"
	exit
fi

OLDDB="$1"
NEWDB="$2"

ROOTPASS="password"
CLONEDB="./clone-db.sh"

echo "Cloning ${OLDDB} as tmp${OLDDB}"
sh ${CLONEDB} ${OLDDB} tmp${OLDDB}
echo "Cloning ${NEWDB} as ${OLDDB}"
sh ${CLONEDB} ${NEWDB} ${OLDDB}
echo "Cloning tmp${OLDDB} as ${NEWDB}"
sh ${CLONEDB} tmp${OLDDB} ${NEWDB}

echo "Dropping tmp${OLDDB}"
mysql -u root --password=${ROOTPASS} -e "DROP DATABASE tmp${OLDDB}"

exit
