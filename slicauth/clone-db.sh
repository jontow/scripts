#!/bin/sh
#
# 2008-10-28 -- jontow@
#

if [ "$2" = "" ]; then
	echo "Syntax: clone-db.sh olddb newdb"
	exit
fi

OLDDB="$1"
NEWDB="$2"

ROOTPASS="password"
DBDIR="$HOME/clonedb/${OLDDB}"

NOW=`date +"%Y-%m-%d_%H:%M"`

mkdir -p ${DBDIR}

echo "Dumping ${OLDDB} to ${DBDIR}/${NOW}-${OLDDB}.sql"
mysqldump -u root --password=${ROOTPASS} ${OLDDB} >${DBDIR}/${NOW}-${OLDDB}.sql
echo "Dropping ${NEWDB}"
mysql -u root --password=${ROOTPASS} -e "DROP DATABASE ${NEWDB}"
echo "Creating ${NEWDB}"
mysql -u root --password=${ROOTPASS} -e "CREATE DATABASE ${NEWDB}"
echo "Importing ${DBDIR}/${NOW}-${OLDDB}.sql into ${NEWDB}"
mysql -u root --password=${ROOTPASS} ${NEWDB} <${DBDIR}/${NOW}-${OLDDB}.sql

exit
