#!/usr/bin/env ruby
#
# 2010-02-06 -- jontow@zenbsd.net
#
# Grab authentication data from SQL DB, dump into text files in the format:
#
#     User-Name:Cleartext-Password:Service-Type

# Only active clients/services/users are dumped.
#

begin
        require 'mysql'
rescue LoadError
        require 'rubygems'
        require 'mysql'
end

DEBUG = false

require File.dirname(__FILE__) + '/utilsconf.rb'

@dbh = Mysql.real_connect(UtilsConf::DBHOST, UtilsConf::DBUSER, UtilsConf::DBPASS, UtilsConf::DBNAME)

def get_active_users(client, servicetypes)
	ulist = {}

	p client, servicetypes if DEBUG

	if !servicetypes.nil?
		svcq = " AND (s.svckey = '" + servicetypes.join("' OR s.svckey = '") + "')"
	else
		svcq = ""
	end

	p svcq if DEBUG
	result = @dbh.query("SELECT u.id,u.username,u.password,u.custname,s.svckey FROM users AS u INNER JOIN usersvcs AS us ON u.id=us.userid INNER JOIN services AS s ON us.svcid=s.id INNER JOIN clients AS c ON u.clientid=c.id WHERE c.clientkey = '#{client}' AND c.active = '1' AND u.active = '1' AND us.active = '1' AND s.active = '1' #{svcq}")
	if result.num_rows >= 1
		result.each do |row|
			p row if DEBUG
			#puts "#{row[1]}:#{row[2]}:#{row[4]}"

			# Is user already in list?
			if ulist.has_key?(row[1])
				ulist[row[1]][1] += ",#{row[4]}"
			else
				ulist[row[1]] = [row[2], row[4]]
			end
		end
	end

	return ulist
end

case ARGV[0]
when "slic"
	userlist = get_active_users("slic", ["BASICDIALUP", "PREMIUMDIALUP", "EMPLOYEEDIALUP", "FREEDIALUP", "TURBO", "BASICDSL", "PREMIUMDSL", "EMPLOYEEDSL", "RCLEC"])

	userlist.each_key do |username|
		puts "#{username}:#{userlist[username][0]}:#{userlist[username][1]}"
	end

when "cittele"
	userlist = get_active_users("cittele", ["DIALUP", "DSL"])

	userlist.each_key do |username|
		puts "#{username}:#{userlist[username][0]}:#{userlist[username][1]}"
	end

when "topstele"
	userlist = get_active_users("topstele", ["DIALUP", "DSL"])

	userlist.each_key do |username|
		puts "#{username}:#{userlist[username][0]}:#{userlist[username][1]}"
	end
else
	puts "Syntax: gen-rad-passfiles.rb <clientname>"
	exit
end
