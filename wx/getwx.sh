#!/bin/sh

XMLURL="http://api.wunderground.com/weatherstation/WXCurrentObXML.asp?ID=MKPTD"
TMPFILE=`mktemp wxtmp.XXXX`

wget -q -O $TMPFILE "$XMLURL"

gettag()
{
	TAG=$1

	grep -i "<${TAG}>" ${TMPFILE} | sed '1,$s/^.*<.*>\(.*\)<.*>$/\1/g' # | sed '1,$s/<.*>$//g'
}

if [ "$1" = "short" ]; then
	OB_TIME=$(gettag observation_time | sed '1,$s/\([0-9]*:[0-9]*\)/\1/g')
	CITY=$(gettag city)
	TEMP_F=$(gettag temp_f)
	HUMID=$(gettag relative_humidity)
	WIND_MPH=$(gettag wind_mph)
	WIND_GUST_MPH=$(gettag wind_gust_mph)
	if [ "$2" = "color" ]; then
		echo "[1;35m${OB_TIME}[0;0m ${CITY}: ${TEMP_F}F, H:${HUMID}%, S:${WIND_MPH}/G:${WIND_GUST_MPH} (mph)"
	else
		echo "${OB_TIME} ${CITY}: ${TEMP_F}F, H:${HUMID}%, S:${WIND_MPH}/G:${WIND_GUST_MPH} (mph)"
	fi
else
	echo -n "City: "
	gettag city
	echo -n "Temp: "
	gettag temperature_string
	echo -n "Relative Humidity: "
	echo -n $(gettag relative_humidity)
	echo "%"
	echo -n "Wind: "
	gettag wind_string
fi

rm $TMPFILE
