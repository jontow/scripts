#!/bin/sh
# 
# 2011-10-25 -- jontow@zenbsd.net
#
# Distribute collected SSH public keys to all machines in supplied list
#

CLIENTLIST="clients.txt"
WORKDIR="./tmp"
IFS="
"

mkdir -p ${WORKDIR}

if [ "$2" = "" ]; then
	echo "Syntax: distkeys-host.sh <machine> <class>"
	exit
fi

UHOST="$1"
CLASS="$2"

echo "  ${UHOST} : ${CLASS}"
echo "" >${WORKDIR}/${UHOST}-authorized_keys

for CLIENT in `cat ${CLIENTLIST} | grep -v -e '^#' -e '^$'`; do
	if [ -f ${CLIENT}/id_rsa_${CLASS}.pub ]; then
		echo "APPEND ${CLIENT}/id_rsa_${CLASS}.pub to ${WORKDIR}/${UHOST}-authorized_keys"
		cat ${CLIENT}/id_rsa_${CLASS}.pub >>${WORKDIR}/${UHOST}-authorized_keys
	else
		echo "Missing ${CLIENT}/id_rsa_${CLASS}.pub -- skipping"
	fi
done

echo "scp ${WORKDIR}/${UHOST}-authorized_keys ${UHOST}:GENERATED-${UHOST}-authorized_keys"
scp ${WORKDIR}/${UHOST}-authorized_keys ${UHOST}:GENERATED-${UHOST}-authorized_keys
