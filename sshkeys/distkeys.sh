#!/bin/sh
# 
# 2011-10-25 -- jontow@zenbsd.net
#
# Distribute collected SSH public keys to all machines in supplied list
#

MACHINELIST="machines.txt"
CLIENTLIST="clients.txt"
CLASSFILE="classes.txt"
WORKDIR="./tmp"
IFS="
"

mkdir -p ${WORKDIR}

for MACHINE in `cat $MACHINELIST | grep -v -e '^#' -e '^$'`; do
	UHOST=`echo $MACHINE | awk '{print $1}'`
	CLASS=`echo $MACHINE | awk '{print $2}'`

	./distkeys-host.sh ${UHOST} ${CLASS}
done
