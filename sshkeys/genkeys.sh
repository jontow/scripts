#!/bin/sh

CLASSFILE="classes.txt"
IFS="
"

if [ -f ~/.ssh/config ]; then
	echo "~/.ssh/config already exists.. merge the following in manually:"
	echo ""
	echo "Host *"
	echo "Protocol 2"

	for MACHCLASS in `cat $CLASSFILE | grep -v -e '^#' -e '^$'`; do
		echo "IdentityFile ~/.ssh/keys/id_rsa_${MACHCLASS}"
	done

	echo ""
else
	echo "~/.ssh/config not found, generating.."
	cat >~/.ssh/config <<EOF
Host *
Protocol 2
EOF

	for MACHCLASS in `cat $CLASSFILE | grep -v -e '^#' -e '^$'`; do
		echo "IdentityFile ~/.ssh/keys/id_rsa_${MACHCLASS}" >>~/.ssh/config
	done

	chmod 600 ~/.ssh/config
fi

mkdir -p ~/.ssh/keys

for MACHCLASS in `cat $CLASSFILE | grep -v -e '^#' -e '^$'`; do
	if [ ! -f ~/.ssh/keys/id_rsa_${MACHCLASS} ]; then
		echo "Generating 2048 bit key for \"${MACHCLASS}\".."
		ssh-keygen -t rsa -b 2048 -f ~/.ssh/keys/id_rsa_${MACHCLASS}
	fi
done
