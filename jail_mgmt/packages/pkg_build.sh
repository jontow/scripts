#!/bin/sh
#
#  2006-05-06 -- jontow
#
#    Walk list of installed packages, build binary packages of them and
#    place into PKGBIN, a collection shared amongst jails.
#
###############################################################################

PKGBIN="/packages"
PDBDIR="/var/db/pkg"
PKGSUF="tbz"

echo "*** Changing to ${PKGBIN} to build..."
cd ${PKGBIN}

for PACKAGE in `ls ${PDBDIR}`; do {
	if [ "${PACKAGE}" = "pkgdb.db" ]; then
		# Skipping pkgdb.db ...not even a package
		continue
	fi

	if [ -f ${PKGBIN}/${PACKAGE}.${PKGSUF} ]; then
		echo "*** ${PACKAGE} already built! skipping."
		continue
	else
		echo -n "Creating package: ${PKGBIN}/${PACKAGE}.${PKGSUF} ..."
		pkg_create -b ${PACKAGE}
		echo "done."
	fi
}; done

echo ""
echo ""
echo "Done.  All installed ports processed."
echo ""
echo ""

exit
