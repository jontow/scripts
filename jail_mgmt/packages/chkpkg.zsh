#!/usr/local/bin/zsh
#
#  2006-05-06 -- jontow
#
#    Walk list of built packages, and either interactively or automatically
#    install them based upon 'OPTIONS' as supplied by the user at run-time.
#
###############################################################################

PKGBIN="/packages"
PDBDIR="/var/db/pkg"
PKGSUF="tbz"

OPTIONS="$1"

if [ "${OPTIONS}" = "-h" ]; then
	echo ""
	echo "Usage: chkpkg [-ih]"
	echo "  -i: interactive mode, ask before installing"
	echo "  -h: this message"
	echo ""
	exit
fi

chdir ${PKGBIN}

foreach PACKAGE in `ls *.tbz`; do {
	PKGNAME=`echo ${PACKAGE} | sed '1,$s/\.tbz//g'`

	if [ ! -d ${PDBDIR}/${PKGNAME} ]; then

		if [ "$OPTIONS" = "-i" ]; then
			echo -n "*** ${PKGNAME} not installed! ...Install it now? (y/n) "
			read YESNO

			if [ "$YESNO" = "y" ] || [ "$YESNO" = "Y" ]; then
				pkg_add ${PACKAGE}
			fi
		else
			pkg_add ${PACKAGE}
		fi
			
	fi

}; done
