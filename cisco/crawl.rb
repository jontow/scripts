#!/usr/bin/env ruby
#
# 2013-09-24 -- jtowne@clarkson.edu
#
# "Cisco Crawl" via RANCID's clogin(1) and ruby tomfoolery.
#
################################################################################

require 'rubygems'
require 'rgl/adjacency'
require 'rgl/dot'

################################################################################
#
# Configurables..
#

# Protocol level output
DEBUG = false

# Code flow level output
VERBOSE = true

# Graph device adjacency?
# GRAPH must be 'true' for any of the others to be used.
GRAPH = true
GRAPH_SWITCHES = true
GRAPH_WAPS = false
GRAPH_PHONES = false

# Where to store per-device CDP captures, for debugging and data mining later
LOGDIR="./cdplog"

# Runaway code brake, number of @todo queue runs: should correspond to roughly
# the number of switches on your network, plus some extra just in case.
@runaway = 200

def syntax
	puts ""
	puts "Syntax: cisco-crawl.rb <startswitchIP> <live|captures|capfetch> [runawaycount]"
	puts ""
	puts "startswitchIP: switch to 'seed' the process with.  You must start somewhere, after all."
	puts ""
	puts "Methods:"
	puts "  live:      Use RANCID clogin(1) script to fetch live 'show cdp neighbors detail'"
	puts "  captures:  Use previously captured 'show cdp neighbors detail' files from disk"
	puts "  capfetch:  Use previous captures, or fetch live if nonexistant"
	puts ""
	puts "runawaycount: optional, specifies maximum device iterations before exiting."
	puts ""
	exit
end

################################################################################
#
# Code setup, don't mess with it.
#
if ARGV[1].nil?
	syntax
	exit
end

if ARGV[1] != "live" and
   ARGV[1] != "captures" and
   ARGV[1] != "capfetch"
	syntax
	exit
end

@startip = ARGV[0]
@method = ARGV[1]

if !ARGV[2].nil?
	@runaway = ARGV[2]
end

if !File.exist?(LOGDIR) or !File.directory?(LOGDIR)
	puts "LOGDIR (#{LOGDIR}) doesn't exist, or isn't a directory."
	exit
end

# Queues, to be used later for workflow.
@finished = []
@todo = []

# Groups, to be sorted into by sortneighbors()
@phones = {}
@waps = {}
@switches = {}

# For graph setup
@dg = RGL::DirectedAdjacencyGraph.new if GRAPH
# Start by adding the seed device
@dg.add_vertex(@startip) if GRAPH

################################################################################
#
# Use previously configured (hint: you must previously configure this) RANCID
# scripts (clogin(1)) to obtain command output in bulk.  This means a .cloginrc
# file in ENV['HOME'] that has credentials for *every device this script could
# possible touch*, or its going to fail, and I don't know how.  Probably not
# very nicely.
#
# This is also where the individual device text captures are written out for
# debugging, data mining, whatever.  Filename is the device IP.
#
def fetch_live_cdp(ipaddr)
	if !ipaddr.nil? and !ipaddr.empty?
		cdpoutput = `clogin -c "show cdp neighbors detail" #{ipaddr}`
		cdpcap = File.open(LOGDIR + "/" + ipaddr, "w")
		cdpcap.puts cdpoutput
		cdpcap.close
		return cdpoutput
	end
end

################################################################################
#
# Frontend the fetch_live_cdp() process, by also offering some other methods,
# including the ability to 'fetch' from previously stored captures, or 
# resume/refresh a previous run by fetching any missing captures, but using
# the ones that exist.
#
def getcdp(ipaddr, method)
	puts "VERBOSE: getcdp()" if VERBOSE

	# First option: use only live captures
	if method == "live"
		cdpoutput = fetch_live_cdp(ipaddr)
	# Second option: use only on-disk captures
	elsif method == "captures"
		if File.exists?(LOGDIR + "/" + ipaddr)
			cdpoutput = []
			File.open(LOGDIR + "/" + ipaddr).each do |line|
				cdpoutput << line
			end
		else
			puts "VERBOSE: ERROR: neighbor #{ipaddr} does not exist as a capture." if VERBOSE
			return false
		end
	# Third option: prefer on-disk captures, but fetch missing ones
	elsif method == "capfetch"
		if File.exists?(LOGDIR + "/" + ipaddr)
			cdpoutput = []
			File.open(LOGDIR + "/" + ipaddr).each do |line|
				cdpoutput << line
			end
		else
			cdpoutput = fetch_live_cdp(ipaddr)
		end
	else
		puts "ERROR: getcdp() invalid method passed: #{method}"
		return false
	end

	p cdpoutput if DEBUG

	return cdpoutput
end

################################################################################
#
# Loop over each line of 'show cdp neighbors detail' output, return
# array-of-hashes containing deviceid, deviceip, and devicemodel; useful to
# distinguish whether device is a switch, phone, or wap.
# 
def parsecdp(upstream, cdp)
	neighbors = {}
	devid = nil
	devip = nil
	devmodel = nil
	devport = nil

	puts "VERBOSE: parsecdp()" if VERBOSE
	if cdp.nil? or !cdp or cdp.empty?
		puts "VERBOSE: ERROR: CDP INPUT EMPTY FOR #{upstream}" if VERBOSE
		return []
	end

	cdp.each do |line|
		# Should see DeviceID first: may need to tweak this, if using strange or insane
		# characters in your device hostnames.  Underscore (_) is already insane enough,
		# but I include it here because I know people do it, and Cisco doesn't enforce it.
		if line.chomp =~ /^Device\ ID:\ ([A-Za-z0-9\-_.]+)$/
			devid = $1
			puts "VERBOSE: Got devid: #{devid}" if VERBOSE
		end

		# If we have DeviceID, and get an IP (v4 only).. I don't have IPv6-CDP to test with.
		if !devid.nil? and line.chomp =~ /^\ \ IP\ address:\ ([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)$/
			if devip.nil?
				devip = $1
				puts "VERBOSE: Got devip: #{devip}" if VERBOSE
			else
				puts "VERBOSE: Got another-devip: #{$1}" if VERBOSE
			end
		end

		# If we have DeviceID, and an IP, and we get a Platform..
		if !devid.nil? and !devip.nil? and line.chomp =~ /^Platform:\ (.*),\ \ Capabilities.*$/
			devmodel = $1
			puts "VERBOSE: Got devmodel: #{devmodel}" if VERBOSE
		end

		if !devid.nil? and !devip.nil? and !devmodel.nil? and line.chomp =~ /^Interface: ([A-Za-z0-9\/\-]+),/
			devport = $1
			puts "VERBOSE: Got devport: #{devport}" if VERBOSE
		end

		# We've got all the info.. call an entry 'complete' for our purposes.
		if !devid.nil? and !devip.nil? and !devmodel.nil? and !devport.nil?
			p devid, devip, devmodel, devport if DEBUG

			if neighbors[devid].nil?
				neighbors[devid] = {'upstream'=>upstream, 'devid'=>devid, 'devip'=>devip, 'devmodel'=>devmodel, 'devport'=>devport}
			else
				puts "VERBOSE: ERROR: neighbors['#{devid}'] already exists." if VERBOSE
			end

			devid = nil
			devip = nil
			devmodel = nil
			devport = nil

			#p neighbors if DEBUG
		end

	end
	return neighbors
end

################################################################################
#
# Sort neighbors into groups (and add switches to @todo queue)
# Also adds graph adjacency, can be turned off at top.
#
def sortneighbors(neighbors)
	puts "VERBOSE: sortneighbors()" if VERBOSE
	neighbors.each do |devid,dev|
		if (dev['devmodel'] =~ /^Mitel\ /) or (devid =~ /^SEP/)
			if @phones[devid].nil?
				@phones[devid] = dev

				if GRAPH and GRAPH_PHONES
					if !@dg.has_edge?(dev['upstream'], dev['devip']) and !@dg.has_edge?(dev['devip'], dev['upstream'])
						@dg.add_edge(dev['upstream'], dev['devip'])
					else
						puts "VERBOSE: GRAPH: #{dev['upstream']},#{dev['devip']} already exists as an edge." if VERBOSE
					end
				end
			else
				puts "VERBOSE: PHONES: #{devid} already exists in list." if VERBOSE
			end
		elsif (dev['devmodel'] =~ /^cisco\ AIR/) or
		      (dev['devmodel'] =~ /^LM5/) or (dev['devmodel'] =~ /^LM2/) or (dev['devmodel'] =~ /^B5N/)
			if @waps[devid].nil?
				@waps[devid] = dev

				if GRAPH and GRAPH_WAPS
					if !@dg.has_edge?(dev['upstream'], dev['devip']) and !@dg.has_edge?(dev['devip'], dev['upstream'])
						@dg.add_edge(dev['upstream'], dev['devip'])
					else
						puts "VERBOSE: GRAPH: #{dev['upstream']},#{dev['devip']} already exists as an edge." if VERBOSE
					end
				end
			else
				puts "VERBOSE: WAPS: #{devid} already exists in list." if VERBOSE
			end
		else
			if @switches[devid].nil?
				@switches[devid] = dev

				# Build @todo queue right here, since we only plan to
				# iterate on switches, this is a "great" spot for it.
				# Avoid duplicating items in @todo list.
				if !@finished.include?(dev['devip']) and !@todo.include?(dev['devip'])
					@todo << dev['devip']
				else
					puts "VERBOSE: #{dev['devip']} already in finished queue." if VERBOSE
				end

				if GRAPH and GRAPH_SWITCHES
					if !@dg.has_edge?(dev['upstream'], dev['devip']) and !@dg.has_edge?(dev['devip'], dev['upstream'])
						@dg.add_edge(dev['upstream'], dev['devip'])
					else
						puts "VERBOSE: GRAPH: #{dev['upstream']},#{dev['devip']} already exists as an edge." if VERBOSE
					end
				end
			else
				puts "VERBOSE: SWITCHES: #{devid} already exists in list." if VERBOSE
			end
		end
	end
end


################################################################################
#
# Main code flow
#

# Do seed switch first.
cdpoutput = getcdp(@startip, @method)
neighbors = parsecdp(@startip, cdpoutput)
sortneighbors(neighbors)
@finished << @startip

# Run the queue right into the ground, but at least we have a brake.
runcount = 0
stopthiscrazything = false
while !@todo.nil? and !@todo.empty? and !stopthiscrazything
	@todo.each do |todo|
		runcount += 1
		puts "VERBOSE: queuerun (#{runcount}/#{@runaway}) hit #{todo}" if VERBOSE
		if runcount >= @runaway.to_i
			puts "VERBOSE: Ran over @runaway counter." if VERBOSE
			stopthiscrazything = true
			break
		end

		if !@finished.include?(todo)
			cdpoutput = getcdp(todo, @method)
			neighbors = parsecdp(todo, cdpoutput)
			if neighbors.nil? or neighbors.empty?
				puts "VERBOSE: ERROR: #{todo} came back with no neighbors." if VERBOSE
			end
			sortneighbors(neighbors)

			# shift item from todo queue to finished list
			puts "VERBOSE: removing #{todo} from @todo queue, adding to finished list" if VERBOSE
			@finished << todo
			@todo.delete(todo)
		end
	end
	puts "VERBOSE: RAN OFF END OF QUEUE, LOOPING" if VERBOSE
end

@todo.each do |todo|
	puts "VERBOSE: UNFINISHED: #{todo}" if VERBOSE
end

@finished.each do |finished|
	puts "VERBOSE: FINISHED: #{finished}" if VERBOSE
end

# "Column" headers..
puts "devtype,upstream device ip,device ip, device model --- switchport --- devid" 

################################################################################
#
# Generate phone reporting
#
@phones.each do |devid,phone|
	puts "PHONE,#{phone['upstream']},#{phone['devip']}, #{phone['devmodel']} --- #{phone['devport']} --- #{phone['devid']}"
end

################################################################################
#
# Generate WAP reporting
#
@waps.each do |devid,wap|
	puts "WAP,#{wap['upstream']},#{wap['devip']}, #{wap['devmodel']} --- #{wap['devport']} --- #{wap['devid']}"
end

################################################################################
#
# Generate switch reporting...
#
@switches.each do |devid,switch|
	puts "SWITCH,#{switch['upstream']},#{switch['devip']}, #{switch['devmodel']} --- #{switch['devport']} --- #{switch['devid']}"
end

################################################################################
#
# Generate graph?
#
if GRAPH
	@dg.write_to_graphic_file('jpg')
end
