#!/usr/bin/env ruby
#
# 2014-12-01 -- jtowne@
#
# Parse Cisco IOS interface configs to get interface description, config
# and vlans.  Meant to work exclusively with RANCID sourced config files.
# If you do not use RANCID, you'll have to hack this a small bit.
#
# Should ideally find common configuration problems and report them in the
# output.
#
# Should be run from freshly updated revision controlled working copy of
# RANCID configs, ie. SVN-rancid/yourunitname/configs/
#
# Will not elegantly handle having things like '../' in @cfgfile at this
# time.  You have been warned.  Hell, you'll be lucky if this even runs.
# Must be able to find '../router.db' using current working directory.
#
# If you also pass "fix" as a parameter, it will attempt to generate a
# command script that should be usable with "clogin -x fixfile routerhost",
# although you're not thinking straight if you don't VERY CAREFULLY review
# the contents of the fixfile before running this.
#
################################################################################

@genfixfile = false

if ARGV[0].nil? or ARGV[0].empty?
	STDERR.puts "Syntax: if-errata <switchconfigfile.txt> [fix]"
	exit
end

@cfgfile = ARGV[0]
@fixfilename = nil
@fixfile = nil
@fixes = []

# This feature is designed to generate a command script to fix common problems
# with port configurations.  It does not actually run any of the commands,
# as that would be batshit insane.  The fixfile is generated such that it
# _could_ be run with 'clogin -x fixfile somerouter', if you so chose.
if ARGV[1] == "fix"
	@genfixfile = true
end

if !File.exists?(@cfgfile) or File.directory?(@cfgfile)
	STDERR.puts "Config '#{@cfgfile}' does not exist, or is a directory."
	exit
end

# This is a hack.. but one that saves me a boatload of time:
devstatus = `grep #{@cfgfile} ../router.db`.chomp.split(':', 3)[2]
if devstatus == "down"
	puts "#{File.basename(@cfgfile)}:FAIL: DEVICE MARKED DOWN IN RANCID"
	exit
end

# First, suck in entire config file to an array for
# manipulation and seek-forward reasons
@rawlines = []
File.open(@cfgfile, 'r').each do |line|
	@rawlines << line.chomp
end

# Setup some scoping for prerequisites and persistant data
@hostname = ""
@ifhash = {}
count = 0
@rawlines.each do |line|
	cfghash = nil
	tmpcount = 0

	if line =~ /RANCID-CONTENT-TYPE: ([^ ]+)$/
		if $1 != "cisco"
			puts "#{File.basename(@cfgfile)}:FAIL: UNSUPPORTED CONFIG TYPE (#{$1})"
			exit
		end
	end

	if line =~ /^!Chassis type: ([^ ]+)/
		chassis = $1
		if !(chassis =~ /C2960|C3750/)
			puts "#{File.basename(@cfgfile)}:FAIL: UNKNOWN CHASSIS (#{chassis})"
		exit
		end
	end

	# Grab hostname of the device
	if line =~ /^hostname (.*)$/
		@hostname = $1.gsub(/"/, '')
	end

	# Let's work on interface configs:
	if line =~ /^interface (.*)/
		# Save port name from regexp match that got us here
		port = $1

		# Skip layer3 interfaces
		next if port =~ /^Vlan/

		# Skip management interfaces
		next if port =~ /^FastEthernet0$/

		# If we've got a previous config for this port, don't override it
		# with a [blank] new copy, keep it and add to it.
		if @ifhash[port].nil?
			cfghash = {"desc" => "", "mode" => "", "vlans" => "", "voice" => "", "portfast" => false}
		else
			cfghash = @ifhash[port]
		end

		# Now look ahead in the config by a calculated amount (line at a time),
		# attempting to pull just the relevant port config out, since we can't rely
		# on valid terminators being present, we use the available "\t" (tab):
		# if it isn't there all of a sudden, we're in a new config block.
		done = false
		tmpcount = count
		until done
			tmpcount += 1
			# If we get a !, we're done
			if @rawlines[tmpcount] =~ /^!/
				done = true
				next
			# If we see 'exit' (may not be there), skip it
			elsif @rawlines[tmpcount] =~ /^\texit/
				next
			# Otherwise, we're inside the config block and are interested:
			else
				# XXX: Note that this currently ignores many port configs, including
				# native vlan/pvid, LACP settings, etc.  These can be added later.

				# Save some typing later
				rl = @rawlines[tmpcount].gsub(/^ /, '')

				# Get the description, if we see it
				if rl =~ /description (.*)/
					cfghash["desc"] = $1.gsub(/"/, '')
				end

				# Found a trunk port [doesn't contain all attributes..]
				if rl == "switchport mode trunk"
					if cfghash["mode"] == "ACCESS" or cfghash["mode"] == "TRUNK-AND-ACCESS"
						cfghash["mode"] = "TRUNK-AND-ACCESS"
					else
						cfghash["mode"] = "TRUNK"
					end
				end

				# Found trunk allowed vlans, but could find more!
				if rl =~ /switchport trunk allowed vlan ([0-9,-]+)/
					if cfghash["mode"] == "ACCESS"
						cfghash["mode"] = "TRUNK-AND-ACCESS"
					end
					cfghash["vlans"] = $1
				end

				# Tricky part: these switches, much like Cisco switches, will split
				# apart an extremely long "allowed vlan" line into multiples, using 'add'
				# to chain them for display.  Parse this, substitute a comma for "add" and
				# *APPEND* to the existing vlan list
				if rl =~ /switchport trunk allowed vlan add ([0-9,-]+)/
					if cfghash["mode"] == "ACCESS"
						cfghash["mode"] = "TRUNK-AND-ACCESS"
					end
					cfghash["vlans"] += "," + $1
				end

				# Boring old access port, set mode and the single vlan
				if rl =~ /switchport access vlan (\d+)/
					# This catches a really weird edge case..
					if cfghash["mode"] == "TRUNK" or cfghash["mode"] == "TRUNK-AND-ACCESS"
						cfghash["mode"] = "TRUNK-AND-ACCESS"
					else
						cfghash["mode"] = "ACCESS"
					end
					cfghash["vlans"] = $1
				end

				# If we find a voice vlan when we're a trunk, thats an error.
				if rl =~ /switchport voice vlan (\d+)/ and cfghash["mode"] == "TRUNK"
					cfghash["mode"] = "TRUNK-AND-VOICE"
					cfghash["voice"] = $1
				end

				if rl =~ /switchport voice vlan (\d+)/ and
				   (cfghash["mode"] == "ACCESS" or cfghash["mode"] == "TRUNK-AND-ACCESS")
					cfghash["voice"] = $1
				end

				if rl =~ /spanning-tree portfast/
					cfghash["portfast"] = true
				end

				# Catalog status of shutdown ports as shutdown, do it at
				# the end so that it overwrites any other mode (shutdown is
				# a hard state, no matter what other config is present)
				if rl == "shutdown"
					cfghash["mode"] = "SHUTDOWN"
				end
			end
		end
		#p @rawlines[count]

		# If no port mode is found, its some bullshit DTP situation, and was probably
		# meant to be a trunk.. force it there.
		if cfghash["mode"].empty?
			cfghash["mode"] = "TRUNK"
		end

		# If no vlans were allowed, that means ALL are allowed:
		if cfghash["vlans"].empty? and cfghash["mode"] == "TRUNK"
			cfghash["vlans"] = "ALL"
		end

		# Copy generated cfghash into permanent location
		@ifhash[port] = cfghash

		# Generate fix-file?
		if @genfixfile
			# Found a port with some screwed up shit going on.. both access and trunk set
			if cfghash["mode"] == "TRUNK-AND-ACCESS"
				STDERR.puts "#{@cfgfile}:Generating fix file with #{port} included.. (TRUNK-AND-ACCESS)"
				@fixes << "interface #{port}"
				@fixes << "shutdown"
				@fixes << "no switchport mode"
				@fixes << "no switchport trunk native vlan"
				@fixes << "no switchport trunk allowed vlan"
				access = cfghash["vlans"].gsub(/#{cfghash["voice"]}/, '').gsub(/,/, '')

				@fixes << "switchport access vlan #{access}"
				@fixes << "no shutdown"
				@fixes << "exit"
			end

			# Found a port that has both a voice vlan set and is a trunk
			if cfghash["mode"] == "TRUNK-AND-VOICE"
				STDERR.puts "#{@cfgfile}:Generating fix file with #{port} included.. (TRUNK-AND-VOICE)"
				@fixes << "interface #{port}"
				@fixes << "shutdown"
				@fixes << "no switchport mode trunk"
				@fixes << "no switchport trunk native vlan"
				@fixes << "no switchport trunk allowed vlan"

				access = cfghash["vlans"].gsub(/#{cfghash["voice"]}/, '').gsub(/,/, '')

				@fixes << "switchport access vlan #{access}"
				@fixes << "no shutdown"
				@fixes << "exit"
			end

			# Found an access (edge) port without portfast set
			if cfghash["mode"] != "TRUNK" and !cfghash["portfast"]
				STDERR.puts "#{@cfgfile}:Generating fix file with #{port} included.. (PORTFAST-MISSING)"
				@fixes << "interface #{port}"
				@fixes << "shutdown"
				@fixes << "spanning-tree portfast"
				if cfghash["mode"] != "SHUTDOWN"
					@fixes << "no shutdown"
				end
				@fixes << "exit"
			end
		end
	end

	count += 1
end

# Nice little hack to put a hostname tagged separator in.. used again later.
printf "%-80s\n", "#{File.basename(@cfgfile)}:#{@hostname}:-----------------------------------------------------------------------------SEP"

# Create new [sorted] hash sorting correctly by numeric and alphabetic
# criteria: if the key is a digit, convert it to an integer and sort
# it first; otherwise sort second.
@sortedifhash = @ifhash.sort_by do |k,v|
	if k =~ /^\d+$/
		[1, $&.to_i]
	else
		[2, k]
	end
end

@sortedifhash.each do |k,v|
	portfast = ""
	if !@ifhash[k]["portfast"] and @ifhash[k]["mode"] != "TRUNK"
		portfast = "PORTFAST-MISSING"
	end

	#printf "%-12s %-16s %-42s %-8s(%s)\n", @hostname, "PORT #{k}", @ifhash[k]["desc"], @ifhash[k]["mode"], @ifhash[k]["vlans"]
	printf "%-24s %-42s %-8s(%s) %s\n", "#{File.basename(@cfgfile)}:#{@hostname}:#{k}", @ifhash[k]["desc"], @ifhash[k]["mode"], @ifhash[k]["vlans"], portfast
end

printf "%-80s\n", "#{File.basename(@cfgfile)}:#{@hostname}:-----------------------------------------------------------------------------SEP"

# Now let's actually write the fixfile, if we have things to fix.
if @genfixfile and !@fixes.empty?
	@fixfilename = @cfgfile + ".FIXFILE"
	if File.exists?(@fixfilename)
		puts "fixfile #{@fixfilename} already exists.."
		exit
	end
	@fixfile = File.open(@fixfilename, 'w')

	@fixfile.puts "config terminal"
	@fixes.each do |fix|
		@fixfile.puts fix
	end
	@fixfile.puts "end"
end

#p @ifhash
