#!/usr/bin/env ruby
################################################################################
#
# 2011-03-31 -- jontow@slic.com
#
# Count authenticated SMTP logins and display, used for abuse probability
# Now with source address printing, and password risk analysis! As Seen on TV!
#
# TODO: use cracklib instead of my goofy 'if' logic.
# 
################################################################################

require 'time'

DEBUG = false
LOGALLOWTIME = 300

# Incredibly incomplete list..
badpasswords = ['password', 'admin', 'guest', 'abc123', '1234', '123456', 'test', 'love', 'god', 'sex', 'hockey', 'football', 'baseball', 'basketball']

rawauthcount = {}
revauthcount = []

if ARGV[0].nil? or ARGV[0].empty?
	puts "Syntax: auth-smtp-count <logfile>"
	exit
end

puts "#\n# AUTHENTICATED SMTP SESSIONS BY ACCOUNT\n#\n"

# Rip through the file passed in..
File.open(ARGV[0], 'r').each do |line|
	if line =~ /([A-Z][a-z][a-z]\s+[0-9]+ [0-9]+:[0-9]+:[0-9]+) [A-Za-z]+ msd\[([0-9]+)\]: AUTH success: \[(.+@.*)\]/
		timestamp = $1
		msdpid = $2
		mailuser = $3

		logintime = Time.parse(timestamp)
		print "FOUND AUTH SUCCESS: #{mailuser} (pid:#{msdpid} at #{timestamp} (#{logintime.to_i})): " if DEBUG

		if rawauthcount[mailuser].nil?
			rawauthcount[mailuser] = [1, []]
		else
			rawauthcount[mailuser][0] += 1
		end

		`grep ' msd\\[#{msdpid}\\]:' #{ARGV[0]}`.each do |dline|
			if dmd = /([A-Z][a-z][a-z]\s+[0-9]+ [0-9]+:[0-9]+:[0-9]+) [A-Za-z]+ msd\[([0-9]+)\]: [A-Za-z ]+: connection from ([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)$/.match(dline.chomp)
				loggedat = Time.parse(dmd[1])
				if loggedat.to_i < (logintime.to_i - LOGALLOWTIME) or
				   loggedat.to_i > (logintime.to_i + LOGALLOWTIME)
					# Log irrelevant, outside valid window, disregard
				else
					puts "FROM #{dmd[3]}" if DEBUG

					if !rawauthcount[mailuser][1].include?(dmd[3])
						rawauthcount[mailuser][1] << dmd[3]
					end
				end
			end
		end
	end
end

# Now mangle the hash for pretty-printing.
rawauthcount.each do |k,v|
	revauthcount << [v, k]
end

revauthcount.sort.reverse.each do |account|
	if account[0][0] >= 10
		# TODO:
		#account_password = lookup_password(account[1])
		#printf "%-4s  %s (%s) :: ", account[0][0], account[1], account_password
		abuserisk = nil
	        username, domain = account[1].split('@', 2)
	        userid = `echo "select get_user_id_from_email('#{username}', '#{domain}');" | psql -U user_acl_owner user_acl -q -t | awk '{print $1}'`.chomp.chomp
	        password = `echo "select password from user_pwds where user_id = '#{userid}' and password_type = 'PLAIN' limit 1;" | psql -U user_acl_owner user_acl -q -t | awk '{print $1}'`.chomp.chomp

		if badpasswords.include?(password) or username == password
			risk = "SEVERE"
		elsif password =~ /^[a-z]+$/ or password =~ /^[A-Z]+$/ or password =~ /^[0-9]+$/
			risk = "HIGH"
		elsif password =~ /^[a-z0-9]+$/ or password =~ /^[A-Z0-9]+$/
			risk = "MEDIUM"
		elsif password =~ /^[A-Za-z0-9]+$/
			risk = "LOW"
		else
			risk = "UNKNOWN"
		end
		
		printf "%-4s  %-12s %s (%s) :: ", account[0][0], risk, account[1], password
		puts account[0][1].sort.join(", ")
	end
end
