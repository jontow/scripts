#!/bin/sh
################################################################################
#
# 2009-09-19 -- jontow@zenbsd.net
#
################################################################################
#
# multitail.conf colorscheme entry:
## POP3 proxy -- 2009-09-19 -- jontow@zenbsd.net
#colorscheme:pop3_proxy:Erlang POP3 Proxy
#cs_re_s:blue,,bold:^([^ ]+ [^ ]+) [^ ]+: User
#cs_re_s:magenta,,bold:^[^ ]+ [^ ]+ ([^ ]+): User.*
#cs_re:green:^.*login: correct domain.*
#cs_re:cyan,,bold:^.*login: no domain.*
#cs_re:red:^.*login: incorrect domain.*
#
################################################################################

cd /var/log/pop3_proxy
MTARG=""
for FILE in `ls`; do
	MTARG="$MTARG -cS pop3_proxy -f ${FILE}"
done
multitail --mergeall `echo $MTARG`
