#!/bin/sh

MAILLOG="/var/log/mail.info"

#if [ "$SUDO_USER" = "" ]; then
#	echo "You forgot to use 'sudo' .. trying to do it for you.."
#	exec sudo trackemail $*
#fi

usage()
{
	echo "Usage: trackemail <<from|to>address user@domain.com|ip 1.2.3.4|regexp whatever> [day:NN|logfile]"
	echo "  Examples:"
	echo "    trackemail fromaddress webmaster@slic.com"
	echo "    trackemail ip 1.2.3.4 /var/log/mail.info.0"
	echo "    trackemail toaddress webmaster@slic.com day:23"
	echo "    trackemail regexp 'IP country code\[[1-9]\..*\]'"
	echo "    trackemail regexp 'a48e3620-bfc7-11de-b977-0022195e107c'"
	exit
}

logcat()
{
	MAILLOG="$1"

	if [ "$(echo ${MAILLOG} | grep '\.gz$')" != "" ]; then
		sudo zcat ${MAILLOG}
	else
		sudo cat ${MAILLOG}
	fi
}

fromaddress()
{
	FROMADDR="$1"
	MAILLOG="$2"
	
	echo "fromaddress(): ${FROMADDR} in ${MAILLOG}"

	IFS='
'

	for LOGLINE in `logcat ${MAILLOG} | egrep -i "FROM:.*${FROMADDR}"`; do
		echo "################################################################################"
		MSDPID=$(echo $LOGLINE | awk '{print $5}' | sed -e '1,$s/msd\[//g' -e '1,$s/\]://g')
		HOUR=$(echo $LOGLINE | awk '{print $3}' | awk -F: '{print $1}')
		MINUTE=$(echo $LOGLINE | awk '{print $3}' | awk -F: '{print $2}')

		#echo "LOG (${MSDPID}): ${LOGLINE}"
		logcat ${MAILLOG} | egrep -i "${HOUR}:.*msd\[${MSDPID}\]:"
	done
}

toaddress()
{
	TOADDR="$1"
	MAILLOG="$2"
	
	echo "toaddress(): ${TOADDR} in ${MAILLOG}"

	IFS='
'

	for LOGLINE in `logcat ${MAILLOG} | grep -i "TO:.*${TOADDR}"`; do
		echo "################################################################################"
		MSDPID=$(echo $LOGLINE | awk '{print $5}' | sed -e '1,$s/msd\[//g' -e '1,$s/\]://g')
		HOUR=$(echo $LOGLINE | awk '{print $3}' | awk -F: '{print $1}')

		echo "LOG (${MSDPID}): ${LOGLINE}"
		logcat ${MAILLOG} | egrep -i "${HOUR}:.*msd\[${MSDPID}\]:"
	done
}

ipaddr()
{
	IPADDR="$1"
	MAILLOG="$2"

	echo "ipaddr(): ${IPADDR} in ${MAILLOG}"

	IFS='
'

	for LOGLINE in `logcat ${MAILLOG} | grep -i "connection from ${IPADDR}"`; do
		echo "################################################################################"
		MSDPID=$(echo $LOGLINE | awk '{print $5}' | sed -e '1,$s/msd\[//g' -e '1,$s/\]://g')
		HOUR=$(echo $LOGLINE | awk '{print $3}' | awk -F: '{print $1}')

		echo "LOG (${MSDPID}): ${LOGLINE}"
		logcat ${MAILLOG} | egrep -i "${HOUR}:.*msd\[${MSDPID}\]:"
	done
}

regexp()
{
	MAILLOG="$1"
	SOMESTRING="$2"

	echo "regexp(): ${SOMESTRING} in ${MAILLOG}"

	IFS='
'

	for LOGLINE in $(logcat ${MAILLOG} | egrep -i ${SOMESTRING}); do
		echo "################################################################################"
		MSDPID=$(echo $LOGLINE | awk '{print $5}' | sed -e '1,$s/msd\[//g' -e '1,$s/\]://g')
		HOUR=$(echo $LOGLINE | awk '{print $3}' | awk -F: '{print $1}')

		echo "LOG (${MSDPID}): ${LOGLINE}"
		logcat ${MAILLOG} | egrep -i "${HOUR}:.*msd\[${MSDPID}\]:"
	done
}

if [ "$2" = "" ]; then
	usage
fi

if [ "$3" != "" ]; then
	MAILLOG="$3"

	# Support maillog == day:NN
	if [ "$(echo ${MAILLOG} | egrep -i '^day:')" != "" ]; then
		DAYNUM=`echo ${MAILLOG} | awk -F: '{print $2}'`
		LOOKFOR=`date +"%Y-%m-"`${DAYNUM}

		MAILLOG="$(ls -l /var/log/mail.info.* | grep -i " ${LOOKFOR} " | awk '{print $NF}')"

		if [ "${MAILLOG}" = "" ]; then
			echo "Couldn't find log for rotate date of ${LOOKFOR}"
			exit
		fi

		echo "SEARCHING WITH ROTATE DATE OF ${LOOKFOR} in ${MAILLOG}"
	fi
fi

case $1 in
	"fromaddress")
		fromaddress $2 $MAILLOG
		exit
		;;
	"toaddress")
		toaddress $2 $MAILLOG
		exit
		;;
	"ip")
		ipaddr $2 $MAILLOG
		exit
		;;
	"regexp")
		regexp $MAILLOG "$2"
		exit
		;;
	*)
		usage
		;;
esac

echo "Falling through.. error or no match!"
