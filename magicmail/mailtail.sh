#!/bin/sh
#
# 2009-09-20 -- jontow@zenbsd.net

MAILLOG="/var/log/mail.info"

sudo multitail -cS magicmail -f ${MAILLOG}
