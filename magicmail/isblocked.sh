#!/bin/sh
#
# 2010-02-01 -- jontow@zenbsd.net
#
# Check RCPT block list for given IP address, optionally remove it
#

if [ "$1" = "" ]; then
	echo "Syntax: isblocked.sh <ipaddr> [-d]"
	exit
fi

BLOCKEDIP="$1"

BRULE=$(iptables --line-numbers -n -L smtpblocks | grep "$BLOCKEDIP")
RULENUM=$(echo $BRULE | awk '{print $1}')

if [ "$BRULE" = "" ]; then
	echo "IP ($BLOCKEDIP) not found in RCPT block list"
	exit
fi

if [ "$2" = "-d" ]; then
	echo "Deleting $BLOCKEDIP from RCPT block list.."
	iptables -D smtpblocks $RULENUM
	echo "Done."
	exit
else
	echo $BRULE
fi
