#!/bin/sh

if [ -z "$1" ]; then
    echo "Syntax: run.sh <script>"
    exit 1
fi

if [ -e "$1" ]; then
    for AP in jeroom-ap1.je; do
        ssh -T -i ~/.ssh/id_rsa_nopass apiadm@${AP} < "$1"
    done
fi
