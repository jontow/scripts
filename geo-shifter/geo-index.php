<?php
/*
 * 2014-03-04 -- jontow@zenbsd.net
 *
 * Curious little hack to divide USA vs. non-USA viewers,
 * with an exception for LAN traffic.
 *
 */

// Expire old cookie, if it exists.
setcookie("MagicDonut", "", time()-3600);

$record = geoip_record_by_name($_SERVER['REMOTE_ADDR']);

/******************************************************************************/

/* Immediate shove to 'local' if coming from on-net. */
if (preg_match("/^192\.168\.\d+\.\d+$/", $_SERVER['REMOTE_ADDR'])) {
	setcookie("MagicDonut", "9270f6ecabca29c8d25348b032f83976", time()+7200);
	header("Location: local.php");
	//echo ":: LAN ::<br/>";
	exit();
}

/******************************************************************************/

/* country_name for production, city for development */
//if ($record['city'] == "HometownUSA") {
if ($record['country_name'] == "United States") {
	/* MD5 hash is 'allow' */
	setcookie("MagicDonut", "9270f6ecabca29c8d25348b032f83976", time()+7200);
	header("Location: local.php");
	//echo ":: USA ::<br/>";
} else {
	/* MD5 hash is 'deny' */
	setcookie("MagicDonut", "a5c9086a7b2ba742be7447b66717fb3a", time()+7200);
	header("Location: remote.php");
	//echo ":: NOT USA! ::<br/>";
}

/******************************************************************************/

/* DEBUG: */
/*
echo $_SERVER['REMOTE_ADDR'] . "<br/>";
echo $record['city'] . "<br/>";
echo $record['region'] . "<br/>";
echo $record['country_name'] . "<br/>";
*/

?>
